﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using PRCService.Models;

namespace PRCService
{
    public partial class PRCService : ServiceBase
    {
        System.Timers.Timer timer = new System.Timers.Timer();
        Int32 ThreadSleepTime = Int32.Parse(ConfigurationManager.AppSettings["ServiceThreadSleepTime"]);
        System.Threading.Thread serviceThread;

        public PRCService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {

            serviceThread = new System.Threading.Thread(new System.Threading.ThreadStart(OnstartThread));
            // Start the thread
            serviceThread.Start();
        }

        public void OnstartThread()
        {
            DAL dal = new DAL();
            if (dal.ValidateDatabaseConnection())
            {
                LogError("PRCService", "OnstartThread", null, "Attemp To Connect With Database Was Successful." + DateTime.Now);
                try
                {
                    string message = this.ServiceName + " is started at " + DateTime.Now;
                    LogError("PRCService", "OnstartThread", null,message);
                    timer.Elapsed += new System.Timers.ElapsedEventHandler(OnElapsedTime);
                    timer.Interval = ThreadSleepTime; //number in milisecinds 
                    timer.Enabled = true;

                }
                catch (Exception ex)
                {
                    LogError("PRCService", "OnstartThread", ex);
                }
            }
            else
            {
                string message = "Attemp To Connect With Database Was Failed." + DateTime.Now;
                LogError("PRCService", "OnstartThread", null, message);
            }

        }


        public void TempStartService()
        {

            DAL dal = new DAL();
            if (dal.ValidateDatabaseConnection())
            {
                LogError("PRCService", "OnstartThread", null, "Attemp To Connect With Database Was Successful." + DateTime.Now);
                try
                {
                    string message = this.ServiceName + " is started at " + DateTime.Now;
                    LogError("PRCService", "OnstartThread", null, message);
                    timer.Elapsed += new System.Timers.ElapsedEventHandler(OnElapsedTime);
                    timer.Interval = ThreadSleepTime; //number in milisecinds 
                    timer.Enabled = true;

                }
                catch (Exception ex)
                {
                    LogError("PRCService", "OnstartThread", ex);
                }
            }
            else
            {
                string message = "Attemp To Connect With Database Was Failed." + DateTime.Now;
                LogError("PRCService", "OnstartThread", null, message);
            }
        }



        private void OnElapsedTime(object source, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                string message = this.ServiceName + " is recall at " + DateTime.Now;
                LogError("PRCService", "OnElapsedTime", null, message);
                DATA_LAKE_READER data = new DATA_LAKE_READER();
                AUTO_EMAILING AutoEmailing = new AUTO_EMAILING();
                
            }
            catch (Exception ex)
            {
                LogError("PRCService", "OnElapsedTime", ex);
            }
        }
        public void TestingMethod()
        {
            try
            {
                string message = this.ServiceName + " is recall at " + DateTime.Now;
                AUTO_EMAILING AutoEmailing = new AUTO_EMAILING();
                
            }
            catch (Exception ex)
            {
                LogError("PRCService", "OnElapsedTime", ex);
            }
        }

        protected override void OnStop()
        {
            try
            {
                serviceThread.Abort();
                string message = this.ServiceName + " is stoped at " + DateTime.Now;
                LogError("PRCService", "OnStop", null, message);
            }
            catch (Exception ex)
            {
                LogError("PRCService","OnStop",ex);
            }
        }


        public void LogError(string controllername, string functionName, Exception excep = null, string message = "")
        {
            // code starts for creating log file starts here this code will be appended to main menu file

            string complete_path = AppDomain.CurrentDomain.BaseDirectory + "PRC_Logs\\";
            string filename = DateTime.Now.Date.Day + "_" + DateTime.Now.Date.Month + "_" + DateTime.Now.Date.Year + ".txt";
            string path = complete_path + filename;
            bool logfilexheck = false;
            if (System.IO.Directory.Exists(path))
            {
                logfilexheck = true;
            }
            else
            {
                System.IO.DirectoryInfo dr = System.IO.Directory.CreateDirectory(complete_path);
                logfilexheck = true;
            }
            if (logfilexheck)
            {
                System.IO.FileStream fs = new System.IO.FileStream(path, System.IO.FileMode.Append, System.IO.FileAccess.Write);

                // Writing log begins            
                System.IO.StreamWriter writer = new System.IO.StreamWriter(fs);
                writer.WriteLine("---------------------------------------------------------------");
                writer.WriteLine("Time of logging: " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString());
                writer.WriteLine("Class/Controller/Report Name: " + controllername);
                writer.WriteLine("Function Name: " + functionName);
                if (excep != null)
                {

                    writer.WriteLine("Exception Description:");
                    writer.WriteLine(excep.Message);
                    writer.WriteLine("Exception Stack Trace:");
                    writer.WriteLine(excep.StackTrace);
                }
                else
                {
                    if (message != "")
                    {
                        writer.WriteLine("Validation Message :");
                        writer.WriteLine(message);
                    }
                }
                writer.WriteLine("--------------------------------------------------------------- ");
                writer.Close();
                //Writing log ends
                fs.Close();
                //System.Windows.Forms.MessageBox.Show("Error found! check the log at: " + path );
            }

            // code for creating log file ends here
        }


    }
}
