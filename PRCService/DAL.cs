﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Configuration;
using PRCService.Models;

namespace PRCService
{
    class DAL
    {
        public DAL()
        {

        }

        public SqlConnection GetSqlConnection()
        {
            SqlConnection conn = new SqlConnection();
            try
            {
                string SERVER_PROP_NAME = ConfigurationManager.AppSettings["SERVER_PROP_NAME"];
                string SERVER_PROP_VALUE = ConfigurationManager.AppSettings["SERVER_PROP_VALUE"];
                string DATABASE_PROP_NAME = ConfigurationManager.AppSettings["DATABASE_PROP_NAME"];
                string DATABASE_PROP_VALUE = ConfigurationManager.AppSettings["DATABASE_PROP_VALUE"];
                conn.ConnectionString = SERVER_PROP_NAME + "=" + SERVER_PROP_VALUE + ";" + DATABASE_PROP_NAME + "=" + DATABASE_PROP_VALUE + ";" + "Trusted_Connection=True;MultipleActiveResultSets=true";
            }
            catch (Exception ex)
            {
                string Message = "";
                if (ex is SqlException)
                {
                    SqlException exx = (SqlException)ex;
                    for (int i = 0; i < exx.Errors.Count; i++)
                    {
                        Message += "Index #" + i + "\n" +
                            "Message: " + exx.Errors[i].Message + Environment.NewLine +
                            "LineNumber: " + exx.Errors[i].LineNumber + Environment.NewLine +
                            "Source: " + exx.Errors[i].Source + Environment.NewLine +
                            "Procedure: " + exx.Errors[i].Procedure + Environment.NewLine;
                    }
                    LogError("DAL","GetSqlConnection",null, "Error On Connection : " + Message);
                }
                else
                {
                    LogError("DAL", "GetSqlConnection", null, "Error On Connection : " + ex.ToString());
                }
            }

            return conn;
        }
        public bool ValidateDatabaseConnection()
        {
            string message = "";
            try
            {
                SqlConnection conn = GetSqlConnection();
                conn.Open();
                System.Threading.Thread.Sleep(5000);
                conn.Close();
                return true;
            }
            catch (Exception ex)
            {
                message = DateTime.Now.ToString() + Environment.NewLine;
                message += "Exception Occured on onStart" + ex.Message + Environment.NewLine;
                if (ex.InnerException != null)
                {
                    message += "Inner Exception : " + ex.InnerException.Message.ToString() + Environment.NewLine;
                    if (ex.InnerException.InnerException != null)
                    {
                        message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                    }
                }
                return false;
            }
            finally
            {
                if (message != "")
                {
                    LogError("DAL","ValidateDatabaseConnection",null,message);
                }
            }
        }

        public PRC_GOV_PRC_DETAIL GetPRC_DATA_Single(string BankRef)
        {
            string message = "";
            try
            {
                SqlConnection conn = GetSqlConnection();
                conn.Open();
                SqlCommand cmd = new SqlCommand("Select * from PRC_GOV_PRC_DETAIL WHERE GPD_BANKREFNO = '" + BankRef + "'", conn);
                SqlDataAdapter da = new SqlDataAdapter(cmd.CommandText, conn);
                DataTable dt = new DataTable();
                da.Fill(dt);
                conn.Close();
                if (dt.Rows.Count > 0)
                {
                    PRC_GOV_PRC_DETAIL newRow = new PRC_GOV_PRC_DETAIL();
                    newRow.GPD_ID = dt.Rows[0]["GPD_ID"] == DBNull.Value ? 0 : Convert.ToInt32(dt.Rows[0]["GPD_ID"]);
                    newRow.GPD_CLIENT = dt.Rows[0]["GPD_CLIENT"] == DBNull.Value ? null : dt.Rows[0]["GPD_CLIENT"].ToString();
                    if (dt.Rows[0]["GPD_TRNSDATE"] != DBNull.Value)
                        newRow.GPD_TRNSDATE = Convert.ToDateTime(dt.Rows[0]["GPD_TRNSDATE"]);
                    else
                        newRow.GPD_TRNSDATE = null;
                    newRow.GPD_CCY = dt.Rows[0]["GPD_CCY"] == DBNull.Value ? null : dt.Rows[0]["GPD_CCY"].ToString();
                    newRow.GPD_AMOUNT = dt.Rows[0]["GPD_AMOUNT"] == DBNull.Value ? null : dt.Rows[0]["GPD_AMOUNT"].ToString();
                    newRow.GPD_PURPOSE = dt.Rows[0]["GPD_PURPOSE"] == DBNull.Value ? null : dt.Rows[0]["GPD_PURPOSE"].ToString();
                    newRow.GPD_BY_ORDER_COUNTRY = dt.Rows[0]["GPD_BY_ORDER_COUNTRY"] == DBNull.Value ? null : dt.Rows[0]["GPD_BY_ORDER_COUNTRY"].ToString();
                    newRow.GPD_PAGE_NO = dt.Rows[0]["GPD_PAGE_NO"] == DBNull.Value ? null : dt.Rows[0]["GPD_PAGE_NO"].ToString();
                    newRow.GPD_SNO = dt.Rows[0]["GPD_SNO"] == DBNull.Value ? null : dt.Rows[0]["GPD_SNO"].ToString();
                    newRow.GPD_PCODE = dt.Rows[0]["GPD_PCODE"] == DBNull.Value ? null : dt.Rows[0]["GPD_PCODE"].ToString();
                    newRow.GPD_CCODE = dt.Rows[0]["GPD_CCODE"] == DBNull.Value ? null : dt.Rows[0]["GPD_CCODE"].ToString();
                    newRow.GPD_BANKREFNO = dt.Rows[0]["GPD_BANKREFNO"] == DBNull.Value ? null : dt.Rows[0]["GPD_BANKREFNO"].ToString();
                    newRow.GPD_RATE = dt.Rows[0]["GPD_RATE"] == DBNull.Value ? null : dt.Rows[0]["GPD_RATE"].ToString();
                    newRow.GPD_LCYAMOUNT = dt.Rows[0]["GPD_LCYAMOUNT"] == DBNull.Value ? null : dt.Rows[0]["GPD_LCYAMOUNT"].ToString();
                    newRow.GPD_CUSTOMER_NO = dt.Rows[0]["GPD_CUSTOMER_NO"] ==DBNull.Value ? null :  dt.Rows[0]["GPD_CUSTOMER_NO"].ToString();
                    newRow.GPD_CUSTOMER_NAME = dt.Rows[0]["GPD_CUSTOMER_NAME"] == DBNull.Value ? null : dt.Rows[0]["GPD_CUSTOMER_NAME"].ToString();
                    newRow.GPD_ACCOUNT_NO = dt.Rows[0]["GPD_ACCOUNT_NO"] == DBNull.Value ? null : dt.Rows[0]["GPD_ACCOUNT_NO"].ToString();
                    newRow.GPD_ACCOUNT_NAME = dt.Rows[0]["GPD_ACCOUNT_NAME"] == DBNull.Value ? null : dt.Rows[0]["GPD_ACCOUNT_NAME"].ToString();
                    newRow.GPD_PRINT_STATUS = dt.Rows[0]["GPD_PRINT_STATUS"] == DBNull.Value ? null : dt.Rows[0]["GPD_PRINT_STATUS"].ToString();
                    newRow.GPD_BY_ORDER_CNIC = dt.Rows[0]["GPD_BY_ORDER_CNIC"] == DBNull.Value ? null : dt.Rows[0]["GPD_BY_ORDER_CNIC"].ToString();
                    newRow.GPD_BY_ORDER_IBAN = dt.Rows[0]["GPD_BY_ORDER_IBAN"] == DBNull.Value ? null :  dt.Rows[0]["GPD_BY_ORDER_IBAN"].ToString();
                    newRow.GPD_BY_ORDER_IBAN_COUNTRY = dt.Rows[0]["GPD_BY_ORDER_IBAN_COUNTRY"] == DBNull.Value ? null : dt.Rows[0]["GPD_BY_ORDER_IBAN_COUNTRY"].ToString();
                    newRow.GPD_REMITTING_INSTITUTION = dt.Rows[0]["GPD_REMITTING_INSTITUTION"].ToString();
                    newRow.GPD_BENE_NTN = dt.Rows[0]["GPD_BENE_NTN"] == DBNull.Value ? null : dt.Rows[0]["GPD_BENE_NTN"].ToString();
                    if(dt.Rows[0]["GPD_UPLOAD_DATE"] != DBNull.Value)
                        newRow.GPD_UPLOAD_DATE = Convert.ToDateTime(dt.Rows[0]["GPD_UPLOAD_DATE"]);
                    else
                        newRow.GPD_UPLOAD_DATE = null;
                    if (dt.Rows[0]["GPD_MODIFIED_DATE"] != DBNull.Value)
                        newRow.GPD_MODIFIED_DATE = Convert.ToDateTime(dt.Rows[0]["GPD_MODIFIED_DATE"]);
                    else
                        newRow.GPD_MODIFIED_DATE = null;
                    newRow.GPD_PRC_NUMBER = dt.Rows[0]["GPD_PRC_NUMBER"] == DBNull.Value ? null : dt.Rows[0]["GPD_PRC_NUMBER"].ToString();
                    newRow.GPD_COMMENTS = dt.Rows[0]["GPD_COMMENTS"] == DBNull.Value ? null : dt.Rows[0]["GPD_COMMENTS"].ToString();
                    newRow.GPD_MAKER_ID = dt.Rows[0]["GPD_MAKER_ID"] == DBNull.Value ? null : dt.Rows[0]["GPD_MAKER_ID"].ToString();
                    newRow.GPD_CHECKER_ID = dt.Rows[0]["GPD_CHECKER_ID"] == DBNull.Value ? null : dt.Rows[0]["GPD_CHECKER_ID"].ToString();
                    newRow.GPD_ISAUTH = dt.Rows[0]["GPD_ISAUTH"] == DBNull.Value ? false : Convert.ToBoolean(dt.Rows[0]["GPD_ISAUTH"]);
                    return newRow;
                }
                else
                    return null;
            }
            catch (Exception ex)
            {
                message = DateTime.Now.ToString() + Environment.NewLine;
                message += "Exception Occured on onStart" + ex.Message + Environment.NewLine;
                if (ex.InnerException != null)
                {
                    message += "Inner Exception : " + ex.InnerException.Message.ToString() + Environment.NewLine;
                    if (ex.InnerException.InnerException != null)
                    {
                        message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                    }
                }
                return null;
            }
            finally
            {
                if (message != "")
                {
                    LogError("DAL", "GetPRC_DATA_Single",null,message);
                }
            }
        }


        public PRC_GOV_PRC_DETAIL GetPRC_MAX_NUM_TODAY(string PRCNUM)
        {
            string message = "";
            try
            {
                SqlConnection conn = GetSqlConnection();
                conn.Open();
                SqlCommand cmd = new SqlCommand("SELECT TOP 1 * FROM PRC_GOV_PRC_DETAIL WHERE GPD_PRC_NUMBER LIKE '%" + PRCNUM +"%' ORDER BY GPD_PRC_NUMBER DESC", conn);
                SqlDataAdapter da = new SqlDataAdapter(cmd.CommandText, conn);
                DataTable dt = new DataTable();
                da.Fill(dt);
                conn.Close();
                if (dt.Rows.Count > 0)
                {
                    PRC_GOV_PRC_DETAIL newRow = new PRC_GOV_PRC_DETAIL();
                    newRow.GPD_ID = dt.Rows[0]["GPD_ID"] == DBNull.Value ? 0 : Convert.ToInt32(dt.Rows[0]["GPD_ID"]);
                    newRow.GPD_CLIENT = dt.Rows[0]["GPD_CLIENT"] == DBNull.Value ? null : dt.Rows[0]["GPD_CLIENT"].ToString();
                    if (dt.Rows[0]["GPD_TRNSDATE"] != DBNull.Value)
                        newRow.GPD_TRNSDATE = Convert.ToDateTime(dt.Rows[0]["GPD_TRNSDATE"]);
                    else
                        newRow.GPD_TRNSDATE = null;
                    newRow.GPD_CCY = dt.Rows[0]["GPD_CCY"] == DBNull.Value ? null : dt.Rows[0]["GPD_CCY"].ToString();
                    newRow.GPD_AMOUNT = dt.Rows[0]["GPD_AMOUNT"] == DBNull.Value ? null : dt.Rows[0]["GPD_AMOUNT"].ToString();
                    newRow.GPD_PURPOSE = dt.Rows[0]["GPD_PURPOSE"] == DBNull.Value ? null : dt.Rows[0]["GPD_PURPOSE"].ToString();
                    newRow.GPD_BY_ORDER_COUNTRY = dt.Rows[0]["GPD_BY_ORDER_COUNTRY"] == DBNull.Value ? null : dt.Rows[0]["GPD_BY_ORDER_COUNTRY"].ToString();
                    newRow.GPD_PAGE_NO = dt.Rows[0]["GPD_PAGE_NO"] == DBNull.Value ? null : dt.Rows[0]["GPD_PAGE_NO"].ToString();
                    newRow.GPD_SNO = dt.Rows[0]["GPD_SNO"] == DBNull.Value ? null : dt.Rows[0]["GPD_SNO"].ToString();
                    newRow.GPD_PCODE = dt.Rows[0]["GPD_PCODE"] == DBNull.Value ? null : dt.Rows[0]["GPD_PCODE"].ToString();
                    newRow.GPD_CCODE = dt.Rows[0]["GPD_CCODE"] == DBNull.Value ? null : dt.Rows[0]["GPD_CCODE"].ToString();
                    newRow.GPD_BANKREFNO = dt.Rows[0]["GPD_BANKREFNO"] == DBNull.Value ? null : dt.Rows[0]["GPD_BANKREFNO"].ToString();
                    newRow.GPD_RATE = dt.Rows[0]["GPD_RATE"] == DBNull.Value ? null : dt.Rows[0]["GPD_RATE"].ToString();
                    newRow.GPD_LCYAMOUNT = dt.Rows[0]["GPD_LCYAMOUNT"] == DBNull.Value ? null : dt.Rows[0]["GPD_LCYAMOUNT"].ToString();
                    newRow.GPD_CUSTOMER_NO = dt.Rows[0]["GPD_CUSTOMER_NO"] == DBNull.Value ? null : dt.Rows[0]["GPD_CUSTOMER_NO"].ToString();
                    newRow.GPD_CUSTOMER_NAME = dt.Rows[0]["GPD_CUSTOMER_NAME"] == DBNull.Value ? null : dt.Rows[0]["GPD_CUSTOMER_NAME"].ToString();
                    newRow.GPD_ACCOUNT_NO = dt.Rows[0]["GPD_ACCOUNT_NO"] == DBNull.Value ? null : dt.Rows[0]["GPD_ACCOUNT_NO"].ToString();
                    newRow.GPD_ACCOUNT_NAME = dt.Rows[0]["GPD_ACCOUNT_NAME"] == DBNull.Value ? null : dt.Rows[0]["GPD_ACCOUNT_NAME"].ToString();
                    newRow.GPD_PRINT_STATUS = dt.Rows[0]["GPD_PRINT_STATUS"] == DBNull.Value ? null : dt.Rows[0]["GPD_PRINT_STATUS"].ToString();
                    newRow.GPD_BY_ORDER_CNIC = dt.Rows[0]["GPD_BY_ORDER_CNIC"] == DBNull.Value ? null : dt.Rows[0]["GPD_BY_ORDER_CNIC"].ToString();
                    newRow.GPD_BY_ORDER_IBAN = dt.Rows[0]["GPD_BY_ORDER_IBAN"] == DBNull.Value ? null : dt.Rows[0]["GPD_BY_ORDER_IBAN"].ToString();
                    newRow.GPD_BY_ORDER_IBAN_COUNTRY = dt.Rows[0]["GPD_BY_ORDER_IBAN_COUNTRY"] == DBNull.Value ? null : dt.Rows[0]["GPD_BY_ORDER_IBAN_COUNTRY"].ToString();
                    newRow.GPD_REMITTING_INSTITUTION = dt.Rows[0]["GPD_REMITTING_INSTITUTION"].ToString();
                    newRow.GPD_BENE_NTN = dt.Rows[0]["GPD_BENE_NTN"] == DBNull.Value ? null : dt.Rows[0]["GPD_BENE_NTN"].ToString();
                    if (dt.Rows[0]["GPD_UPLOAD_DATE"] != DBNull.Value)
                        newRow.GPD_UPLOAD_DATE = Convert.ToDateTime(dt.Rows[0]["GPD_UPLOAD_DATE"]);
                    else
                        newRow.GPD_UPLOAD_DATE = null;
                    if (dt.Rows[0]["GPD_MODIFIED_DATE"] != DBNull.Value)
                        newRow.GPD_MODIFIED_DATE = Convert.ToDateTime(dt.Rows[0]["GPD_MODIFIED_DATE"]);
                    else
                        newRow.GPD_MODIFIED_DATE = null;
                    newRow.GPD_PRC_NUMBER = dt.Rows[0]["GPD_PRC_NUMBER"] == DBNull.Value ? null : dt.Rows[0]["GPD_PRC_NUMBER"].ToString();
                    newRow.GPD_COMMENTS = dt.Rows[0]["GPD_COMMENTS"] == DBNull.Value ? null : dt.Rows[0]["GPD_COMMENTS"].ToString();
                    newRow.GPD_MAKER_ID = dt.Rows[0]["GPD_MAKER_ID"] == DBNull.Value ? null : dt.Rows[0]["GPD_MAKER_ID"].ToString();
                    newRow.GPD_CHECKER_ID = dt.Rows[0]["GPD_CHECKER_ID"] == DBNull.Value ? null : dt.Rows[0]["GPD_CHECKER_ID"].ToString();
                    newRow.GPD_ISAUTH = dt.Rows[0]["GPD_ISAUTH"] == DBNull.Value ? false : Convert.ToBoolean(dt.Rows[0]["GPD_ISAUTH"]);
                    return newRow;
                }
                else
                    return null;
            }
            catch (Exception ex)
            {
                message = DateTime.Now.ToString() + Environment.NewLine;
                message += "Exception Occured on onStart" + ex.Message + Environment.NewLine;
                if (ex.InnerException != null)
                {
                    message += "Inner Exception : " + ex.InnerException.Message.ToString() + Environment.NewLine;
                    if (ex.InnerException.InnerException != null)
                    {
                        message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                    }
                }
                return null;
            }
            finally
            {
                if (message != "")
                {
                    LogError("DAL", "GetPRC_MAX_NUM_TODAY", null, message);
                }
            }
        }


        public int PRC_GOV_PRC_DETAIL_Add(PRC_GOV_PRC_DETAIL newRow)
        {
            string message = "";
            try
            {
                int queryResult = 0;
                using (SqlConnection connection = GetSqlConnection())
                {
                    using (SqlCommand command = new SqlCommand("INSERT INTO PRC_GOV_PRC_DETAIL VALUES (@GPD_ID,@GPD_CLIENT,@GPD_TRNSDATE,@GPD_CCY,@GPD_AMOUNT,@GPD_PURPOSE,@GPD_BY_ORDER_COUNTRY,@GPD_PAGE_NO,@GPD_SNO,@GPD_PCODE,@GPD_CCODE,@GPD_BANKREFNO,@GPD_RATE,@GPD_LCYAMOUNT,@GPD_CUSTOMER_NO,@GPD_CUSTOMER_NAME,@GPD_ACCOUNT_NO,@GPD_ACCOUNT_NAME,@GPD_PRINT_STATUS,@GPD_BY_ORDER_CNIC,@GPD_BY_ORDER_IBAN,@GPD_BY_ORDER_IBAN_COUNTRY,@GPD_REMITTING_INSTITUTION,@GPD_BENE_NTN,@GPD_UPLOAD_DATE,@GPD_MODIFIED_DATE,@GPD_PRC_NUMBER,@GPD_COMMENTS,@GPD_MAKER_ID,@GPD_CHECKER_ID,@GPD_ISAUTH)"))
                    {
                        command.Connection = connection;
                        command.CommandType = CommandType.Text;
                        command.Parameters.AddWithValue("@GPD_ID", newRow.GPD_ID);
                        command.Parameters.AddWithValue("@GPD_CLIENT", newRow.GPD_CLIENT == null ? "" : newRow.GPD_CLIENT);
                        if (newRow.GPD_TRNSDATE == null)
                            command.Parameters.AddWithValue("@GPD_TRNSDATE", DBNull.Value);
                        else
                            command.Parameters.AddWithValue("@GPD_TRNSDATE", newRow.GPD_TRNSDATE);
                        command.Parameters.AddWithValue("@GPD_CCY", newRow.GPD_CCY == null ? "" : newRow.GPD_CCY);
                        command.Parameters.AddWithValue("@GPD_AMOUNT", newRow.GPD_AMOUNT == null ? "0" : newRow.GPD_AMOUNT);
                        command.Parameters.AddWithValue("@GPD_PURPOSE", newRow.GPD_PURPOSE == null ? "" : newRow.GPD_PURPOSE);
                        command.Parameters.AddWithValue("@GPD_BY_ORDER_COUNTRY", newRow.GPD_BY_ORDER_COUNTRY == null ? "" : newRow.GPD_BY_ORDER_COUNTRY);
                        command.Parameters.AddWithValue("@GPD_PAGE_NO", newRow.GPD_PAGE_NO == null ? "" : newRow.GPD_PAGE_NO);
                        command.Parameters.AddWithValue("@GPD_SNO", newRow.GPD_SNO == null ? "" : newRow.GPD_SNO);
                        command.Parameters.AddWithValue("@GPD_PCODE", newRow.GPD_PCODE == null ? "" : newRow.GPD_PCODE);
                        command.Parameters.AddWithValue("@GPD_CCODE", newRow.GPD_CCODE == null ? "" : newRow.GPD_CCODE);
                        command.Parameters.AddWithValue("@GPD_BANKREFNO", newRow.GPD_BANKREFNO == null ? "" : newRow.GPD_BANKREFNO);
                        command.Parameters.AddWithValue("@GPD_RATE", newRow.GPD_RATE == null ? "0" : newRow.GPD_RATE);
                        command.Parameters.AddWithValue("@GPD_LCYAMOUNT", newRow.GPD_LCYAMOUNT == null ? "" : newRow.GPD_LCYAMOUNT);
                        command.Parameters.AddWithValue("@GPD_CUSTOMER_NO", newRow.GPD_CUSTOMER_NO == null ? "" : newRow.GPD_CUSTOMER_NO);
                        command.Parameters.AddWithValue("@GPD_CUSTOMER_NAME", newRow.GPD_CUSTOMER_NAME == null ? "" : newRow.GPD_CUSTOMER_NAME);
                        command.Parameters.AddWithValue("@GPD_ACCOUNT_NO", newRow.GPD_ACCOUNT_NO == null ? "" : newRow.GPD_ACCOUNT_NO);
                        command.Parameters.AddWithValue("@GPD_ACCOUNT_NAME", newRow.GPD_ACCOUNT_NAME == null ? "" : newRow.GPD_ACCOUNT_NAME);
                        command.Parameters.AddWithValue("@GPD_PRINT_STATUS", newRow.GPD_PRINT_STATUS == null ? "" : newRow.GPD_PRINT_STATUS);
                        command.Parameters.AddWithValue("@GPD_BY_ORDER_CNIC", newRow.GPD_BY_ORDER_CNIC == null ? "" : newRow.GPD_BY_ORDER_CNIC);
                        command.Parameters.AddWithValue("@GPD_BY_ORDER_IBAN", newRow.GPD_BY_ORDER_IBAN == null ? "" : newRow.GPD_BY_ORDER_IBAN);
                        command.Parameters.AddWithValue("@GPD_BY_ORDER_IBAN_COUNTRY", newRow.GPD_BY_ORDER_IBAN_COUNTRY == null ? "" : newRow.GPD_BY_ORDER_IBAN_COUNTRY);
                        command.Parameters.AddWithValue("@GPD_REMITTING_INSTITUTION", newRow.GPD_REMITTING_INSTITUTION == null ? "" : newRow.GPD_REMITTING_INSTITUTION);
                        command.Parameters.AddWithValue("@GPD_BENE_NTN", newRow.GPD_BENE_NTN == null ? "" : newRow.GPD_BENE_NTN);
                        if (newRow.GPD_UPLOAD_DATE == null)
                            command.Parameters.AddWithValue("@GPD_UPLOAD_DATE", DBNull.Value);
                        else
                            command.Parameters.AddWithValue("@GPD_UPLOAD_DATE", newRow.GPD_UPLOAD_DATE);
                        if (newRow.GPD_MODIFIED_DATE == null)
                            command.Parameters.AddWithValue("@GPD_MODIFIED_DATE", DBNull.Value);
                        else
                            command.Parameters.AddWithValue("@GPD_MODIFIED_DATE", newRow.GPD_MODIFIED_DATE);
                        command.Parameters.AddWithValue("@GPD_PRC_NUMBER", newRow.GPD_PRC_NUMBER == null ? "" : newRow.GPD_PRC_NUMBER);
                        command.Parameters.AddWithValue("@GPD_COMMENTS", newRow.GPD_COMMENTS == null ? "" : newRow.GPD_COMMENTS);
                        command.Parameters.AddWithValue("@GPD_MAKER_ID", newRow.GPD_MAKER_ID == null ? "" : newRow.GPD_MAKER_ID);
                        command.Parameters.AddWithValue("@GPD_CHECKER_ID", newRow.GPD_CHECKER_ID == null ? "" : newRow.GPD_CHECKER_ID);
                        command.Parameters.AddWithValue("@GPD_ISAUTH", newRow.GPD_ISAUTH == null ? false : newRow.GPD_ISAUTH);
                        connection.Open();
                        queryResult = command.ExecuteNonQuery();
                    }
                    connection.Close();
                    return queryResult;
                }
            }
            catch (Exception ex)
            {
                message = DateTime.Now.ToString() + Environment.NewLine;
                message += "Exception Occured on onStart" + ex.Message + Environment.NewLine;
                if (ex.InnerException != null)
                {
                    message += "Inner Exception : " + ex.InnerException.Message.ToString() + Environment.NewLine;
                    if (ex.InnerException.InnerException != null)
                    {
                        message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                    }
                }
                return 0;
            }
            finally
            {
                if (message != "")
                {
                    LogError("DAL", "PRC_GOV_PRC_DETAIL_Add", null, message);
                }
            }
        }
        public int GetMAXID_PRC()
        {
            string message = "";
            try
            {
                SqlConnection conn = GetSqlConnection();
                conn.Open();
                SqlCommand cmd = new SqlCommand("Select (ISNULL(MAX(GPD_ID),0) + 1)GPD_ID from PRC_GOV_PRC_DETAIL", conn);
                SqlDataAdapter da = new SqlDataAdapter(cmd.CommandText, conn);
                DataTable dt = new DataTable();
                da.Fill(dt);
                conn.Close();
                if (dt.Rows.Count > 0)
                {
                    int GPD_ID = dt.Rows[0]["GPD_ID"] == DBNull.Value ? 0 : Convert.ToInt32(dt.Rows[0]["GPD_ID"]);
                    return GPD_ID;
                }
                else
                    return 0;
            }
            catch (Exception ex)
            {
                message = DateTime.Now.ToString() + Environment.NewLine;
                message += "Exception Occured on onStart" + ex.Message + Environment.NewLine;
                if (ex.InnerException != null)
                {
                    message += "Inner Exception : " + ex.InnerException.Message.ToString() + Environment.NewLine;
                    if (ex.InnerException.InnerException != null)
                    {
                        message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                    }
                }
                return 0;
            }
            finally
            {
                if (message != "")
                {
                    LogError("DAL", "GetMAXID_PRC", null, message);
                }
            }
        }



        public static void LogError(string controllername, string functionName, Exception excep = null, string message = "")
        {
            // code starts for creating log file starts here this code will be appended to main menu file

            string complete_path = AppDomain.CurrentDomain.BaseDirectory + "PRC_Logs\\";
            string filename = DateTime.Now.Date.Day + "_" + DateTime.Now.Date.Month + "_" + DateTime.Now.Date.Year + ".txt";
            string path = complete_path + filename;
            bool logfilexheck = false;
            if (System.IO.Directory.Exists(path))
            {
                logfilexheck = true;
            }
            else
            {
                System.IO.DirectoryInfo dr = System.IO.Directory.CreateDirectory(complete_path);
                logfilexheck = true;
            }
            if (logfilexheck)
            {
                System.IO.FileStream fs = new System.IO.FileStream(path, System.IO.FileMode.Append, System.IO.FileAccess.Write);

                // Writing log begins            
                System.IO.StreamWriter writer = new System.IO.StreamWriter(fs);
                writer.WriteLine("---------------------------------------------------------------");
                writer.WriteLine("Time of logging: " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString());
                writer.WriteLine("Class/Controller/Report Name: " + controllername);
                writer.WriteLine("Function Name: " + functionName);
                if (excep != null)
                {

                    writer.WriteLine("Exception Description:");
                    writer.WriteLine(excep.Message);
                    writer.WriteLine("Exception Stack Trace:");
                    writer.WriteLine(excep.StackTrace);
                }
                else
                {
                    if (message != "")
                    {
                        writer.WriteLine("Validation Message :");
                        writer.WriteLine(message);
                    }
                }
                writer.WriteLine("--------------------------------------------------------------- ");
                writer.Close();
                //Writing log ends
                fs.Close();
                //System.Windows.Forms.MessageBox.Show("Error found! check the log at: " + path );
            }

            // code for creating log file ends here
        }

        public DataTable GetTodayEmailDate()
        {
            string message = "";
            try
            {
                string d = DateTime.Now.ToString("dd");
                string m = DateTime.Now.ToString("MM");

                string FinalDate = m + "/" + d;
                SqlConnection conn = GetSqlConnection();
                conn.Open();
                SqlCommand cmd = new SqlCommand("SELECT * FROM PRC_AUTO_EMAILING WHERE PAE_EXECUTION_DATE_N_MONTH = '" + FinalDate + "' AND PAE_STATUS = 0", conn);
                SqlDataAdapter da = new SqlDataAdapter(cmd.CommandText, conn);
                DataTable dt = new DataTable();
                da.Fill(dt);
                conn.Close();
                return dt;
            }
            catch (Exception ex)
            {
                message = DateTime.Now.ToString() + Environment.NewLine;
                message += "Exception Occured on onStart" + ex.Message + Environment.NewLine;
                if (ex.InnerException != null)
                {
                    message += "Inner Exception : " + ex.InnerException.Message.ToString() + Environment.NewLine;
                    if (ex.InnerException.InnerException != null)
                    {
                        message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                    }
                }
                return new DataTable();
            }
            finally
            {
                if (message != "")
                {
                    LogError("DAL", "GetTodayEmailDate", null, message);
                }
            }
        }
        public DataTable GetAllClientforSPRC(DateTime FromDate, DateTime ToDate, string AccountNo)
        {
            string message = "";
            try
            {
                SqlConnection conn = GetSqlConnection();
                conn.Open();
                SqlCommand cmd = new SqlCommand("EXEC PRC_SEND_EMAIL_WITH_PRC @FromDate = '" + FromDate + "', @EndDate = '" + ToDate + "', @AccountNo = '" + AccountNo + "'", conn);
                SqlDataAdapter da = new SqlDataAdapter(cmd.CommandText, conn);
                DataTable dt = new DataTable();
                da.Fill(dt);
                conn.Close();
                return dt;
            }
            catch (Exception ex)
            {
                message = DateTime.Now.ToString() + Environment.NewLine;
                message += "Exception Occured on onStart" + ex.Message + Environment.NewLine;
                if (ex.InnerException != null)
                {
                    message += "Inner Exception : " + ex.InnerException.Message.ToString() + Environment.NewLine;
                    if (ex.InnerException.InnerException != null)
                    {
                        message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                    }
                }
                return new DataTable();
            }
            finally
            {
                if (message != "")
                {
                    LogError("DAL", "GetAllClientforSPRC", null, message);
                }
            }
        }

        public DataTable InitializeDataTableForFurtherRecords()
        {
            string message = "";
            try
            {
                SqlConnection conn = GetSqlConnection();
                conn.Open();
                SqlCommand cmd = new SqlCommand("EXEC PRC_SEND_EMAIL_WITH_PRC @FromDate = '" + DateTime.Now + "', @EndDate = '" + DateTime.Now + "', @AccountNo = '0000000000000000'", conn);
                SqlDataAdapter da = new SqlDataAdapter(cmd.CommandText, conn);
                DataTable dt = new DataTable();
                da.Fill(dt);
                conn.Close();
                return dt;
            }
            catch (Exception ex)
            {
                message = DateTime.Now.ToString() + Environment.NewLine;
                message += "Exception Occured on onStart" + ex.Message + Environment.NewLine;
                if (ex.InnerException != null)
                {
                    message += "Inner Exception : " + ex.InnerException.Message.ToString() + Environment.NewLine;
                    if (ex.InnerException.InnerException != null)
                    {
                        message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                    }
                }
                return new DataTable();
            }
            finally
            {
                if (message != "")
                {
                    LogError("DAL", "GetAllClientforSPRC", null, message);
                }
            }
        }
        public DataTable GetEmailConfigration()
        {
            string message = "";
            try
            {
                SqlConnection conn = GetSqlConnection();
                conn.Open();
                SqlCommand cmd = new SqlCommand("SELECT TOP 1 * FROM PRC_EMAIL_CONFIGRATION WHERE EC_STATUS = 1 AND EC_IS_AUTH = 1", conn);
                SqlDataAdapter da = new SqlDataAdapter(cmd.CommandText, conn);
                DataTable dt = new DataTable();
                da.Fill(dt);
                conn.Close();
                return dt;
            }
            catch (Exception ex)
            {
                message = DateTime.Now.ToString() + Environment.NewLine;
                message += "Exception Occured on onStart" + ex.Message + Environment.NewLine;
                if (ex.InnerException != null)
                {
                    message += "Inner Exception : " + ex.InnerException.Message.ToString() + Environment.NewLine;
                    if (ex.InnerException.InnerException != null)
                    {
                        message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                    }
                }
                return new DataTable();
            }
            finally
            {
                if (message != "")
                {
                    LogError("DAL", "GetEmailConfigration", null, message);
                }
            }
        }
        public DataTable GetAllUniquePartyID()
        {
            string message = "";
            try
            {
                SqlConnection conn = GetSqlConnection();
                conn.Open();
                SqlCommand cmd = new SqlCommand("SELECT DISTINCT PCP_PARTY_ID FROM PRC_CLIENT_PORTFOLIO", conn);
                SqlDataAdapter da = new SqlDataAdapter(cmd.CommandText, conn);
                DataTable dt = new DataTable();
                da.Fill(dt);
                conn.Close();
                return dt;
            }
            catch (Exception ex)
            {
                message = DateTime.Now.ToString() + Environment.NewLine;
                message += "Exception Occured on onStart" + ex.Message + Environment.NewLine;
                if (ex.InnerException != null)
                {
                    message += "Inner Exception : " + ex.InnerException.Message.ToString() + Environment.NewLine;
                    if (ex.InnerException.InnerException != null)
                    {
                        message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                    }
                }
                return new DataTable();
            }
            finally
            {
                if (message != "")
                {
                    LogError("DAL", "GetAllUniquePartyID", null, message);
                }
            }
        }
        public DataTable GetClientAgainstPartyId(string PartyID)
        {
            string message = "";
            try
            {
                SqlConnection conn = GetSqlConnection();
                conn.Open();
                SqlCommand cmd = new SqlCommand("SELECT * FROM PRC_CLIENT_PORTFOLIO WHERE PCP_PARTY_ID = '" + PartyID + "' AND PCP_EMAIL IS NOT NULL AND PCP_SENT_AUTO_EMAILING = 'Enable' AND PCP_STATUS = 'Active'", conn);
                SqlDataAdapter da = new SqlDataAdapter(cmd.CommandText, conn);
                DataTable dt = new DataTable();
                da.Fill(dt);
                conn.Close();
                return dt;
            }
            catch (Exception ex)
            {
                message = DateTime.Now.ToString() + Environment.NewLine;
                message += "Exception Occured on onStart" + ex.Message + Environment.NewLine;
                if (ex.InnerException != null)
                {
                    message += "Inner Exception : " + ex.InnerException.Message.ToString() + Environment.NewLine;
                    if (ex.InnerException.InnerException != null)
                    {
                        message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                    }
                }
                return new DataTable();
            }
            finally
            {
                if (message != "")
                {
                    LogError("DAL", "GetClientAgainstPartyId", null, message);
                }
            }
        }
        public DataTable GetMasterDataForSPRC(string UniqueID)
        {
            string message = "";
            try
            {
                SqlConnection conn = GetSqlConnection();
                conn.Open();
                SqlCommand cmd = new SqlCommand("SELECT * FROM PRC_GOV_PRC_DETAIL_VIEW WHERE GPD_ID =  " + UniqueID + "", conn);
                SqlDataAdapter da = new SqlDataAdapter(cmd.CommandText, conn);
                DataTable dt = new DataTable();
                da.Fill(dt);
                conn.Close();
                return dt;
            }
            catch (Exception ex)
            {
                message = DateTime.Now.ToString() + Environment.NewLine;
                message += "Exception Occured on onStart" + ex.Message + Environment.NewLine;
                if (ex.InnerException != null)
                {
                    message += "Inner Exception : " + ex.InnerException.Message.ToString() + Environment.NewLine;
                    if (ex.InnerException.InnerException != null)
                    {
                        message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                    }
                }
                return new DataTable();
            }
            finally
            {
                if (message != "")
                {
                    LogError("DAL", "GetMasterDataForSPRC", null, message);
                }
            }
        }
        public DataTable GetDetailDataForSPRC(DateTime FromDate, DateTime ToDate, string AccountNo)
        {
            string message = "";
            try
            {
                SqlConnection conn = GetSqlConnection();
                conn.Open();
                //SqlCommand cmd = new SqlCommand("EXEC PRC_SUB_REPORT_FOR_SERVICE_PROC @FromDate = '" + FromDate + "', @ToDate = '" + ToDate + "', @IBAN_NO = '" + AccountNo + "'", conn);
                SqlCommand cmd = new SqlCommand("SELECT * FROM PRC_GOV_PRC_DETAIL WHERE CONVERT(date,GPD_TRNSDATE) >= CONVERT(date,'" + FromDate + "') AND CONVERT(date,GPD_TRNSDATE) <= CONVERT(date,'" + ToDate + "') AND GPD_ACCOUNT_NO IN (" + AccountNo + ")", conn);
                SqlDataAdapter da = new SqlDataAdapter(cmd.CommandText, conn);
                DataTable dt = new DataTable();
                da.Fill(dt);
                conn.Close();
                return dt;
            }
            catch (Exception ex)
            {
                message = DateTime.Now.ToString() + Environment.NewLine;
                message += "Exception Occured on onStart" + ex.Message + Environment.NewLine;
                if (ex.InnerException != null)
                {
                    message += "Inner Exception : " + ex.InnerException.Message.ToString() + Environment.NewLine;
                    if (ex.InnerException.InnerException != null)
                    {
                        message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                    }
                }
                return new DataTable();
            }
            finally
            {
                if (message != "")
                {
                    LogError("DAL", "GetMasterDataForSPRC", null, message);
                }
            }
        }

        public DataTable UpdateEmailStatusOfExecutionDate(int ID)
        {
            string message = "";
            try
            {
                int queryResult = 0;
                using (SqlConnection connection = GetSqlConnection())
                {
                    using (SqlCommand command = new SqlCommand("UPDATE PRC_AUTO_EMAILING SET PAE_STATUS = 1 WHERE PAE_ID = " + ID + ""))
                    {
                        command.Connection = connection;
                        command.CommandType = CommandType.Text;
                        connection.Open();
                        queryResult = command.ExecuteNonQuery();
                    }
                    connection.Close();
                    return null;
                }
            }
            catch (Exception ex)
            {
                message = DateTime.Now.ToString() + Environment.NewLine;
                message += "Exception Occured on onStart" + ex.Message + Environment.NewLine;
                if (ex.InnerException != null)
                {
                    message += "Inner Exception : " + ex.InnerException.Message.ToString() + Environment.NewLine;
                    if (ex.InnerException.InnerException != null)
                    {
                        message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                    }
                }
                return new DataTable();
            }
            finally
            {
                if (message != "")
                {
                    LogError("DAL", "UpdateEmailStatusOfExecutionDate", null, message);
                }
            }
        }

        public DataTable UpdateEmailStatusToFaslse(string executionDate)
        {
            string message = "";
            try
            {
                int queryResult = 0;
                using (SqlConnection connection = GetSqlConnection())
                {
                    using (SqlCommand command = new SqlCommand("UPDATE PRC_AUTO_EMAILING SET PAE_STATUS = 0 WHERE PAE_EXECUTION_DATE_N_MONTH <> '" + executionDate + "'"))
                    {
                        command.Connection = connection;
                        command.CommandType = CommandType.Text;
                        connection.Open();
                        queryResult = command.ExecuteNonQuery();
                    }
                    connection.Close();
                    return null;
                }
            }
            catch (Exception ex)
            {
                message = DateTime.Now.ToString() + Environment.NewLine;
                message += "Exception Occured on onStart" + ex.Message + Environment.NewLine;
                if (ex.InnerException != null)
                {
                    message += "Inner Exception : " + ex.InnerException.Message.ToString() + Environment.NewLine;
                    if (ex.InnerException.InnerException != null)
                    {
                        message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                    }
                }
                return new DataTable();
            }
            finally
            {
                if (message != "")
                {
                    LogError("DAL", "UpdateEmailStatusToFaslse", null, message);
                }
            }
        }
    }
}
