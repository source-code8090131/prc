﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace PRCService.Models
{
    class DATA_LAKE_READER
    {

        public DATA_LAKE_READER()
        {
            string errormessage = "";
            int RowCount = 0;
            try
            {
                DataTable PRCsData = ReadDataLakeTXT();
                if (PRCsData.Rows.Count > 0)
                {
                    errormessage += "*************************************** Process Update to Database Start : " + DateTime.Now + " ***************************************" + Environment.NewLine;
                    foreach (DataRow row in PRCsData.Rows)
                    {
                        PRC_GOV_PRC_DETAIL ListOfPRC = new PRC_GOV_PRC_DETAIL();
                        if (row["contract_ref_no"].ToString().Length > 0)
                        {
                            DAL dal = new DAL();
                            string BankRefNo = row["contract_ref_no"].ToString();
                            bool CheckIfExist = false;
                            ListOfPRC = dal.GetPRC_DATA_Single(BankRefNo);
                            if (ListOfPRC == null)
                            {
                                ListOfPRC = new PRC_GOV_PRC_DETAIL();
                                ListOfPRC.GPD_ID = dal.GetMAXID_PRC();
                                ListOfPRC.GPD_UPLOAD_DATE = DateTime.Now.AddDays(-1);
                                CheckIfExist = false;
                            }
                            else
                            {
                                //ListOfPRC.GPD_MODIFIED_DATE = DateTime.Now;
                                CheckIfExist = true;
                                errormessage += "Bank Reference Number : " + row["contract_ref_no"].ToString() + "  At Client : " + row["ult_beneficiary_nm"].ToString() + " is alredy exist in the system." + Environment.NewLine;
                            }
                            if (CheckIfExist == false)
                            {
                                ListOfPRC.GPD_CLIENT = row["reme_nm"].ToString();
                                string transDate = row["dr_value_date"].ToString();
                                if (transDate != null && transDate != "")
                                {
                                    double transdate;
                                    bool isNumeric = double.TryParse(row["dr_value_date"].ToString(), out transdate);
                                    if (isNumeric)
                                    {
                                        double TRNSDATE = double.Parse(row["dr_value_date"].ToString());
                                        DateTime TRANSDATE = DateTime.FromOADate(TRNSDATE);
                                        ListOfPRC.GPD_TRNSDATE = TRANSDATE;
                                    }
                                    else
                                    {
                                        DateTime TRANSDATE = Convert.ToDateTime(row["dr_value_date"].ToString());
                                        ListOfPRC.GPD_TRNSDATE = TRANSDATE;
                                    }
                                }
                                else
                                {
                                    ListOfPRC.GPD_TRNSDATE = null;
                                }
                                ListOfPRC.GPD_CCY = row["dr_ccy"].ToString();
                                ListOfPRC.GPD_AMOUNT = row["dr_amount"].ToString();
                                ListOfPRC.GPD_PURPOSE = row["payment_details_purpose"].ToString();
                                ListOfPRC.GPD_BY_ORDER_COUNTRY = row["reme_ctry"].ToString();
                                ListOfPRC.GPD_PAGE_NO = "-";
                                ListOfPRC.GPD_SNO = null;
                                ListOfPRC.GPD_PCODE = null;
                                ListOfPRC.GPD_CCODE = null;
                                ListOfPRC.GPD_BANKREFNO = BankRefNo;
                                ListOfPRC.GPD_RATE = row["exchange_rate"].ToString();
                                ListOfPRC.GPD_LCYAMOUNT = row["lcy_equiv"].ToString();
                                ListOfPRC.GPD_CUSTOMER_NO = row["counterparty"].ToString();
                                ListOfPRC.GPD_CUSTOMER_NAME = row["ult_beneficiary_nm"].ToString();
                                ListOfPRC.GPD_ACCOUNT_NO = row["bene_acc_no"].ToString();
                                ListOfPRC.GPD_ACCOUNT_NAME = row["reme_bank_nm"].ToString();
                                ListOfPRC.GPD_BY_ORDER_CNIC = row["remittter_nic_ntn"].ToString();
                                ListOfPRC.GPD_BY_ORDER_IBAN = row["reme_acc_no"].ToString();
                                ListOfPRC.GPD_BY_ORDER_IBAN_COUNTRY = row["reme_ctry"].ToString();
                                ListOfPRC.GPD_REMITTING_INSTITUTION = row["reme_bank_nm"].ToString();
                                ListOfPRC.GPD_BENE_NTN = row["bene_ntn_no"].ToString();
                                ListOfPRC.GPD_MAKER_ID = "Data Lake Feed";
                                ListOfPRC.GPD_CHECKER_ID = "Data Lake Feed";
                                ListOfPRC.GPD_ISAUTH = true;
                                ListOfPRC.GPD_PRC_NUMBER = PRCNumber();
                                RowCount += dal.PRC_GOV_PRC_DETAIL_Add(ListOfPRC);
                                errormessage += "PRC Number : " + ListOfPRC.GPD_PRC_NUMBER + Environment.NewLine;
                            }
                        }
                    }
                    if (RowCount > 0)
                    {
                        errormessage += "Data Uploaded Successfully " + RowCount + " Rows Affected" + Environment.NewLine;
                        errormessage += "*************************************** Process Complete " + DateTime.Now + " ***************************************";
                    }
                }
                else
                {
                    errormessage = "";
                }
            }
            catch (Exception ex)
            {
                LogError("DATA_LAKE_READER", "ReadDataLakeTXT", ex);
            }
            finally
            {
                if (errormessage != "")
                {
                    LogError("DATA_LAKE_READER", "ReadDataLakeTXT", null, errormessage);
                }
            }

        }
        public DataTable ReadDataLakeTXT()
        {
            DataTable dt = new DataTable();
            string errormessage = "";
            try
            {
                string pathToFile = ConfigurationManager.AppSettings["FEED_PATH"];
                if (Directory.Exists(pathToFile))
                {
                    if (hasWriteAccessToFolder(pathToFile))
                    {
                        DirectoryInfo datalakefolder = new DirectoryInfo(pathToFile);
                        foreach (var file in datalakefolder.GetFiles())
                        {
                            errormessage += "File Found : " + file.Name + Environment.NewLine;
                            string FullText = "";
                            List<string> Lines = new List<string>();
                            using (StreamReader sr = new StreamReader(file.FullName))
                            {
                                while (sr.Peek() >= 0)
                                {
                                    string ThisLine = sr.ReadLine();
                                    FullText += ThisLine + Environment.NewLine;
                                    Lines.Add(ThisLine);
                                }
                            }
                            for (int i = 1; i <= Lines.Count - 2; i++)
                            {
                                string EachLine = Lines[i].ToString();
                                if (EachLine != null && EachLine != "")
                                {

                                    string[] Values;
                                    if (i == 1)
                                    {
                                        string[] Headers = EachLine.Split('~');
                                        foreach (string header in Headers)
                                        {
                                            if (!dt.Columns.Contains(header))
                                                dt.Columns.Add(header);
                                        }
                                    }
                                    else
                                    {
                                        if (dt.Columns.Count > 0)
                                        {
                                            Values = EachLine.Split('~');
                                            DataRow newRow = dt.Rows.Add();
                                            for (int eachcolindex = 0; eachcolindex < dt.Columns.Count; eachcolindex++)
                                            {
                                                newRow[eachcolindex] = Values[eachcolindex];
                                            }
                                        }
                                        else
                                        {
                                            errormessage += "No Headers Found in the DataTable , Problem while reading headers from Line No : " + (i - 1) + "." + Environment.NewLine;
                                        }
                                    }
                                }
                                else
                                {
                                    errormessage += "Some Problem Occurred While Reading Line No : " + i + ", Found null or empty string." + Environment.NewLine;
                                }
                            }
                            errormessage += "Reading File Process Completed, " + dt.Rows.Count + " total records found in files yet." + Environment.NewLine;
                            if (File.Exists(pathToFile + "\\Archive\\" + file.Name))
                            {
                                string LastModifiedDateTime = File.GetLastWriteTime(pathToFile + "\\Archive\\" + file.Name).ToString("ddMMyyyyHHmmss");
                                File.Move(pathToFile + "\\Archive\\" + file.Name, pathToFile + "\\Archive\\" + LastModifiedDateTime + "_" + file.Name);
                                errormessage += "File with the same name already exists in Archive folder, hence previous file is renamed to : " + LastModifiedDateTime + "_" + file.Name + Environment.NewLine;
                            }
                            Directory.Move(file.FullName, pathToFile + "\\Archive\\" + file.Name);
                            errormessage += "File Moved To \\Archive\\" + file.Name + Environment.NewLine;
                        }
                        return dt;
                    }
                    else
                    {
                        LogError("DATA_LAKE_READER", "ReadDataLakeTXT", null, "Error While trying to access" + pathToFile);
                        return dt;
                    }
                }
                else
                {
                    LogError("DATA_LAKE_READER", "ReadDataLakeTXT", null, "Directory To Read Data Lake Feed Not Found");
                    return dt;
                }
            }
            catch (Exception ex)
            {
                LogError("DATA_LAKE_READER", "ReadDataLakeTXT", ex);
                return dt;
            }
            finally
            {
                if (errormessage != "")
                {
                    LogError("DATA_LAKE_READER", "ReadDataLakeTXT", null, errormessage);
                }
            }
        }


        private bool hasWriteAccessToFolder(string folderPath)
        {
            try
            {
                // Attempt to get a list of security permissions from the folder. 
                // This will raise an exception if the path is read only or do not have access to view the permissions. 
                System.Security.AccessControl.DirectorySecurity ds = Directory.GetAccessControl(folderPath);
                return true;
            }
            catch (UnauthorizedAccessException ex)
            {
                LogError("DATA_LAKE_READER", "hasWriteAccessToFolder", ex);
                return false;
            }
        }
        public static void LogError(string controllername, string functionName, Exception excep = null, string message = "")
        {
            // code starts for creating log file starts here this code will be appended to main menu file

            string complete_path = AppDomain.CurrentDomain.BaseDirectory + "\\PRC_Logs\\";
            string filename = DateTime.Now.Date.Day + "_" + DateTime.Now.Date.Month + "_" + DateTime.Now.Date.Year + ".txt";
            string path = complete_path + filename;
            bool logfilexheck = false;
            if (System.IO.Directory.Exists(path))
            {
                logfilexheck = true;
            }
            else
            {
                System.IO.DirectoryInfo dr = System.IO.Directory.CreateDirectory(complete_path);
                logfilexheck = true;
            }
            if (logfilexheck)
            {
                System.IO.FileStream fs = new System.IO.FileStream(path, System.IO.FileMode.Append, System.IO.FileAccess.Write);

                // Writing log begins            
                System.IO.StreamWriter writer = new System.IO.StreamWriter(fs);
                writer.WriteLine("---------------------------------------------------------------");
                writer.WriteLine("Time of logging: " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString());
                writer.WriteLine("Class/Controller/Report Name: " + controllername);
                writer.WriteLine("Function Name: " + functionName);
                if (excep != null)
                {

                    writer.WriteLine("Exception Description:");
                    writer.WriteLine(excep.Message);
                    writer.WriteLine("Exception Stack Trace:");
                    writer.WriteLine(excep.StackTrace);
                }
                else
                {
                    if (message != "")
                    {
                        writer.WriteLine("Validation Message :");
                        writer.WriteLine(message);
                    }
                }
                writer.WriteLine("--------------------------------------------------------------- ");
                writer.Close();
                //Writing log ends
                fs.Close();
                //System.Windows.Forms.MessageBox.Show("Error found! check the log at: " + path );
            }

            // code for creating log file ends here
        }

        private string PRCNumber()
        {
            int y = DateTime.Now.Year;
            int m = DateTime.Now.Month;
            int d = DateTime.Now.Day;
            string PRCNumber = "";
            int daysCount = 0;
            for (int i = 1; i <= m; i++)
            {
                string getDate = "0" + i + "/" + (d) + "/" + y;
                DateTime gDate = DateTime.Now;

                if (i == 2)
                {
                    string getDateX = "0" + i + "/" + 28 + "/" + y;
                    gDate = Convert.ToDateTime(getDateX);
                }
                else
                {
                    if (d == 31 && (i == 4 || i == 6 || i == 9 || i == 11))
                    {

                        getDate = "0" + i + "/" + 30 + "/" + y;
                    }
                    gDate = Convert.ToDateTime(getDate);
                }

                int DaysInMonth = 0;

                if (i != m)
                {
                    DaysInMonth = DateTime.DaysInMonth(gDate.Year, gDate.Month);
                }
                else
                {
                    DateTime getCurrentDate = DateTime.Now;
                    DaysInMonth = Convert.ToDateTime(getCurrentDate).Day;
                }
                daysCount = daysCount + DaysInMonth;
            }
            int getdtRowCount = 0;
            string TestsubStringx = "000";
            //// - 1 Day Condition;
            daysCount -= 1;
            int TestlengValx = Convert.ToInt32(Convert.ToString(daysCount).Length);
            string TestgetValdaysCount = TestsubStringx.Substring(0, (3 - TestlengValx)) + daysCount;
            string CheckPRCNumber = "CITI-PRC-" + DateTime.Now.ToString("yyyy") + "-" + TestgetValdaysCount;
            DAL dal = new DAL();
            PRC_GOV_PRC_DETAIL Entity = dal.GetPRC_MAX_NUM_TODAY(CheckPRCNumber);
            if (Entity != null)
            {
                string getMaxValue = Entity.GPD_PRC_NUMBER;
                if (!string.IsNullOrEmpty(getMaxValue))
                {
                    string getVals = getMaxValue.Substring(17, 6);

                    int getMaxVal = Convert.ToInt32(getVals);

                    getdtRowCount = getMaxVal + 1;
                }
                else
                {
                    getdtRowCount = 1;
                }
            }
            else
            {
                getdtRowCount = 1;
            }
            string subStringx = "000";
            int lengValx = Convert.ToInt32(Convert.ToString(daysCount).Length);
            string getValdaysCount = subStringx.Substring(0, (3 - lengValx)) + daysCount;
            int lengVal = Convert.ToInt32(Convert.ToString(getdtRowCount).Length);
            string subString = "000000";
            string getVal = subString.Substring(0, (6 - lengVal));
            PRCNumber = "CITI-PRC-" + DateTime.Now.ToString("yyyy") + "-" + getValdaysCount + getVal + getdtRowCount;
            return PRCNumber;
        }
    }
}
