﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCService.Models
{
    public class PRC_GOV_PRC_DETAIL
    {
        public int GPD_ID { get; set; }
        public string GPD_CLIENT { get; set; }
        public Nullable<System.DateTime> GPD_TRNSDATE { get; set; }
        public string GPD_CCY { get; set; }
        public string GPD_AMOUNT { get; set; }
        public string GPD_PURPOSE { get; set; }
        public string GPD_BY_ORDER_COUNTRY { get; set; }
        public string GPD_PAGE_NO { get; set; }
        public string GPD_SNO { get; set; }
        public string GPD_PCODE { get; set; }
        public string GPD_CCODE { get; set; }
        public string GPD_BANKREFNO { get; set; }
        public string GPD_RATE { get; set; }
        public string GPD_LCYAMOUNT { get; set; }
        public string GPD_CUSTOMER_NO { get; set; }
        public string GPD_CUSTOMER_NAME { get; set; }
        public string GPD_ACCOUNT_NO { get; set; }
        public string GPD_ACCOUNT_NAME { get; set; }
        public string GPD_PRINT_STATUS { get; set; }
        public string GPD_BY_ORDER_CNIC { get; set; }
        public string GPD_BY_ORDER_IBAN { get; set; }
        public string GPD_BY_ORDER_IBAN_COUNTRY { get; set; }
        public string GPD_REMITTING_INSTITUTION { get; set; }
        public string GPD_BENE_NTN { get; set; }
        public Nullable<System.DateTime> GPD_UPLOAD_DATE { get; set; }
        public Nullable<System.DateTime> GPD_MODIFIED_DATE { get; set; }
        public string GPD_PRC_NUMBER { get; set; }
        public string GPD_COMMENTS { get; set; }
        public string GPD_MAKER_ID { get; set; }
        public string GPD_CHECKER_ID { get; set; }
        public Nullable<bool> GPD_ISAUTH { get; set; }
    }
}
