﻿using HiQPdf;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Web.Hosting;

namespace PRCService.Models
{
    class AUTO_EMAILING
    {
        public AUTO_EMAILING()
        {
            string message = "";
            try
            {
                DAL dal = new DAL();
                string complete_path = AppDomain.CurrentDomain.BaseDirectory + "\\Attachments\\";
                if (System.IO.Directory.Exists(complete_path))
                {

                }
                else
                {
                    System.IO.DirectoryInfo dr = System.IO.Directory.CreateDirectory(complete_path);
                }
                int ExecutionID = 0;
                string ClientAccountNo = "";
                string ExecutionDate = "";
                string OutputPath = AppDomain.CurrentDomain.BaseDirectory + "\\Attachments\\" + "OutputFile_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".pdf";
                DataTable EmailDate = dal.GetTodayEmailDate();
                if (EmailDate.Rows.Count > 0)
                {
                    foreach (DataRow item in EmailDate.Rows)
                    {
                        ExecutionDate = "";
                        ExecutionDate = item["PAE_EXECUTION_DATE_N_MONTH"].ToString();
                        string FromDateRange = item["PAE_DATA_RANGE_FROM_DATE"].ToString();
                        string ToDateRange = item["PAE_DATA_RANGE_TO"].ToString();
                        ExecutionID = Convert.ToInt32(item["PAE_ID"].ToString());
                        if (ExecutionDate != null)
                        {
                            DataTable EmailConfig = dal.GetEmailConfigration();
                            foreach (DataRow emailconfig in EmailConfig.Rows)
                            {
                                if (EmailConfig.Rows.Count > 0)
                                {
                                    string EMAIL_HOST = emailconfig["EC_SEREVER_HOST"].ToString();
                                    string EMAIL_CREDENTIAL_ID = emailconfig["EC_CREDENTIAL_ID"].ToString();
                                    string EMAIL_PORT = emailconfig["EC_EMAIL_PORT"].ToString();
                                    #region To Get Perfect Date 
                                    DateTime FromDate = DateTime.Now;
                                    DateTime ToDate = DateTime.Now;
                                    if (ExecutionDate.Contains("01/"))
                                    {
                                        int y = DateTime.Now.Year -1;
                                        string Fdate = FromDateRange + "/" + y;
                                        FromDate = Convert.ToDateTime(Fdate);
                                        string Tdate = ToDateRange + "/" + y;
                                        ToDate = Convert.ToDateTime(Tdate);
                                    }
                                    else
                                    {
                                        int y = DateTime.Now.Year;
                                        string Fdate = FromDateRange + "/" + y;
                                        FromDate = Convert.ToDateTime(Fdate);
                                        string Tdate = ToDateRange + "/" + y;
                                        ToDate = Convert.ToDateTime(Tdate);
                                    }
                                    #endregion
                                    DataTable PartyId = dal.GetAllUniquePartyID();
                                    if (PartyId.Rows.Count > 0)
                                    {
                                        foreach (DataRow partyid in PartyId.Rows)
                                        {
                                            string PARTY_ID = partyid["PCP_PARTY_ID"].ToString();
                                            if (PARTY_ID != null && PARTY_ID != "")
                                            {
                                                DataTable ClientDetails = dal.GetClientAgainstPartyId(PARTY_ID);
                                                if (ClientDetails.Rows.Count > 0)
                                                {
                                                    ClientAccountNo = "";
                                                    string CLIENT_EMAIL = "";
                                                    string CLIENT_NOTIFICATION_EMAIL = "";
                                                    string ACCOUNT_NO = "";
                                                    foreach (DataRow client in ClientDetails.Rows)
                                                    {
                                                        if (ClientAccountNo == "")
                                                            ClientAccountNo = "'" + client["PCP_IBAN"].ToString() + "'";
                                                        else
                                                            ClientAccountNo += ",'" + client["PCP_IBAN"].ToString() + "'";
                                                        CLIENT_EMAIL += client["PCP_EMAIL"].ToString();
                                                        CLIENT_NOTIFICATION_EMAIL += client["PCP_NOTIFICATION_EMAIL"].ToString();
                                                        ACCOUNT_NO = client["PCP_IBAN"].ToString();
                                                        if (ClientDetails.Rows.Count > 0 && CLIENT_NOTIFICATION_EMAIL != "" && CLIENT_NOTIFICATION_EMAIL != null)
                                                        {
                                                            char CheckClientNotificationEmail = CLIENT_NOTIFICATION_EMAIL[CLIENT_NOTIFICATION_EMAIL.Length - 1];
                                                            if (CheckClientNotificationEmail != ',')
                                                            {
                                                                CLIENT_NOTIFICATION_EMAIL += ",";
                                                            }
                                                        }
                                                        if (ClientDetails.Rows.Count > 0 && CLIENT_EMAIL != "" && CLIENT_EMAIL != null)
                                                        {
                                                            char CheckClientEmail = CLIENT_EMAIL[CLIENT_EMAIL.Length - 1];
                                                            if (CheckClientEmail != ',')
                                                            {
                                                                CLIENT_EMAIL += ",";
                                                            }
                                                        }   
                                                    }


                                                    DataTable ALLSPRC = dal.InitializeDataTableForFurtherRecords();
                                                    foreach (DataRow EachAccountData in ClientDetails.Rows)
                                                    {
                                                        DataTable EachData = dal.GetAllClientforSPRC(FromDate, ToDate, EachAccountData["PCP_IBAN"].ToString());
                                                        if (EachData.Rows.Count > 0)
                                                        {
                                                            foreach (DataRow EachDataEachRow in EachData.Rows)
                                                            {
                                                                int GPD_UNIQUE_ID = Convert.ToInt32(EachDataEachRow["GPD_ID"].ToString());
                                                                if (ALLSPRC.AsEnumerable().Any(row => GPD_UNIQUE_ID == row.Field<int>("GPD_ID")) == false)
                                                                {
                                                                    DataRow ALLPR_NEW_ROW = ALLSPRC.NewRow();
                                                                    ALLPR_NEW_ROW.ItemArray = EachDataEachRow.ItemArray;
                                                                    ALLSPRC.Rows.Add(ALLPR_NEW_ROW);
                                                                }
                                                            }
                                                        }
                                                    }
                                                   
                                                    if (ALLSPRC.Rows.Count > 0)
                                                    {
                                                        foreach (DataRow EachSPRC in ALLSPRC.Rows.Cast<DataRow>().Take(1))
                                                        {
                                                            string UNIQUE_ID = EachSPRC["GPD_ID"].ToString();
                                                            DataTable MasterDataForSPRC = dal.GetMasterDataForSPRC(UNIQUE_ID);
                                                            if (MasterDataForSPRC.Rows.Count > 0)
                                                            {
                                                                foreach (DataRow MasterSPRCData in MasterDataForSPRC.Rows)
                                                                {
                                                                    var PRCReportView = MasterSPRCData.Table;
                                                                    string GPD_CUSTOMER_NAME = MasterSPRCData["GPD_CUSTOMER_NAME"].ToString();
                                                                    string GPD_BENE_NTN = MasterSPRCData["GPD_BENE_NTN"].ToString();
                                                                    string GPD_ACCOUNT_NO = MasterSPRCData["GPD_ACCOUNT_NO"].ToString();
                                                                    DataTable DetailDataForSPRC = dal.GetDetailDataForSPRC(FromDate, ToDate, ClientAccountNo);
                                                                    if (DetailDataForSPRC.Rows.Count > 0)
                                                                    {
                                                                        var PRCSubReportSource = DetailDataForSPRC;
                                                                        #region Replace Html File with Orignal data
                                                                        ConvertHtmlToPDFCustomm(OutputPath, PopulateDataIntoHtml(MasterSPRCData, DetailDataForSPRC, FromDate, ToDate));
                                                                        #endregion
                                                                        if (File.Exists(OutputPath))
                                                                        {
                                                                            try
                                                                            {
                                                                                using (MailMessage mail = new MailMessage())
                                                                                {
                                                                                    //ReportDocument rd = new ReportDocument();
                                                                                    string ToEmail = CLIENT_EMAIL;
                                                                                    string FromEmail = EMAIL_CREDENTIAL_ID;
                                                                                    string HostName = EMAIL_HOST;
                                                                                    int EmailPort = Convert.ToInt32(EMAIL_PORT);
                                                                                    NetworkCredential NetworkCredentials = new System.Net.NetworkCredential();
                                                                                    NetworkCredentials.UserName = FromEmail;
                                                                                    ////#if dev
                                                                                    //NetworkCredentials.Password = "Test@@123";
                                                                                    mail.From = new MailAddress(FromEmail);
                                                                                    //To Emails
                                                                                    if (CLIENT_EMAIL != null && CLIENT_EMAIL != "")
                                                                                    {
                                                                                        if (CLIENT_EMAIL.Contains(","))
                                                                                        {
                                                                                            CLIENT_EMAIL = CLIENT_EMAIL.Remove(CLIENT_EMAIL.Length - 1);
                                                                                            string[] To_Email = CLIENT_EMAIL.Split(',');
                                                                                            foreach (string MultiEmail in To_Email)
                                                                                            {
                                                                                                mail.To.Add(new MailAddress(MultiEmail));
                                                                                            }
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            mail.To.Add(new MailAddress(CLIENT_EMAIL));
                                                                                        }
                                                                                    }
                                                                                    //CC Emails
                                                                                    if (CLIENT_NOTIFICATION_EMAIL != null && CLIENT_NOTIFICATION_EMAIL != "")
                                                                                    {
                                                                                        if (CLIENT_NOTIFICATION_EMAIL.Contains(","))
                                                                                        {
                                                                                            CLIENT_NOTIFICATION_EMAIL = CLIENT_NOTIFICATION_EMAIL.Remove(CLIENT_NOTIFICATION_EMAIL.Length - 1);
                                                                                            string[] CC_Email = CLIENT_NOTIFICATION_EMAIL.Split(',');
                                                                                            foreach (string MultiEmail in CC_Email)
                                                                                            {
                                                                                                mail.CC.Add(MultiEmail);
                                                                                            }
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            mail.CC.Add(CLIENT_NOTIFICATION_EMAIL);
                                                                                        }
                                                                                    }
                                                                                    mail.Subject = "(Secure) PRC Certificate Request - " + Convert.ToDateTime(FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy") + " To " + Convert.ToDateTime(ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy");
                                                                                    mail.Body = "<div class='row' style='background-color:#E9ECEF;'><div class='col-md-2'></div><div class='col-md-8'><div class='container' style='background-color:#FFFFFF'><div class='container-fluid'><p style='font-family:Bahnschrift;font-weight:lighter;font-size:15px'>Dear Client,</p><p style='font-family:Bahnschrift;font-weight:lighter;font-size:15px'>Please find attached here with the S-PRC (Statement of Proceeds Realization Certificates) for transactions covering the period from " + FromDate + " To " + ToDate + " with respect to Account number " + ACCOUNT_NO + ".<br />  </p><p style='font-family:Bahnschrift;font-size:15px;font-weight:lighter;'>For any queries, please reach out to below contacts:<br /> </p>	<p style='font-family:Bahnschrift;font-size:15px;font-weight:lighter;'>Email: citiservice.pakistan@citi.com <br /> </p><p style='font-family:Arial;font-size:13px'>Contact No: 021-111-777-777 </p>	<p style='font-family:Bahnschrift;font-size:15px;font-weight:lighter;'><b>Note</b>: This is a system generated email, Please don't reply. <br /> </p><p style='font-family:Bahnschrift;font-size:15px;font-weight:lighter;' ><b> Disclaimer </b>: This email is confidential, may be legally privileged, and is for the intended recipient only.Access, disclosure, copying, distribution, or reliance on any of it by anyone else is prohibited and may be a criminal offence.Please delete if obtained in error and email confirmation to the sender.<br /> </p></div></div></div><div class='col-md-2'></div></div>";
                                                                                    string FileName = "SPRC_Summary_" + DateTime.Now.ToString("yyyyMMdd");
                                                                                    string oldFilePath = OutputPath; // Full path of old file
                                                                                    string newFilePath = AppDomain.CurrentDomain.BaseDirectory + "\\Attachments\\" + FileName + ".pdf"; // Full path of new file
                                                                                    if (File.Exists(newFilePath))
                                                                                    {
                                                                                        File.Delete(newFilePath);
                                                                                    }
                                                                                    File.Move(oldFilePath, newFilePath);
                                                                                    mail.Attachments.Add(new Attachment(newFilePath));
                                                                                    mail.IsBodyHtml = true;
                                                                                    using (SmtpClient smtp = new SmtpClient(HostName, EmailPort))
                                                                                    {
                                                                                        smtp.Credentials = NetworkCredentials;
                                                                                        //if dev
                                                                                        //smtp.EnableSsl = true;
                                                                                        smtp.Send(mail);
                                                                                    }
                                                                                    LogError("AUTO_EMAILING", "AUTO_EMAILING", null, "Successfully sent email to the client having Party Id # " + PARTY_ID + " Duration Start Date : " + FromDate + " End Date : " + ToDate + ".");
                                                                                }
                                                                            }
                                                                            catch (Exception ex)
                                                                            {
                                                                                message = ex.Message + Environment.NewLine;
                                                                                if (ex.InnerException != null)
                                                                                {
                                                                                    message += ex.InnerException.Message;
                                                                                    if (ex.InnerException.InnerException != null)
                                                                                    {
                                                                                        message += ex.InnerException.InnerException.Message;
                                                                                    }
                                                                                }
                                                                                message += Environment.NewLine + "On provided Account No # " + ACCOUNT_NO + ".";
                                                                                LogError("AUTO_EMAILING", "AUTO_EMAILING", null, message);
                                                                            }
                                                                        }
                                                                        else
                                                                        {
                                                                            message = "Error While Generating Report On Start Date : " + FromDate + " End Date : " + ToDate + " UNIQUE MASTER ID : " + UNIQUE_ID;
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        message = "No PRC Found on the selectd Start Date : " + FromDate + " End Date : " + ToDate + " and Account No # "+ ACCOUNT_NO + ".";
                                                                        LogError("AUTO_EMAILING", "AUTO_EMAILING", null, message);
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        message = "No records were found in the system against Account No # "+ ACCOUNT_NO + ". Hence, no email was sent to the client." + Environment.NewLine;
                                                        LogError("AUTO_EMAILING", "AUTO_EMAILING", null, message);
                                                    }
                                                }
                                                else
                                                {
                                                    message = "No Client found in the system on Party Id # "+ PARTY_ID + "." + Environment.NewLine;
                                                    LogError("AUTO_EMAILING", "AUTO_EMAILING", null, message);
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        message = "No Client found in the system." + Environment.NewLine;
                                        LogError("AUTO_EMAILING", "AUTO_EMAILING", null, message);
                                    }
                                }
                                else
                                {
                                    message = "No Email Configration found in the system." + Environment.NewLine;
                                    LogError("AUTO_EMAILING", "AUTO_EMAILING", null, message);
                                }
                            }
                        }
                    }
                    dal.UpdateEmailStatusOfExecutionDate(ExecutionID);
                    dal.UpdateEmailStatusToFaslse(ExecutionDate);
                }
                
            }
            catch (Exception ex)
            {
                message = "Exception Occurred - " + ex.Message;
                if (ex.InnerException != null)
                {
                    message += "  Inner Exception - " + ex.InnerException.Message;
                    if (ex.InnerException.InnerException != null)
                    {
                        message += "  Inner Inner Exception - " + ex.InnerException.InnerException.Message;
                    }
                }
            }
            if (message != "")
            {
                LogError("AUTO_EMAILING", "AUTO_EMAILING", null, message);
            }
        }
        public static string GenerateSPRCNumber()
        {
            int y = DateTime.Now.Year;
            int m = DateTime.Now.Month;
            int d = DateTime.Now.Day;
            string PRCNumber = "";
            int daysCount = 0;
            for (int i = 1; i <= m; i++)
            {
                string getDate = "";
                if (i < 10)
                    getDate = "0" + i + "/" + (d) + "/" + y;
                else
                    getDate = i + "/" + (d) + "/" + y;
                DateTime gDate = DateTime.Now;

                if (i == 2)
                {
                    string getDateX = "0" + i + "/" + 28 + "/" + y;
                    gDate = Convert.ToDateTime(getDateX);
                }
                else
                {
                    if (d == 31 && (i == 4 || i == 6 || i == 9 || i == 11))
                    {
                        getDate = "0" + i + "/" + 30 + "/" + y;
                    }
                    gDate = Convert.ToDateTime(getDate);
                }

                int DaysInMonth = 0;

                if (i != m)
                {
                    DaysInMonth = DateTime.DaysInMonth(gDate.Year, gDate.Month);
                }
                else
                {
                    DateTime getCurrentDate = DateTime.Now;
                    DaysInMonth = Convert.ToDateTime(getCurrentDate).Day;
                }
                daysCount = daysCount + DaysInMonth;
            }
            int getdtRowCount = 1;
            string subStringx = "000";
            int lengValx = Convert.ToInt32(Convert.ToString(daysCount).Length);
            string getValdaysCount = subStringx.Substring(0, (3 - lengValx)) + daysCount;
            int lengVal = Convert.ToInt32(Convert.ToString(getdtRowCount).Length);
            string subString = "000000";
            string getVal = subString.Substring(0, (6 - lengVal));
            PRCNumber = "CITI-SPRC-" + DateTime.Now.ToString("yyyy") + "-" + getValdaysCount + getVal + getdtRowCount;
            return PRCNumber;
        }
        public static void LogError(string controllername, string functionName, Exception excep = null, string message = "")
        {
            // code starts for creating log file starts here this code will be appended to main menu file

            string complete_path = AppDomain.CurrentDomain.BaseDirectory + "\\PRC_Logs\\";
            string filename = DateTime.Now.Date.Day + "_" + DateTime.Now.Date.Month + "_" + DateTime.Now.Date.Year + ".txt";
            string path = complete_path + filename;
            bool logfilexheck = false;
            if (System.IO.Directory.Exists(path))
            {
                logfilexheck = true;
            }
            else
            {
                System.IO.DirectoryInfo dr = System.IO.Directory.CreateDirectory(complete_path);
                logfilexheck = true;
            }
            if (logfilexheck)
            {
                System.IO.FileStream fs = new System.IO.FileStream(path, System.IO.FileMode.Append, System.IO.FileAccess.Write);

                // Writing log begins            
                System.IO.StreamWriter writer = new System.IO.StreamWriter(fs);
                writer.WriteLine("---------------------------------------------------------------");
                writer.WriteLine("Time of logging: " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString());
                writer.WriteLine("Class/Controller/Report Name: " + controllername);
                writer.WriteLine("Function Name: " + functionName);
                if (excep != null)
                {

                    writer.WriteLine("Exception Description:");
                    writer.WriteLine(excep.Message);
                    writer.WriteLine("Exception Stack Trace:");
                    writer.WriteLine(excep.StackTrace);
                }
                else
                {
                    if (message != "")
                    {
                        writer.WriteLine("Validation Message :");
                        writer.WriteLine(message);
                    }
                }
                writer.WriteLine("--------------------------------------------------------------- ");
                writer.Close();
                //Writing log ends
                fs.Close();
                //System.Windows.Forms.MessageBox.Show("Error found! check the log at: " + path );
            }

            // code for creating log file ends here
        }

        public string PopulateDataIntoHtml(DataRow masterRow, DataTable detaildata, DateTime FromDate, DateTime Todate)
        {
            string FinalHtmlToReturn = "";
            if (detaildata.Rows.Count > 0)
            {
                string fileName = AppDomain.CurrentDomain.BaseDirectory + "TemplateReportFile.txt";
                string html = File.ReadAllText(fileName);
                string CUSTOMER_NAME = masterRow["GPD_CUSTOMER_NAME"].ToString();
                string GPD_BENE_NTN = masterRow["GPD_BENE_NTN"].ToString();
                string GPD_ACCOUNT_NO = masterRow["GPD_ACCOUNT_NO"].ToString();
                html = html.Replace("@@YearOnly", Convert.ToString(DateTime.Now.Year));
                html = html.Replace("@@GPD_CUSTOMER_NAME", CUSTOMER_NAME);
                html = html.Replace("@@GPD_BENE_NTN", GPD_BENE_NTN);
                html = html.Replace("@@GPD_ACCOUNT_NO", GPD_ACCOUNT_NO);
                html = html.Replace("@@GPD_SPRC_NUMBER", GenerateSPRCNumber());
                html = html.Replace("@@REQUEST_ISSUE_DATE", DateTime.Now.ToString("dd-MMMM-yyyy"));
                html = html.Replace("@@FROMDATE", FromDate.ToString("dd-MMM-yyyy"));
                html = html.Replace("@@TODATE", Todate.ToString("dd-MMM-yyyy"));
                string LogoImagePath = AppDomain.CurrentDomain.BaseDirectory + "logo.jpg";
                string ImageBase64 = ImageToBase64(LogoImagePath);
                html = html.Replace("@@logoimagecompletepath@@", ImageBase64);
                string HtmlBeforeTable = html.Substring(0, html.IndexOf("CUSTOM DETAIL TABLE EACH ROW START HERE"));
                string HtmlAfterTable = html.Substring(html.IndexOf("CUSTOM DETAIL TABLE EACH ROW END HERE"));

                FinalHtmlToReturn = HtmlBeforeTable;

                int CssToAddCount = 0;
                int TopElementValue = 488;
                //string MasterFileLisTable = AppDomain.CurrentDomain.BaseDirectory + "TemplateReportFileTableBorder.txt";
                string MasterListhtml = "";
                int SNO = 0;
                foreach (DataRow EachDetailRow in detaildata.Rows)
                {
                    TopElementValue = 488;
                    string DetailfileName = AppDomain.CurrentDomain.BaseDirectory + "TemplateReportFileDetail.txt";
                    string Detailhtml = File.ReadAllText(DetailfileName);
                    #region Populating Table html
                    if (CssToAddCount > 0)
                    {
                        TopElementValue = TopElementValue + (CssToAddCount * 81);
                        Detailhtml = Detailhtml.Replace("@@DetialTopValue", TopElementValue.ToString());
                        MasterListhtml += "<div style='position: absolute;z-index: 25;z-index:99;top:" + TopElementValue + "px;left:8px;border-color:#000000;border-style:solid;border-width:0px;border-top-width:1px;width:1165px;'></div>" + Environment.NewLine;
                        MasterListhtml += "<div style='position: absolute;z-index: 25;z-index:99;top:336px;left:8px;border-color:#000000;border-style:solid;border-width:0px;border-left-width:1px;clip:rect(0px,1px,152px,0px);height:152px;'></div>" + Environment.NewLine;
                        MasterListhtml += "<div style='position: absolute;z-index: 25;z-index:99;top:336px;left:8px;border-color:#000000;border-style:solid;border-width:0px;border-top-width:1px;width:1165px;'></div>" + Environment.NewLine;
                        MasterListhtml += "<div style='position: absolute;z-index: 25;z-index:99;top:336px;left:1172px;border-color:#000000;border-style:solid;border-width:0px;border-left-width:1px;clip:rect(0px,1px,152px,0px);height:152px;'></div>" + Environment.NewLine;
                        MasterListhtml += "<div style='position: absolute;z-index: 25;z-index:99;top:" + TopElementValue + "px;left:8px;border-color:#000000;border-style:solid;border-width:0px;border-left-width:1px;clip:rect(0px,1px,80px,0px);height:80px;'></div>" + Environment.NewLine;
                        MasterListhtml += "<div style='position: absolute;z-index: 25;z-index:99;top:" + TopElementValue + "px;left:45px;border-color:#000000;border-style:solid;border-width:0px;border-left-width:1px;clip:rect(0px,1px,80px,0px);height:80px;'></div>" + Environment.NewLine;
                        MasterListhtml += "<div style='position: absolute;z-index: 25;z-index:99;top:" + TopElementValue + "px;left:145px;border-color:#000000;border-style:solid;border-width:0px;border-left-width:1px;clip:rect(0px,1px,80px,0px);height:80px;'></div>" + Environment.NewLine;
                        MasterListhtml += "<div style='position: absolute;z-index: 25;z-index:99;top:" + TopElementValue + "px;left:245px;border-color:#000000;border-style:solid;border-width:0px;border-left-width:1px;clip:rect(0px,1px,80px,0px);height:80px;'></div>" + Environment.NewLine;
                        MasterListhtml += "<div style='position: absolute;z-index: 25;z-index:99;top:" + TopElementValue + "px;left:321px;border-color:#000000;border-style:solid;border-width:0px;border-left-width:1px;clip:rect(0px,1px,80px,0px);height:80px;'></div>" + Environment.NewLine;
                        MasterListhtml += "<div style='position: absolute;z-index: 25;z-index:99;top:" + TopElementValue + "px;left:397px;border-color:#000000;border-style:solid;border-width:0px;border-left-width:1px;clip:rect(0px,1px,80px,0px);height:80px;'></div>" + Environment.NewLine;
                        MasterListhtml += "<div style='position: absolute;z-index: 25;z-index:99;top:" + TopElementValue + "px;left:481px;border-color:#000000;border-style:solid;border-width:0px;border-left-width:1px;clip:rect(0px,1px,80px,0px);height:80px;'></div>" + Environment.NewLine;
                        MasterListhtml += "<div style='position: absolute;z-index: 25;z-index:99;top:" + TopElementValue + "px;left:540px;border-color:#000000;border-style:solid;border-width:0px;border-left-width:1px;clip:rect(0px,1px,80px,0px);height:80px;'></div>" + Environment.NewLine;
                        MasterListhtml += "<div style='position: absolute;z-index: 25;z-index:99;top:" + TopElementValue + "px;left:609px;border-color:#000000;border-style:solid;border-width:0px;border-left-width:1px;clip:rect(0px,1px,80px,0px);height:80px;'></div>" + Environment.NewLine;
                        MasterListhtml += "<div style='position: absolute;z-index: 25;z-index:99;top:" + TopElementValue + "px;left:685px;border-color:#000000;border-style:solid;border-width:0px;border-left-width:1px;clip:rect(0px,1px,80px,0px);height:80px;'></div>" + Environment.NewLine;
                        MasterListhtml += "<div style='position: absolute;z-index: 25;z-index:99;top:" + TopElementValue + "px;left:761px;border-color:#000000;border-style:solid;border-width:0px;border-left-width:1px;clip:rect(0px,1px,80px,0px);height:80px;'></div>" + Environment.NewLine;
                        MasterListhtml += "<div style='position: absolute;z-index: 25;z-index:99;top:" + TopElementValue + "px;left:845px;border-color:#000000;border-style:solid;border-width:0px;border-left-width:1px;clip:rect(0px,1px,80px,0px);height:80px;'></div>" + Environment.NewLine;
                        MasterListhtml += "<div style='position: absolute;z-index: 25;z-index:99;top:" + TopElementValue + "px;left:913px;border-color:#000000;border-style:solid;border-width:0px;border-left-width:1px;clip:rect(0px,1px,80px,0px);height:80px;'></div>" + Environment.NewLine;
                        MasterListhtml += "<div style='position: absolute;z-index: 25;z-index:99;top:" + TopElementValue + "px;left:970px;border-color:#000000;border-style:solid;border-width:0px;border-left-width:1px;clip:rect(0px,1px,80px,0px);height:80px;'></div>" + Environment.NewLine;
                        MasterListhtml += "<div style='position: absolute;z-index: 25;z-index:99;top:" + TopElementValue + "px;left:1049px;border-color:#000000;border-style:solid;border-width:0px;border-left-width:1px;clip:rect(0px,1px,80px,0px);height:80px;'></div>" + Environment.NewLine;
                        MasterListhtml += "<div style='position: absolute;z-index: 25;z-index:99;top:" + TopElementValue + "px;left:1100px;border-color:#000000;border-style:solid;border-width:0px;border-left-width:1px;clip:rect(0px,1px,80px,0px);height:80px;'></div>" + Environment.NewLine;
                        MasterListhtml += "<div style='position: absolute;z-index: 25;z-index:99;top:" + TopElementValue + "px;left:1172px;border-color:#000000;border-style:solid;border-width:0px;border-left-width:1px;clip:rect(0px,1px,80px,0px);height:80px;'></div>" + Environment.NewLine;
                    }
                    else
                    {
                        Detailhtml = Detailhtml.Replace("@@DetialTopValue", "488");
                        MasterListhtml += "<div style='position: absolute;z-index: 25;z-index:99;top:488px;left:8px;border-color:#000000;border-style:solid;border-width:0px;border-top-width:1px;width:1165px;'></div>" + Environment.NewLine;
                        MasterListhtml += "<div style='position: absolute;z-index: 25;z-index:99;top:336px;left:8px;border-color:#000000;border-style:solid;border-width:0px;border-left-width:1px;clip:rect(0px,1px,152px,0px);height:152px;'></div>" + Environment.NewLine;
                        MasterListhtml += "<div style='position: absolute;z-index: 25;z-index:99;top:336px;left:8px;border-color:#000000;border-style:solid;border-width:0px;border-top-width:1px;width:1165px;'></div>" + Environment.NewLine;
                        MasterListhtml += "<div style='position: absolute;z-index: 25;z-index:99;top:336px;left:1172px;border-color:#000000;border-style:solid;border-width:0px;border-left-width:1px;clip:rect(0px,1px,152px,0px);height:152px;'></div>" + Environment.NewLine;
                        MasterListhtml += "<div style='position: absolute;z-index: 25;z-index:99;top:488px;left:8px;border-color:#000000;border-style:solid;border-width:0px;border-left-width:1px;clip:rect(0px,1px,80px,0px);height:80px;'></div>" + Environment.NewLine;
                        MasterListhtml += "<div style='position: absolute;z-index: 25;z-index:99;top:488px;left:45px;border-color:#000000;border-style:solid;border-width:0px;border-left-width:1px;clip:rect(0px,1px,80px,0px);height:80px;'></div>" + Environment.NewLine;
                        MasterListhtml += "<div style='position: absolute;z-index: 25;z-index:99;top:488px;left:145px;border-color:#000000;border-style:solid;border-width:0px;border-left-width:1px;clip:rect(0px,1px,80px,0px);height:80px;'></div>" + Environment.NewLine;
                        MasterListhtml += "<div style='position: absolute;z-index: 25;z-index:99;top:488px;left:245px;border-color:#000000;border-style:solid;border-width:0px;border-left-width:1px;clip:rect(0px,1px,80px,0px);height:80px;'></div>" + Environment.NewLine;
                        MasterListhtml += "<div style='position: absolute;z-index: 25;z-index:99;top:488px;left:321px;border-color:#000000;border-style:solid;border-width:0px;border-left-width:1px;clip:rect(0px,1px,80px,0px);height:80px;'></div>" + Environment.NewLine;
                        MasterListhtml += "<div style='position: absolute;z-index: 25;z-index:99;top:488px;left:397px;border-color:#000000;border-style:solid;border-width:0px;border-left-width:1px;clip:rect(0px,1px,80px,0px);height:80px;'></div>" + Environment.NewLine;
                        MasterListhtml += "<div style='position: absolute;z-index: 25;z-index:99;top:488px;left:481px;border-color:#000000;border-style:solid;border-width:0px;border-left-width:1px;clip:rect(0px,1px,80px,0px);height:80px;'></div>" + Environment.NewLine;
                        MasterListhtml += "<div style='position: absolute;z-index: 25;z-index:99;top:488px;left:540px;border-color:#000000;border-style:solid;border-width:0px;border-left-width:1px;clip:rect(0px,1px,80px,0px);height:80px;'></div>" + Environment.NewLine;
                        MasterListhtml += "<div style='position: absolute;z-index: 25;z-index:99;top:488px;left:609px;border-color:#000000;border-style:solid;border-width:0px;border-left-width:1px;clip:rect(0px,1px,80px,0px);height:80px;'></div>" + Environment.NewLine;
                        MasterListhtml += "<div style='position: absolute;z-index: 25;z-index:99;top:488px;left:685px;border-color:#000000;border-style:solid;border-width:0px;border-left-width:1px;clip:rect(0px,1px,80px,0px);height:80px;'></div>" + Environment.NewLine;
                        MasterListhtml += "<div style='position: absolute;z-index: 25;z-index:99;top:488px;left:761px;border-color:#000000;border-style:solid;border-width:0px;border-left-width:1px;clip:rect(0px,1px,80px,0px);height:80px;'></div>" + Environment.NewLine;
                        MasterListhtml += "<div style='position: absolute;z-index: 25;z-index:99;top:488px;left:845px;border-color:#000000;border-style:solid;border-width:0px;border-left-width:1px;clip:rect(0px,1px,80px,0px);height:80px;'></div>" + Environment.NewLine;
                        MasterListhtml += "<div style='position: absolute;z-index: 25;z-index:99;top:488px;left:913px;border-color:#000000;border-style:solid;border-width:0px;border-left-width:1px;clip:rect(0px,1px,80px,0px);height:80px;'></div>" + Environment.NewLine;
                        MasterListhtml += "<div style='position: absolute;z-index: 25;z-index:99;top:488px;left:970px;border-color:#000000;border-style:solid;border-width:0px;border-left-width:1px;clip:rect(0px,1px,80px,0px);height:80px;'></div>" + Environment.NewLine;
                        MasterListhtml += "<div style='position: absolute;z-index: 25;z-index:99;top:488px;left:1049px;border-color:#000000;border-style:solid;border-width:0px;border-left-width:1px;clip:rect(0px,1px,80px,0px);height:80px;'></div>" + Environment.NewLine;
                        MasterListhtml += "<div style='position: absolute;z-index: 25;z-index:99;top:488px;left:1100px;border-color:#000000;border-style:solid;border-width:0px;border-left-width:1px;clip:rect(0px,1px,80px,0px);height:80px;'></div>" + Environment.NewLine;
                        MasterListhtml += "<div style='position: absolute;z-index: 25;z-index:99;top:488px;left:1172px;border-color:#000000;border-style:solid;border-width:0px;border-left-width:1px;clip:rect(0px,1px,80px,0px);height:80px;'></div>" + Environment.NewLine;
                    }
                    #endregion

                    //Detail List data.
                    string BY_ORDER_IBAN = EachDetailRow["GPD_BY_ORDER_IBAN"].ToString();
                    DateTime UpdaloadDate = DateTime.Now;
                    DateTime TransDate = DateTime.Now;
                    if (EachDetailRow["GPD_UPLOAD_DATE"].ToString() != "")
                        UpdaloadDate = Convert.ToDateTime(EachDetailRow["GPD_UPLOAD_DATE"]);
                    if (EachDetailRow["GPD_TRNSDATE"].ToString() != "")
                        TransDate = Convert.ToDateTime(EachDetailRow["GPD_TRNSDATE"]);
                    if (BY_ORDER_IBAN != "" && BY_ORDER_IBAN != null)
                    {
                        for (int i = 8; i <= BY_ORDER_IBAN.Length; i += 8)
                        {
                            BY_ORDER_IBAN = BY_ORDER_IBAN.Insert(i, " ");
                            i++;
                        }
                    }

                    string Gpd_Pupose = EachDetailRow["GPD_PURPOSE"].ToString();
                    if (Gpd_Pupose != "" && Gpd_Pupose != null)
                    {
                        for (int i = 11; i <= Gpd_Pupose.Length; i += 11)
                        {
                            Gpd_Pupose = Gpd_Pupose.Insert(i, Environment.NewLine);
                            i++;
                        }
                    }
                    string Lcy_amount = EachDetailRow["GPD_LCYAMOUNT"].ToString();
                    if (Lcy_amount != "" && Lcy_amount != null)
                    {
                        for (int i = 10; i <= Lcy_amount.Length; i += 10)
                        {
                            Lcy_amount = Lcy_amount.Insert(i, Environment.NewLine);
                            i++;
                        }
                    }

                    SNO = SNO + 1;

                    Detailhtml = Detailhtml.Replace("@@GPD_SNO", Convert.ToString(SNO));
                    Detailhtml = Detailhtml.Replace("@@GPD_CLIENT", EachDetailRow["GPD_CLIENT"].ToString());
                    Detailhtml = Detailhtml.Replace("@@GPD_BY_ORDER_CNIC", EachDetailRow["GPD_BY_ORDER_CNIC"].ToString());
                    Detailhtml = Detailhtml.Replace("@@GPD_BY_ORDER_IBAN", BY_ORDER_IBAN);
                    Detailhtml = Detailhtml.Replace("@@GPD_REMITTING_INSTITUTION", EachDetailRow["GPD_REMITTING_INSTITUTION"].ToString());
                    Detailhtml = Detailhtml.Replace("@@GPD_BY_ORDER_COUNTRY", EachDetailRow["GPD_BY_ORDER_COUNTRY"].ToString());
                    Detailhtml = Detailhtml.Replace("@@GPD_CCODE", EachDetailRow["GPD_CCODE"].ToString());
                    Detailhtml = Detailhtml.Replace("@@GPD_PRC_NUMBER", EachDetailRow["GPD_PRC_NUMBER"].ToString());
                    Detailhtml = Detailhtml.Replace("@@GPD_UPLOAD_DATE", EachDetailRow["GPD_UPLOAD_DATE"].ToString() == "" ? "" : UpdaloadDate.ToString("dd-MMM-yyyy"));
                    Detailhtml = Detailhtml.Replace("@@GPD_TRNSDATE", EachDetailRow["GPD_TRNSDATE"].ToString() == "" ? "" : TransDate.ToString("dd-MMM-yyyy"));
                    Detailhtml = Detailhtml.Replace("@@GPD_AMOUNT", EachDetailRow["GPD_AMOUNT"].ToString());
                    Detailhtml = Detailhtml.Replace("@@GPD_RATE", EachDetailRow["GPD_RATE"].ToString());
                    Detailhtml = Detailhtml.Replace("@@GPD_LCYAMOUNT", Lcy_amount);
                    Detailhtml = Detailhtml.Replace("@@GPD_PURPOSE", Gpd_Pupose);
                    Detailhtml = Detailhtml.Replace("@@GPD_PCODE", EachDetailRow["GPD_PCODE"].ToString());
                    Detailhtml = Detailhtml.Replace("@@GPD_COMMENTS", EachDetailRow["GPD_COMMENTS"].ToString());

                    FinalHtmlToReturn = FinalHtmlToReturn + Detailhtml;
                    CssToAddCount = CssToAddCount + 1;
                }

                #region Footer Content
                //Calculation for footer table border and footer values
                int FooterSimilarTopValue = TopElementValue + 81;
                int FooterBottomTopValue = TopElementValue + 81 + 112;
                int FooterSingleTopValue = TopElementValue + 81 + 80;
                int FooterFirstValue = TopElementValue + 81 + 20;
                int FooterSecondtValue = TopElementValue + 81 + 90;
                int FooterThirdValue = TopElementValue + 81 + 120;
                //Footer Border Values
                FinalHtmlToReturn = FinalHtmlToReturn.Replace("@@FOOTERSIMILARTOPBORDER", FooterSimilarTopValue.ToString());
                FinalHtmlToReturn = FinalHtmlToReturn.Replace("@@FOOTERBOTTOMTOPBORDER", FooterBottomTopValue.ToString());
                FinalHtmlToReturn = FinalHtmlToReturn.Replace("@@FOOTERSINGLETOPBORDER", FooterSingleTopValue.ToString());
                //Footer Values
                HtmlAfterTable = HtmlAfterTable.Replace("@@FOOTERTOPONE", FooterFirstValue.ToString());
                HtmlAfterTable = HtmlAfterTable.Replace("@@FOOTERTOPTWO", FooterSecondtValue.ToString());
                HtmlAfterTable = HtmlAfterTable.Replace("@@FOOTERTOPTHREE", FooterThirdValue.ToString());
                #endregion

                FinalHtmlToReturn = FinalHtmlToReturn + HtmlAfterTable;
                string ReplaceHeaderBorderBeforeString = FinalHtmlToReturn.Substring(0, FinalHtmlToReturn.IndexOf("LIST COLUMN START HERE"));
                string ReplaceHeaderBorderAfter = FinalHtmlToReturn.Substring(FinalHtmlToReturn.IndexOf("LIST COLUMN END HERE"));
                FinalHtmlToReturn = ReplaceHeaderBorderBeforeString + MasterListhtml + ReplaceHeaderBorderAfter;
                FinalHtmlToReturn =  FinalHtmlToReturn.Replace("CUSTOM DETAIL TABLE EACH ROW START HERE", "");
                FinalHtmlToReturn =  FinalHtmlToReturn.Replace("CUSTOM DETAIL TABLE EACH ROW END HERE", "");
                FinalHtmlToReturn =  FinalHtmlToReturn.Replace("LIST COLUMN START HERE", "");
                FinalHtmlToReturn =  FinalHtmlToReturn.Replace("LIST COLUMN END HERE", "");
                return FinalHtmlToReturn;
            }
            else
                return FinalHtmlToReturn;
        }

        public static string ImageToBase64(string _imagePath)
        {
            string _base64String = null;

            using (System.Drawing.Image _image = System.Drawing.Image.FromFile(_imagePath))
            {
                using (MemoryStream _mStream = new MemoryStream())
                {
                    _image.Save(_mStream, _image.RawFormat);
                    byte[] _imageBytes = _mStream.ToArray();
                    _base64String = Convert.ToBase64String(_imageBytes);

                    return "data:image/jpg;base64," + _base64String;
                }
            }
        }
        public bool ConvertHtmlToPDFCustomm(String dest ,string html)
        {
            #region HTML Convert to PDF

            //string fileName = AppDomain.CurrentDomain.BaseDirectory + "TemplateReportFile.txt";

            //string html = File.ReadAllText(fileName);
            // instantiate the HiQPdf HTML to PDF converter Width 279.4254 and height 215.9254
            HtmlToPdf htmlToPdfConverter = new HtmlToPdf();
            //htmlToPdfConverter.HtmlLoadedTimeout = 1200;
            htmlToPdfConverter.HtmlLoadedTimeout = 9999999;
            htmlToPdfConverter.Document.PageSize = PdfPageSize.A4;
            htmlToPdfConverter.Document.FitPageWidth = true;
            htmlToPdfConverter.Document.ResizePageWidth = true;
            htmlToPdfConverter.Document.PostCardMode = false;

            htmlToPdfConverter.Document.PageOrientation = PdfPageOrientation.Landscape;


            htmlToPdfConverter.SerialNumber = "CEBhWVhs-bkRhanpp-enE5OCY4-KDkoOigw-ODAoOzkm-OTomMTEx-MQ==";

            byte[] pdfBuffer = htmlToPdfConverter.ConvertHtmlToMemory(html, "");

            System.IO.File.WriteAllBytes(dest, pdfBuffer);
            //StringReader sr = new StringReader(html);
            //Document pdfDoc = new Document(PageSize.A4, 15f, 15f, 10f, 0f);
            //pdfDoc.SetPageSize(PageSize.A4);
            //HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            //using (MemoryStream memoryStream = new MemoryStream())
            //{
            //    PdfWriter writer = PdfWriter.GetInstance(pdfDoc, memoryStream);
            //    pdfDoc.Open();
            //    htmlparser.Parse(sr);
            //    pdfDoc.Close();
            //    byte[] pdfBuffer = memoryStream.ToArray();
            //    System.IO.File.WriteAllBytes(dest, pdfBuffer);
            //    memoryStream.Close();
            //}
            #endregion
            if (File.Exists(dest))
                return true;
            else
                return false;
        }
    }
}
