﻿#define develop
//#define production
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using PRCService.Models;

namespace PRCService
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
#if production
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                new PRCService()
            };
            ServiceBase.Run(ServicesToRun);
#endif

#if develop
            AUTO_EMAILING testserv = new AUTO_EMAILING();
            //testserv.TestingMethod();
#endif
        }
    }
}
