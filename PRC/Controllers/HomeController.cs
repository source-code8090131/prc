﻿
using PRC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PRC.Controllers
{
    public class HomeController : Controller
    {
        PRCEntities context = new PRCEntities();
        // GET: Home
        public ActionResult Index()
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Home", "Index", Session["USER_ID"].ToString()))
                {
                    string UserID = Session["USER_ID"].ToString();
                    int?[] RoleIds = context.PRC_ASSIGN_LOGIN_RIGHTS.Where(m => m.PALR_USER_ID == UserID && m.PALR_STATUS == true).Select(m => m.PALR_ROLE_ID).ToArray();
                    RoleIds = context.PRC_ROLES.Where(m => RoleIds.Contains(m.PR_ID) && m.PR_STATUS == "true" && m.PR_ISAUTH == true).Select(m => (int?)m.PR_ID).ToArray();
                    int?[] FunctionIds = context.PRC_ASSIGN_FUNCTIONS.Where(m => RoleIds.Contains(m.PAF_ROLE_ID) && m.PAF_STATUS == true).Select(m => m.PAF_FUNCTION_ID).Distinct().ToArray();
                    List<PRC_FUNCTIONS> menu = context.PRC_FUNCTIONS.Where(m => FunctionIds.Contains(m.PF_ID) && m.PF_ACTION != "#" && m.PF_CONTROLLER != "#").Distinct().ToList();
                    return PartialView(menu);
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }
        [ChildActionOnly]
        public ActionResult Menu()
        {
            if (Session["USER_ID"] != null)
            {
                string UserID = Session["USER_ID"].ToString();
                int?[] RoleIds = context.PRC_ASSIGN_LOGIN_RIGHTS.Where(m => m.PALR_USER_ID == UserID && m.PALR_STATUS == true).Select(m => m.PALR_ROLE_ID).ToArray();
                RoleIds = context.PRC_ROLES.Where(m => RoleIds.Contains(m.PR_ID) && m.PR_STATUS == "true" && m.PR_ISAUTH == true).Select(m => (int?)m.PR_ID).ToArray();
                int?[] FunctionIds = context.PRC_ASSIGN_FUNCTIONS.Where(m => RoleIds.Contains(m.PAF_ROLE_ID) && m.PAF_STATUS == true).Select(m => m.PAF_FUNCTION_ID).Distinct().ToArray();
                List<PRC_FUNCTIONS> menu = context.PRC_FUNCTIONS.Where(m => FunctionIds.Contains(m.PF_ID)).Distinct().ToList();
                return PartialView(menu);
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }
    }
}