﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using PRC.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Mail;
using System.Runtime.InteropServices;
using System.Text;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;

namespace PRC.Controllers
{
    public class PRCController : Controller
    {
        // GET: PRC
        PRCEntities context = new PRCEntities();
        #region PRC GIDA Request
        public ActionResult ManReq()
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("PRC", "ManReq", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        return View();
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        List<SelectListItem> PaymentType = new SelectList(context.PRC_PAYMENT_TYPE.ToList(), "PPT_NAME", "PPT_NAME", 0).ToList();
                        PaymentType.Insert(0, (new SelectListItem { Text = "--Select Payment Type--", Value = "0" }));
                        ViewBag.PaymentType = PaymentType;
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
            return View();
        }
        public PartialViewResult ManReqView(TestingInputs Entity)
        {
            if (DAL.CheckFunctionValidity("PRC", "ManReq", Session["USER_ID"].ToString()))
            {
                string message = "";
                try
                {
                    if (Entity.InputParams != null)
                    {
                        if (Entity.InputParams.SearchFilter == "All")
                        {
                            if (Entity.InputParams.FromDate != null && Entity.InputParams.ToDate != null && Entity.InputParams.AccountNo != null)
                            {
                                var ViewData = context.PRC_SEND_EMAIL_WITH_PRC(Entity.InputParams.FromDate, Entity.InputParams.ToDate, Entity.InputParams.AccountNo, Entity.InputParams.PRCNumber, Entity.InputParams.TransReference).OrderByDescending(m => m.GPD_TRNSDATE).ToList();
                                return PartialView(ViewData);
                            }
                            else if (Entity.InputParams.FromDate != null && Entity.InputParams.ToDate != null)
                            {
                                var ViewData = context.PRC_SEND_EMAIL_WITH_PRC(Entity.InputParams.FromDate, Entity.InputParams.ToDate, Entity.InputParams.AccountNo, Entity.InputParams.PRCNumber, Entity.InputParams.TransReference).OrderByDescending(m => m.GPD_TRNSDATE).ToList();
                                return PartialView(ViewData);
                            }
                        }
                        else if (Entity.InputParams.SearchFilter == "Authorize")
                        {
                            if (Entity.InputParams.FromDate != null && Entity.InputParams.ToDate != null && Entity.InputParams.AccountNo != null)
                            {
                                var ViewData = context.PRC_SEND_EMAIL_WITH_PRC(Entity.InputParams.FromDate, Entity.InputParams.ToDate, Entity.InputParams.AccountNo, Entity.InputParams.PRCNumber, Entity.InputParams.TransReference).OrderByDescending(m => m.GPD_TRNSDATE).ToList();
                                if (ViewData.Count > 0)
                                {
                                    ViewData = ViewData.Where(m => m.GPD_MAKER_ID != Session["USER_ID"].ToString() && m.GPD_ISAUTH == true).OrderByDescending(m => m.GPD_TRNSDATE).ToList();
                                }
                                return PartialView(ViewData);
                            }
                            else if (Entity.InputParams.FromDate != null && Entity.InputParams.ToDate != null)
                            {
                                var ViewData = context.PRC_SEND_EMAIL_WITH_PRC(Entity.InputParams.FromDate, Entity.InputParams.ToDate, Entity.InputParams.AccountNo, Entity.InputParams.PRCNumber, Entity.InputParams.TransReference).OrderByDescending(m => m.GPD_TRNSDATE).ToList();
                                if (ViewData.Count > 0)
                                {
                                    ViewData = ViewData.Where(m => m.GPD_MAKER_ID != Session["USER_ID"].ToString() && m.GPD_ISAUTH == true).OrderByDescending(m => m.GPD_TRNSDATE).ToList();
                                }
                                return PartialView(ViewData);
                            }
                        }
                        else if (Entity.InputParams.SearchFilter == "Unauthorize")
                        {
                            if (Entity.InputParams.FromDate != null && Entity.InputParams.ToDate != null && Entity.InputParams.AccountNo != null)
                            {
                                var ViewData = context.PRC_SEND_EMAIL_WITH_PRC(Entity.InputParams.FromDate, Entity.InputParams.ToDate, Entity.InputParams.AccountNo, Entity.InputParams.PRCNumber, Entity.InputParams.TransReference).OrderByDescending(m => m.GPD_TRNSDATE).ToList();
                                if (ViewData.Count > 0)
                                {
                                    ViewData = ViewData.Where(m => m.GPD_MAKER_ID != Session["USER_ID"].ToString() && m.GPD_ISAUTH == false).OrderByDescending(m => m.GPD_TRNSDATE).ToList();
                                }
                                return PartialView(ViewData);
                            }
                            else if (Entity.InputParams.FromDate != null && Entity.InputParams.ToDate != null)
                            {
                                var ViewData = context.PRC_SEND_EMAIL_WITH_PRC(Entity.InputParams.FromDate, Entity.InputParams.ToDate, Entity.InputParams.AccountNo, Entity.InputParams.PRCNumber, Entity.InputParams.TransReference).OrderByDescending(m => m.GPD_TRNSDATE).ToList();
                                if (ViewData.Count > 0)
                                {
                                    ViewData = ViewData.Where(m => m.GPD_MAKER_ID != Session["USER_ID"].ToString() && m.GPD_ISAUTH == false).OrderByDescending(m => m.GPD_TRNSDATE).ToList();
                                }
                                return PartialView(ViewData);
                            }
                        }
                        else
                        {
                            if (Entity.InputParams.FromDate != null && Entity.InputParams.ToDate != null && Entity.InputParams.AccountNo != null)
                            {
                                var ViewData = context.PRC_SEND_EMAIL_WITH_PRC(Entity.InputParams.FromDate, Entity.InputParams.ToDate, Entity.InputParams.AccountNo, Entity.InputParams.PRCNumber, Entity.InputParams.TransReference).OrderByDescending(m => m.GPD_TRNSDATE).ToList();
                                if (ViewData.Count > 0)
                                {
                                    ViewData = ViewData.Where(m => m.GPD_MAKER_ID != Session["USER_ID"].ToString() || m.GPD_ISAUTH == true).OrderByDescending(m => m.GPD_TRNSDATE).ToList();
                                }
                                return PartialView(ViewData);
                            }
                            else if (Entity.InputParams.FromDate != null && Entity.InputParams.ToDate != null)
                            {
                                var ViewData = context.PRC_SEND_EMAIL_WITH_PRC(Entity.InputParams.FromDate, Entity.InputParams.ToDate, Entity.InputParams.AccountNo, Entity.InputParams.PRCNumber, Entity.InputParams.TransReference).OrderByDescending(m => m.GPD_TRNSDATE).ToList();
                                if (ViewData.Count > 0)
                                {
                                    ViewData = ViewData.Where(m => m.GPD_MAKER_ID != Session["USER_ID"].ToString() || m.GPD_ISAUTH == true).OrderByDescending(m => m.GPD_TRNSDATE).ToList();
                                }
                                return PartialView(ViewData);
                            }
                        }
                    }
                    else
                    {
                        var ViewData = context.PRC_SEND_EMAIL_WITH_PRC(Entity.InputParams.FromDate, Entity.InputParams.ToDate, Entity.InputParams.AccountNo, Entity.InputParams.PRCNumber, Entity.InputParams.TransReference).OrderByDescending(m => m.GPD_TRNSDATE).ToList();
                        if (ViewData.Count > 0)
                        {
                            ViewData = ViewData.Where(m => m.GPD_MAKER_ID != Session["USER_ID"].ToString() || m.GPD_ISAUTH == true).OrderByDescending(m => m.GPD_TRNSDATE).ToList();
                        }
                        return PartialView(ViewData);
                    }
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                }
                finally
                {
                    ViewBag.message = message;
                }
                return PartialView();
            }
            else
                return PartialView(null);
        }
        [HttpPost]
        public JsonResult AuthroizeMUC(int GPD_ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("PRC", "ManReq", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PRC_GOV_PRC_DETAIL UpdateEntity = context.PRC_GOV_PRC_DETAIL.Where(m => m.GPD_ID == GPD_ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            if (UpdateEntity.GPD_MAKER_ID != Session["USER_ID"].ToString() || UpdateEntity.GPD_MAKER_ID == "Admin123")
                            {
                                UpdateEntity.GPD_ISAUTH = true;
                                UpdateEntity.GPD_CHECKER_ID = Session["USER_ID"].ToString();
                                UpdateEntity.GPD_MODIFIED_DATE = DateTime.Now;
                                context.Entry(UpdateEntity).State = EntityState.Modified;
                                int RowCount = context.SaveChanges();
                                if (RowCount > 0)
                                {
                                    message = UpdateEntity.GPD_ACCOUNT_NAME + " is successfully Authorized";
                                }
                            }
                            else
                                message = "Sorry, Maker cannot authorize the same record.";
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + GPD_ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        [HttpPost]
        public JsonResult RejectMUC(int GPD_ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("PRC", "ManReq", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PRC_GOV_PRC_DETAIL UpdateEntity = context.PRC_GOV_PRC_DETAIL.Where(m => m.GPD_ID == GPD_ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            if (UpdateEntity.GPD_MAKER_ID != Session["USER_ID"].ToString() || UpdateEntity.GPD_MAKER_ID == "Admin123")
                            {
                                context.PRC_GOV_PRC_DETAIL.Remove(UpdateEntity);
                                int RowCount = context.SaveChanges();
                                if (RowCount > 0)
                                {
                                    message = UpdateEntity.GPD_CUSTOMER_NAME + " is rejected successfully";
                                }
                            }
                            else
                                message = "Sorry, Maker cannot reject the same record.";
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + GPD_ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        #endregion

        #region Upload Bulk PRC
        //[DllImport(@"urlmon.dll", CharSet = CharSet.Auto)]
        //private extern static System.UInt32 FindMimeFromData(System.UInt32 pBC,
        //[MarshalAs(UnmanagedType.LPStr)] System.String pwzUrl,
        //[MarshalAs(UnmanagedType.LPArray)] byte[] pBuffer,
        //System.UInt32 cbSize, [MarshalAs(UnmanagedType.LPStr)] System.String pwzMimeProposed,
        //System.UInt32 dwMimeFlags,
        //out System.UInt32 ppwzMimeOut,
        //System.UInt32 dwReserverd);
        public FileStreamResult UploadPRC(HttpPostedFileBase PostedFile)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("PRC", "ManReq", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        string filePath = string.Empty;
                        if (PostedFile != null)
                        {
                            string path = Server.MapPath("~/Uploads/");
                            if (!Directory.Exists(path))
                            {
                                Directory.CreateDirectory(path);
                            }
                            filePath = path + Path.GetFileName(PostedFile.FileName);
                            string extension = Path.GetExtension(PostedFile.FileName);
                            //byte[] document = new byte[PostedFile.ContentLength];
                            //PostedFile.InputStream.Read(document, 0, PostedFile.ContentLength);
                            //System.UInt32 mimetype;
                            //FindMimeFromData(0, null, document, 256, null, 0, out mimetype, 0);
                            //System.IntPtr mimeTypePtr = new IntPtr(mimetype);
                            //string mime = Marshal.PtrToStringUni(mimeTypePtr);
                            //Marshal.FreeCoTaskMem(mimeTypePtr);
                            if (extension == ".xlsx" || extension == ".xls")
                            {
                                //if (extension == ".xlsx")
                                //{
                                //    if (mime != "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" && mime != "application/octet-stream" && mime != "application/x-zip-compressed")
                                //    {
                                //        message = "Invalid file : " + PostedFile.FileName + Environment.NewLine + "Invalid xlsx file format provided.";
                                //        return CreateLogFile(message);
                                //    }
                                //}
                                //else if (extension == ".xls")
                                //{
                                //    if (mime != "application/vnd.ms-excel" || mime != "application/x-zip-compressed" || mime != "application/octet-stream")
                                //    {
                                //        message = "Invalid file : " + PostedFile.FileName + Environment.NewLine + "Invalid xlx file format provided.";
                                //        return CreateLogFile(message);
                                //    }
                                //}
                                PostedFile.SaveAs(filePath);
                                var Scanner = new AntiVirus.Scanner();
                                var result = Scanner.ScanAndClean(filePath);
                                if (result != AntiVirus.ScanResult.VirusNotFound)
                                {
                                    message += "malicious file detected Unable to Upload File To the Server.";
                                    if (System.IO.File.Exists(filePath))
                                        System.IO.File.Delete(filePath);
                                    return CreateLogFile(message);
                                }
                            }
                            else
                            {
                                message += "Invalid format : '" + extension + "' provided for the file required format is '.xlsx' or 'xls'.";
                                return CreateLogFile(message);
                            }
                            DataTable dt = new DataTable();
                            try
                            {
                                dt = BulkOptions.ReadAsDataTable(filePath);
                            }
                            catch (Exception ex)
                            {
                                message += "Exception Occured " + ex.Message + Environment.NewLine;
                                if (ex.InnerException != null)
                                {
                                    message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                                }
                                return CreateLogFile(message);
                            }
                            if (dt.Rows.Count > 0)
                            {
                                int numberOfRecords = dt.Rows.Count;
                                int numberOfColumns = dt.Columns.Count;
                                int RowCount = 0;
                                dt = dt.Rows.Cast<DataRow>().Where(row => !row.ItemArray.All(field => field is DBNull ||
                                                                     string.IsNullOrWhiteSpace(field as string)))
                                    .CopyToDataTable();
                                message += "*************************************** Manual PRC Process Start : " + DateTime.Now + " ***************************************" + Environment.NewLine;
                                message += "Manual PRC " + DateTime.Now + "Total Number of Rows : " + numberOfRecords + Environment.NewLine;
                                message += "Manual PRC " + DateTime.Now + "Total Number of Columns : " + numberOfColumns + Environment.NewLine;
                                foreach (DataRow row in dt.Rows)
                                {
                                    PRC_GOV_PRC_DETAIL ListOfPRC = new PRC_GOV_PRC_DETAIL();
                                    if (row["BANK REF NO."].ToString().Length > 0)
                                    {
                                        string BankRefNo = row["BANK REF NO."].ToString();
                                        bool CheckIfExist = false;
                                        ListOfPRC = context.PRC_GOV_PRC_DETAIL.Where(m => m.GPD_BANKREFNO == BankRefNo).FirstOrDefault();
                                        if (ListOfPRC == null)
                                        {
                                            ListOfPRC = new PRC_GOV_PRC_DETAIL();
                                            ListOfPRC.GPD_ID = Convert.ToInt32(context.PRC_GOV_PRC_DETAIL.Max(m => (decimal?)m.GPD_ID)) + 1;
                                            ListOfPRC.GPD_UPLOAD_DATE = DateTime.Now;
                                            CheckIfExist = false;
                                        }
                                        else
                                        {
                                            //ListOfPRC.GPD_MODIFIED_DATE = DateTime.Now;
                                            CheckIfExist = true;
                                            message += "Bank Reference Number : " + row["BANK REF NO."].ToString() + "  At Client : " + row["CLIENT"].ToString() + " is alredy exist in the system." + Environment.NewLine;
                                        }
                                        if (CheckIfExist == false)
                                        {
                                            if (row["CLIENT"] != null && row["CLIENT"].ToString() != "" || row["CLIENT"] == null || row["CLIENT"].ToString() == "")
                                            {
                                                string Client = row["CLIENT"].ToString();
                                                ListOfPRC.GPD_CLIENT = Client;
                                                if (row["TRNS. DATE"] != null && row["TRNS. DATE"].ToString() != "" || row["TRNS. DATE"] == null || row["TRNS. DATE"].ToString() == "")
                                                {
                                                    string transDate = row["TRNS. DATE"].ToString();
                                                    if (transDate != null && transDate != "")
                                                    {
                                                        double transdate;
                                                        bool isNumeric = double.TryParse(row["TRNS. DATE"].ToString(), out transdate);
                                                        if (isNumeric)
                                                        {
                                                            double TRNSDATE = double.Parse(row["TRNS. DATE"].ToString());
                                                            DateTime TRANSDATE = DateTime.FromOADate(TRNSDATE);
                                                            ListOfPRC.GPD_TRNSDATE = TRANSDATE;
                                                        }
                                                        else
                                                        {
                                                            DateTime TRANSDATE = Convert.ToDateTime(row["TRNS. DATE"].ToString());
                                                            ListOfPRC.GPD_TRNSDATE = TRANSDATE;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        ListOfPRC.GPD_TRNSDATE = null;
                                                    }
                                                    if (row["CCY"] != null && row["CCY"].ToString() != "" || row["CCY"] == null || row["CCY"].ToString() == "")
                                                    {
                                                        string CCY = row["CCY"].ToString();
                                                        ListOfPRC.GPD_CCY = CCY;
                                                        if (row["AMOUNT"] != null && row["AMOUNT"].ToString() != "" || row["AMOUNT"] == null || row["AMOUNT"].ToString() == "")
                                                        {
                                                            string AMOUNT = row["AMOUNT"].ToString();
                                                            if (AMOUNT != null || AMOUNT != "")
                                                                ListOfPRC.GPD_AMOUNT = Convert.ToDecimal(AMOUNT);
                                                            else
                                                                ListOfPRC.GPD_AMOUNT = null;
                                                            if (row["PURPOSE"] != null && row["PURPOSE"].ToString() != "" || row["PURPOSE"] == null || row["PURPOSE"].ToString() == "")
                                                            {
                                                                string PURPOSE = row["PURPOSE"].ToString();
                                                                ListOfPRC.GPD_PURPOSE = PURPOSE;
                                                                if (row["BY ORDER / COUNTRY"] != null && row["BY ORDER / COUNTRY"].ToString() != "" || row["BY ORDER / COUNTRY"] == null || row["BY ORDER / COUNTRY"].ToString() == "")
                                                                {
                                                                    string ByOrderCountry = row["BY ORDER / COUNTRY"].ToString();
                                                                    ListOfPRC.GPD_BY_ORDER_COUNTRY = ByOrderCountry;
                                                                    if (row["PAGE NO."] != null && row["PAGE NO."].ToString() != "" || row["PAGE NO."] == null || row["PAGE NO."].ToString() == "")
                                                                    {
                                                                        string PageNO = row["PAGE NO."].ToString();
                                                                        ListOfPRC.GPD_PAGE_NO = PageNO;
                                                                        if (row["S. NO."] != null && row["S. NO."].ToString() != "" || row["S. NO."] == null || row["S. NO."].ToString() == "")
                                                                        {
                                                                            string SNo = row["S. NO."].ToString();
                                                                            ListOfPRC.GPD_SNO = SNo;
                                                                            if (row["P. CODE"] != null && row["P. CODE"].ToString() != "" || row["P. CODE"] == null || row["P. CODE"].ToString() == "")
                                                                            {
                                                                                string PCode = row["P. CODE"].ToString();
                                                                                ListOfPRC.GPD_PCODE = PCode;
                                                                                if (row["C. CODE"] != null && row["C. CODE"].ToString() != "" || row["C. CODE"] == null || row["C. CODE"].ToString() == "")
                                                                                {
                                                                                    string CCode = row["C. CODE"].ToString();
                                                                                    ListOfPRC.GPD_CCODE = CCode;
                                                                                    if (row["BANK REF NO."] != null && row["BANK REF NO."].ToString() != "")
                                                                                    {
                                                                                        ListOfPRC.GPD_BANKREFNO = BankRefNo;
                                                                                        if (row["RATE"] != null && row["RATE"].ToString() != "" || row["RATE"] == null || row["RATE"].ToString() == "")
                                                                                        {
                                                                                            string Rate = row["RATE"].ToString();
                                                                                            ListOfPRC.GPD_RATE = Rate;
                                                                                            if (row["LCY AMOUNT"] != null && row["LCY AMOUNT"].ToString() != "" || row["LCY AMOUNT"] == null || row["LCY AMOUNT"].ToString() == "")
                                                                                            {
                                                                                                string LcyAmount = row["LCY AMOUNT"].ToString();
                                                                                                if(LcyAmount != null || LcyAmount != "")
                                                                                                    ListOfPRC.GPD_LCYAMOUNT = Convert.ToDecimal(LcyAmount);
                                                                                                else
                                                                                                    ListOfPRC.GPD_LCYAMOUNT = null;
                                                                                                if (row["Customer No"] != null && row["Customer No"].ToString() != "" || row["Customer No"] == null || row["Customer No"].ToString() == "")
                                                                                                {
                                                                                                    string CustomerNo = row["Customer No"].ToString();
                                                                                                    ListOfPRC.GPD_CUSTOMER_NO = CustomerNo;
                                                                                                    if (row["Customer Name"] != null && row["Customer Name"].ToString() != "" || row["Customer Name"] == null || row["Customer Name"].ToString() == "")
                                                                                                    {
                                                                                                        string CustomerName = row["Customer Name"].ToString();
                                                                                                        ListOfPRC.GPD_CUSTOMER_NAME = CustomerName;
                                                                                                        if (row["Account No"] != null && row["Account No"].ToString() != "" || row["Account No"] == null || row["Account No"].ToString() == "")
                                                                                                        {
                                                                                                            string AccountNo = row["Account No"].ToString();
                                                                                                            ListOfPRC.GPD_ACCOUNT_NO = AccountNo;
                                                                                                            if (row["Account Name"] != null && row["Account Name"].ToString() != "" || row["Account Name"] == null || row["Account Name"].ToString() == "")
                                                                                                            {
                                                                                                                string AccountName = row["Account Name"].ToString();
                                                                                                                ListOfPRC.GPD_ACCOUNT_NAME = AccountName;
                                                                                                                if (row["By Order CNIC / Reg #"] != null && row["By Order CNIC / Reg #"].ToString() != "" || row["By Order CNIC / Reg #"] == null || row["By Order CNIC / Reg #"].ToString() == "")
                                                                                                                {
                                                                                                                    string ByOrderCNIC = row["By Order CNIC / Reg #"].ToString();
                                                                                                                    ListOfPRC.GPD_BY_ORDER_CNIC = ByOrderCNIC;
                                                                                                                    if (row["By Order IBAN"] != null && row["By Order IBAN"].ToString() != "" || row["By Order IBAN"] == null || row["By Order IBAN"].ToString() == "")
                                                                                                                    {
                                                                                                                        string ByOrderIBAN = row["By Order IBAN"].ToString();
                                                                                                                        ListOfPRC.GPD_BY_ORDER_IBAN = ByOrderIBAN;
                                                                                                                        if (row["By Order Country"] != null && row["By Order Country"].ToString() != "" || row["By Order Country"] == null || row["By Order Country"].ToString() == "")
                                                                                                                        {
                                                                                                                            string OrderCountry = row["By Order Country"].ToString();
                                                                                                                            ListOfPRC.GPD_BY_ORDER_IBAN_COUNTRY = OrderCountry;
                                                                                                                            if (row["Remitting Institution"] != null && row["Remitting Institution"].ToString() != "" || row["Remitting Institution"] == null || row["Remitting Institution"].ToString() == "")
                                                                                                                            {
                                                                                                                                string RemittingInstitution = row["Remitting Institution"].ToString();
                                                                                                                                ListOfPRC.GPD_REMITTING_INSTITUTION = RemittingInstitution;
                                                                                                                                if (row["Bene NTN #"] != null && row["Bene NTN #"].ToString() != "" || row["Bene NTN #"] == null || row["Bene NTN #"].ToString() == "")
                                                                                                                                {
                                                                                                                                    string BeneNTN = row["Bene NTN #"].ToString();
                                                                                                                                    ListOfPRC.GPD_BENE_NTN = BeneNTN;
                                                                                                                                    if (Session["USER_ID"].ToString() == "Admin123")
                                                                                                                                    {
                                                                                                                                        ListOfPRC.GPD_MAKER_ID = Session["USER_ID"].ToString();
                                                                                                                                        ListOfPRC.GPD_CHECKER_ID = Session["USER_ID"].ToString();
                                                                                                                                        ListOfPRC.GPD_ISAUTH = true;
                                                                                                                                    }
                                                                                                                                    else
                                                                                                                                    {
                                                                                                                                        ListOfPRC.GPD_MAKER_ID = Session["USER_ID"].ToString();
                                                                                                                                        ListOfPRC.GPD_CHECKER_ID = null;
                                                                                                                                        ListOfPRC.GPD_ISAUTH = false;
                                                                                                                                    }
                                                                                                                                    //if (CheckIfExist == false)
                                                                                                                                    //{
                                                                                                                                    ListOfPRC.GPD_PRC_NUMBER = PRCNumber();
                                                                                                                                    context.PRC_GOV_PRC_DETAIL.Add(ListOfPRC);
                                                                                                                                    //}
                                                                                                                                    //else
                                                                                                                                    //{
                                                                                                                                    //    context.Entry(ListOfPRC).State = EntityState.Modified;
                                                                                                                                    //}
                                                                                                                                    message += "************************* writing data into temp table : " + DateTime.Now + Environment.NewLine;
                                                                                                                                    message += "************************* PRC Number : " + ListOfPRC.GPD_PRC_NUMBER + Environment.NewLine;
                                                                                                                                }
                                                                                                                                else
                                                                                                                                {
                                                                                                                                    message += "Invalid Bene NTN # " + row["Bene NTN #"].ToString() + "  At Client : " + row["CLIENT"].ToString() + Environment.NewLine;
                                                                                                                                }
                                                                                                                            }
                                                                                                                            else
                                                                                                                            {
                                                                                                                                message += "Invalid Remitting Institution : " + row["Remitting Institution"].ToString() + "  At Client : " + row["CLIENT"].ToString() + Environment.NewLine;
                                                                                                                            }
                                                                                                                        }
                                                                                                                        else
                                                                                                                        {
                                                                                                                            message += "Invalid By Order Country : " + row["By Order Country"].ToString() + "  At Client : " + row["CLIENT"].ToString() + Environment.NewLine;
                                                                                                                        }
                                                                                                                    }
                                                                                                                    else
                                                                                                                    {
                                                                                                                        message += "Invalid By Order IBAN : " + row["By Order IBAN"].ToString() + "  At Client : " + row["CLIENT"].ToString() + Environment.NewLine;
                                                                                                                    }
                                                                                                                }
                                                                                                                else
                                                                                                                {
                                                                                                                    message += "Invalid By Order CNIC / Reg # " + row["By Order CNIC / Reg #"].ToString() + "  At Client : " + row["CLIENT"].ToString() + Environment.NewLine;
                                                                                                                }
                                                                                                            }
                                                                                                            else
                                                                                                            {
                                                                                                                message += "Invalid Account Name : " + row["Account Name"].ToString() + "  At Client : " + row["CLIENT"].ToString() + Environment.NewLine;
                                                                                                            }
                                                                                                        }
                                                                                                        else
                                                                                                        {
                                                                                                            message += "Invalid Account No : " + row["Account No"].ToString() + "  At Client : " + row["CLIENT"].ToString() + Environment.NewLine;
                                                                                                        }
                                                                                                    }
                                                                                                    else
                                                                                                    {
                                                                                                        message += "Invalid Customer Name : " + row["Customer Name"].ToString() + "  At Client : " + row["CLIENT"].ToString() + Environment.NewLine;
                                                                                                    }
                                                                                                }
                                                                                                else
                                                                                                {
                                                                                                    message += "Invalid Customer No : " + row["Customer No"].ToString() + "  At Client : " + row["CLIENT"].ToString() + Environment.NewLine;
                                                                                                }
                                                                                            }
                                                                                            else
                                                                                            {
                                                                                                message += "Invalid LCY Amount : " + row["LCY AMOUNT"].ToString() + "  At Client : " + row["CLIENT"].ToString() + Environment.NewLine;
                                                                                            }
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            message += "Invalid Rate : " + row["RATE"].ToString() + "  At Client : " + row["CLIENT"].ToString() + Environment.NewLine;
                                                                                        }
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        message += "Invalid Bank Ref Number : " + row["BANK REF NO."].ToString() + "  At Client : " + row["CLIENT"].ToString() + Environment.NewLine;
                                                                                    }
                                                                                }
                                                                                else
                                                                                {
                                                                                    message += "Invalid C.Code : " + row["C. CODE"].ToString() + "  At Client : " + row["CLIENT"].ToString() + Environment.NewLine;
                                                                                }
                                                                            }
                                                                            else
                                                                            {
                                                                                message += "Invalid P.Code : " + row["P. CODE"].ToString() + "  At Client : " + row["CLIENT"].ToString() + Environment.NewLine;
                                                                            }
                                                                        }
                                                                        else
                                                                        {
                                                                            message += "Invalid S. No : " + row["S. NO."].ToString() + "  At Client : " + row["CLIENT"].ToString() + Environment.NewLine;
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        message += "Invalid Page No : " + row["PAGE NO."].ToString() + "  At Client : " + row["CLIENT"].ToString() + Environment.NewLine;
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    message += "Invalid BY ORDER / COUNTRY : " + row["BY ORDER / COUNTRY"].ToString() + "  At Client : " + row["CLIENT"].ToString() + Environment.NewLine;
                                                                }
                                                            }
                                                            else
                                                            {
                                                                message += "Invalid Purpose : " + row["PURPOSE"].ToString() + "  At Client : " + row["CLIENT"].ToString() + Environment.NewLine;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            message += "Invalid Amount : " + row["AMOUNT"].ToString() + "  At Client : " + row["CLIENT"].ToString() + Environment.NewLine;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        message += "Invalid Currency : " + row["CCY"].ToString() + "  At Client : " + row["CLIENT"].ToString() + Environment.NewLine;
                                                    }
                                                }
                                                else
                                                {
                                                    message += "Invalid TRNS.DATE : " + row["TRNS. DATE"].ToString() + "  At Client : " + row["CLIENT"].ToString() + Environment.NewLine;
                                                }
                                            }
                                            else
                                            {
                                                message += "Invalid Client " + row["CLIENT"].ToString() + ".System does not have any Client like " + row["Authorities"].ToString() + Environment.NewLine;
                                            }
                                        }
                                    }
                                    RowCount += context.SaveChanges();
                                }
                                if (RowCount > 0)
                                {
                                    message += "Data Uploaded Successfully " + RowCount + " Rows Affected" + Environment.NewLine;
                                    message += "*************************************** Manual PRC Process Complete " + DateTime.Now + " ***************************************";
                                }
                            }
                            else
                            {
                                message = "No Data Found In The File." + Environment.NewLine;
                            }
                            if (System.IO.File.Exists(filePath))
                                System.IO.File.Delete(filePath);
                        }
                    }
                    catch (Exception ex)
                    {
                        message += "Exception Occured " + ex.Message + Environment.NewLine;
                        if (ex.InnerException != null)
                        {
                            message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                        }
                    }
                    return CreateLogFile(message);
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }
        private string PRCNumber()
        {
            int y = DateTime.Now.Year;
            int m = DateTime.Now.Month;
            int d = DateTime.Now.Day;
            string PRCNumber = "";
            int daysCount = 0;
            for (int i = 1; i <= m; i++)
            {
                string getDate = "0" + i + "/" + (d) + "/" + y;
                DateTime gDate = DateTime.Now;

                if (i == 2)
                {
                    string getDateX = "0" + i + "/" + 28 + "/" + y;
                    gDate = Convert.ToDateTime(getDateX);
                }
                else
                {
                    if (d == 31 && (i == 4 || i == 6 || i == 9 || i == 11))
                    {
                        getDate = "0" + i + "/" + 30 + "/" + y;
                    }
                    gDate = Convert.ToDateTime(getDate);
                }

                int DaysInMonth = 0;

                if (i != m)
                {
                    DaysInMonth = DateTime.DaysInMonth(gDate.Year, gDate.Month);
                }
                else
                {
                    DateTime getCurrentDate = DateTime.Now;
                    DaysInMonth = Convert.ToDateTime(getCurrentDate).Day;
                }
                daysCount = daysCount + DaysInMonth;
            }
            int getdtRowCount = 0;
            string TestsubStringx = "000";
            int TestlengValx = Convert.ToInt32(Convert.ToString(daysCount).Length);
            string TestgetValdaysCount = TestsubStringx.Substring(0, (3 - TestlengValx)) + daysCount;
            string CheckPRCNumber = "CITI-PRC-" + DateTime.Now.ToString("yyyy") + "-" + TestgetValdaysCount;
            PRC_GOV_PRC_DETAIL Entity = context.PRC_GOV_PRC_DETAIL.Where(x => x.GPD_PRC_NUMBER.Contains(CheckPRCNumber)).OrderByDescending(x => x.GPD_PRC_NUMBER).FirstOrDefault();
            if (Entity != null)
            {
                string getMaxValue = Entity.GPD_PRC_NUMBER;
                if (!string.IsNullOrEmpty(getMaxValue))
                {
                    string getVals = getMaxValue.Substring(17, 6);

                    int getMaxVal = Convert.ToInt32(getVals);

                    getdtRowCount = getMaxVal + 1;
                }
                else
                {
                    getdtRowCount = 1;
                }
            }
            else
            {
                getdtRowCount = 1;
            }
            string subStringx = "000";
            int lengValx = Convert.ToInt32(Convert.ToString(daysCount).Length);
            string getValdaysCount = subStringx.Substring(0, (3 - lengValx)) + daysCount;
            int lengVal = Convert.ToInt32(Convert.ToString(getdtRowCount).Length);
            string subString = "000000";
            string getVal = subString.Substring(0, (6 - lengVal));
            PRCNumber = "CITI-PRC-" + DateTime.Now.ToString("yyyy") + "-" + getValdaysCount + getVal + getdtRowCount;
            return PRCNumber;
        }
        public FileStreamResult CreateLogFile(string Message)
        {
            var byteArray = Encoding.ASCII.GetBytes(Message);
            var stream = new MemoryStream(byteArray);
            string FileName = "Log PRC " + DateTime.Now.ToString();
            return File(stream, "text/plain", FileName + ".txt");
        }
        #endregion

        #region Upload PRC P-Code & C-Code
        public FileStreamResult UploadPRCReference(HttpPostedFileBase PostedFile)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("PRC", "ManReq", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        string filePath = string.Empty;
                        if (PostedFile != null)
                        {
                            string path = Server.MapPath("~/Uploads/");
                            if (!Directory.Exists(path))
                            {
                                Directory.CreateDirectory(path);
                            }
                            filePath = path + Path.GetFileName(PostedFile.FileName);
                            string extension = Path.GetExtension(PostedFile.FileName);
                            if (extension == ".xlsx" || extension == ".xls")
                            {
                                PostedFile.SaveAs(filePath);
                                var Scanner = new AntiVirus.Scanner();
                                var result = Scanner.ScanAndClean(filePath);
                                if (result != AntiVirus.ScanResult.VirusNotFound)
                                {
                                    message += "malicious file detected Unable to Upload File To the Server.";
                                    if (System.IO.File.Exists(filePath))
                                        System.IO.File.Delete(filePath);
                                    return CreateLogFile(message);
                                }
                            }
                            else
                            {
                                message += "Invalid format : '" + extension + "' provided for the file required format is '.xlsx' or 'xls'.";
                                return CreateLogFile(message);
                            }
                            DataTable dt = new DataTable();
                            try
                            {
                                dt = BulkOptions.ReadAsDataTable(filePath);
                            }
                            catch (Exception ex)
                            {
                                message += "Exception Occured " + ex.Message + Environment.NewLine;
                                if (ex.InnerException != null)
                                {
                                    message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                                }
                                return CreateLogFile(message);
                            }
                            if (dt.Rows.Count > 0)
                            {
                                int numberOfRecords = dt.Rows.Count;
                                int numberOfColumns = dt.Columns.Count;
                                int RowCount = 0;
                                dt = dt.Rows.Cast<DataRow>().Where(row => !row.ItemArray.All(field => field is DBNull ||
                                                                     string.IsNullOrWhiteSpace(field as string)))
                                    .CopyToDataTable();
                                message += "*************************************** Process Start : " + DateTime.Now + " ***************************************" + Environment.NewLine;
                                message += "Total Number of Rows : " + numberOfRecords + Environment.NewLine;
                                message += "Total Number of Columns : " + numberOfColumns + Environment.NewLine;
                                foreach (DataRow row in dt.Rows)
                                {
                                    PRC_GOV_PRC_DETAIL ListOfPRC = new PRC_GOV_PRC_DETAIL();
                                    if (row["Transaction Reference No."].ToString().Length > 0)
                                    {
                                        string TransRef = row["Transaction Reference No."].ToString();
                                        ListOfPRC = context.PRC_GOV_PRC_DETAIL.Where(m => m.GPD_BANKREFNO == TransRef).FirstOrDefault();
                                        if (ListOfPRC != null)
                                        {
                                            string PurposeCode = row["Purpose Code (as per ITRS)"].ToString();
                                            string PurposeOfRemittance = row["Purpose of Remittance (as per ITRS)"].ToString();
                                            string CountryCode = row["Country Code (as per ITRS)"].ToString();
                                            string CountryName = row["Name of Country"].ToString();

                                            ListOfPRC.GPD_BANKREFNO = TransRef;
                                            ListOfPRC.GPD_PCODE = PurposeCode;
                                            ListOfPRC.GPD_PURPOSE = PurposeOfRemittance;
                                            ListOfPRC.GPD_CCODE = CountryCode;
                                            ListOfPRC.GPD_BY_ORDER_COUNTRY = CountryName;
                                            ListOfPRC.GPD_MAKER_ID = Session["USER_ID"].ToString();
                                            ListOfPRC.GPD_CHECKER_ID = null;
                                            ListOfPRC.GPD_ISAUTH = false;

                                            context.Entry(ListOfPRC).State = EntityState.Modified;
                                            message += "************************* Transaction Reference No : " + ListOfPRC.GPD_BANKREFNO  + " updated sucessfully"+ Environment.NewLine;
                                        }
                                        else
                                        {
                                            message += "************************* No Data found against Transaction Reference No : " + TransRef + Environment.NewLine;
                                        }
                                    }
                                    else
                                        message += "************************* Transaction Reference No is required" + Environment.NewLine;
                                    RowCount += context.SaveChanges();
                                }
                                if (RowCount > 0)
                                {
                                    message += "Data Uploaded Successfully " + RowCount + " Rows Affected" + Environment.NewLine;
                                    message += "*************************************** Process End " + DateTime.Now + " ***************************************";
                                }
                            }
                            else
                            {
                                message = "No Data Found In The File." + Environment.NewLine;
                            }
                            if (System.IO.File.Exists(filePath))
                                System.IO.File.Delete(filePath);
                        }
                    }
                    catch (Exception ex)
                    {
                        message += "Exception Occured " + ex.Message + Environment.NewLine;
                        if (ex.InnerException != null)
                        {
                            message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                        }
                    }
                    return CreateLogFile(message);
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }
        #endregion

        #region Email And View e-PRC S-PRC
        [HttpPost]
        public ActionResult ManReq(TestingInputs Inputs)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("PRC", "ManReq", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    int RequestLogID = 0;
                    List<int> RequestLogIDs = new List<int>();
                    try
                    {
                        if (Inputs.InputParams.FromDate != null && Inputs.InputParams.ToDate != null)
                        {
                            TestingInputs inputs = new TestingInputs();
                            inputs.InputParams = new TestingInputs.InputFilter();
                            inputs.InputParams.FromDate = Inputs.InputParams.FromDate;
                            inputs.InputParams.ToDate = Inputs.InputParams.ToDate;
                            inputs.InputParams.AccountNo = Inputs.InputParams.AccountNo;
                            if (Inputs.InputParams.BtnSubmit == "BtnRefresh")
                            {
                                if (Inputs.InputParams.SearchFilter == "All")
                                {
                                    if (Inputs.InputParams.FromDate != null && Inputs.InputParams.ToDate != null && Inputs.InputParams.AccountNo != null)
                                    {
                                        var ViewData = context.PRC_SEND_EMAIL_WITH_PRC(Inputs.InputParams.FromDate, Inputs.InputParams.ToDate, Inputs.InputParams.AccountNo, Inputs.InputParams.PRCNumber, Inputs.InputParams.TransReference).ToList();
                                        if (ViewData.Count() > 0)
                                        {
                                            return View("ManReq", inputs);
                                        }
                                        else
                                        {
                                            message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(Inputs.InputParams.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy") + " End date : " + Convert.ToDateTime(Inputs.InputParams.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy") + " & Account Number : " + Inputs.InputParams.AccountNo + " on " + Inputs.InputParams.SearchFilter + " filter";
                                            return View("ManReq", inputs);
                                        }
                                    }
                                    else if (Inputs.InputParams.FromDate != null && Inputs.InputParams.ToDate != null)
                                    {
                                        var ViewData = context.PRC_SEND_EMAIL_WITH_PRC(Inputs.InputParams.FromDate, Inputs.InputParams.ToDate, Inputs.InputParams.AccountNo, Inputs.InputParams.PRCNumber, Inputs.InputParams.TransReference).ToList();
                                        if (ViewData.Count() > 0)
                                        {
                                            return View("ManReq", inputs);
                                        }
                                        else
                                        {
                                            message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(Inputs.InputParams.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy") + " End date : " + Convert.ToDateTime(Inputs.InputParams.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy") + " on " + Inputs.InputParams.SearchFilter + " filter";
                                            return View("ManReq", inputs);
                                        }
                                    }
                                }
                                else if (Inputs.InputParams.SearchFilter == "Authorize")
                                {
                                    if (Inputs.InputParams.FromDate != null && Inputs.InputParams.ToDate != null && Inputs.InputParams.AccountNo != null)
                                    {
                                        var ViewData = context.PRC_SEND_EMAIL_WITH_PRC(Inputs.InputParams.FromDate, Inputs.InputParams.ToDate, Inputs.InputParams.AccountNo, Inputs.InputParams.PRCNumber, Inputs.InputParams.TransReference).ToList();
                                        if (ViewData.Count() > 0)
                                        {
                                            ViewData = ViewData.Where(m => m.GPD_MAKER_ID != Session["USER_ID"].ToString() && m.GPD_ISAUTH == true).OrderByDescending(m => m.GPD_TRNSDATE).ToList();
                                            if (ViewData.Count() > 0)
                                                return View("ManReq", inputs);
                                            else
                                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(Inputs.InputParams.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy") + " End date : " + Convert.ToDateTime(Inputs.InputParams.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy") + " & Account Number : " + Inputs.InputParams.AccountNo + " on " + Inputs.InputParams.SearchFilter + " filter";
                                        }
                                        else
                                        {
                                            message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(Inputs.InputParams.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy") + " End date : " + Convert.ToDateTime(Inputs.InputParams.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy") + " & Account Number : " + Inputs.InputParams.AccountNo + " on " + Inputs.InputParams.SearchFilter + " filter";
                                            return View("ManReq", inputs);
                                        }
                                    }
                                    else if (Inputs.InputParams.FromDate != null && Inputs.InputParams.ToDate != null)
                                    {
                                        var ViewData = context.PRC_SEND_EMAIL_WITH_PRC(Inputs.InputParams.FromDate, Inputs.InputParams.ToDate, Inputs.InputParams.AccountNo, Inputs.InputParams.PRCNumber, Inputs.InputParams.TransReference).ToList();
                                        if (ViewData.Count() > 0)
                                        {
                                            ViewData = ViewData.Where(m => m.GPD_MAKER_ID != Session["USER_ID"].ToString() && m.GPD_ISAUTH == true).OrderByDescending(m => m.GPD_TRNSDATE).ToList();
                                                if (ViewData.Count() > 0)
                                                    return View("ManReq", inputs);
                                                else
                                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(Inputs.InputParams.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy") + " End date : " + Convert.ToDateTime(Inputs.InputParams.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy") + " & Account Number : " + Inputs.InputParams.AccountNo + " on " + Inputs.InputParams.SearchFilter + " filter";
                                        }
                                        else
                                        {
                                            message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(Inputs.InputParams.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy") + " End date : " + Convert.ToDateTime(Inputs.InputParams.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy") + " on " + Inputs.InputParams.SearchFilter + " filter";
                                            return View("ManReq", inputs);
                                        }
                                    }
                                }
                                else if (Inputs.InputParams.SearchFilter == "Unauthorize")
                                {
                                    if (Inputs.InputParams.FromDate != null && Inputs.InputParams.ToDate != null && Inputs.InputParams.AccountNo != null)
                                    {
                                        var ViewData = context.PRC_SEND_EMAIL_WITH_PRC(Inputs.InputParams.FromDate, Inputs.InputParams.ToDate, Inputs.InputParams.AccountNo, Inputs.InputParams.PRCNumber, Inputs.InputParams.TransReference).ToList();
                                        if (ViewData.Count() > 0)
                                        {
                                            ViewData = ViewData.Where(m => m.GPD_MAKER_ID != Session["USER_ID"].ToString() && m.GPD_ISAUTH == false).OrderByDescending(m => m.GPD_TRNSDATE).ToList();
                                            if (ViewData.Count > 0)
                                                return View("ManReq", inputs);
                                            else
                                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(Inputs.InputParams.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy") + " End date : " + Convert.ToDateTime(Inputs.InputParams.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy") + " & Account Number : " + Inputs.InputParams.AccountNo + " on " + Inputs.InputParams.SearchFilter + " filter";
                                        }
                                        else
                                        {
                                            message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(Inputs.InputParams.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy") + " End date : " + Convert.ToDateTime(Inputs.InputParams.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy") + " & Account Number : " + Inputs.InputParams.AccountNo + " on " + Inputs.InputParams.SearchFilter + " filter";
                                            return View("ManReq", inputs);
                                        }
                                    }
                                    else if (Inputs.InputParams.FromDate != null && Inputs.InputParams.ToDate != null)
                                    {
                                        var ViewData = context.PRC_SEND_EMAIL_WITH_PRC(Inputs.InputParams.FromDate, Inputs.InputParams.ToDate, Inputs.InputParams.AccountNo, Inputs.InputParams.PRCNumber, Inputs.InputParams.TransReference).ToList();
                                        if (ViewData.Count() > 0)
                                        {
                                            ViewData = ViewData.Where(m => m.GPD_MAKER_ID != Session["USER_ID"].ToString() && m.GPD_ISAUTH == false).OrderByDescending(m => m.GPD_TRNSDATE).ToList();
                                            if (ViewData.Count > 0)
                                                return View("ManReq", inputs);
                                            else
                                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(Inputs.InputParams.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy") + " End date : " + Convert.ToDateTime(Inputs.InputParams.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy") + " & Account Number : " + Inputs.InputParams.AccountNo + " on " + Inputs.InputParams.SearchFilter + " filter";
                                        }
                                        else
                                        {
                                            message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(Inputs.InputParams.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy") + " End date : " + Convert.ToDateTime(Inputs.InputParams.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy") + " on " + Inputs.InputParams.SearchFilter + " filter";
                                            return View("ManReq", inputs);
                                        }
                                    }
                                }
                                else
                                {
                                    if (Inputs.InputParams.FromDate != null && Inputs.InputParams.ToDate != null && Inputs.InputParams.AccountNo != null)
                                    {
                                        var ViewData = context.PRC_SEND_EMAIL_WITH_PRC(Inputs.InputParams.FromDate, Inputs.InputParams.ToDate, Inputs.InputParams.AccountNo, Inputs.InputParams.PRCNumber, Inputs.InputParams.TransReference).ToList();
                                        if (ViewData.Count() > 0)
                                        {
                                            return View("ManReq", inputs);
                                        }
                                        else
                                        {
                                            message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(Inputs.InputParams.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy") + " End date : " + Convert.ToDateTime(Inputs.InputParams.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy") + " & Account Number : " + Inputs.InputParams.AccountNo;
                                            return View("ManReq", inputs);
                                        }
                                    }
                                    else if (Inputs.InputParams.FromDate != null && Inputs.InputParams.ToDate != null)
                                    {
                                        var ViewData = context.PRC_SEND_EMAIL_WITH_PRC(Inputs.InputParams.FromDate, Inputs.InputParams.ToDate, Inputs.InputParams.AccountNo, Inputs.InputParams.PRCNumber, Inputs.InputParams.TransReference).ToList();
                                        if (ViewData.Count() > 0)
                                        {
                                            return View("ManReq", inputs);
                                        }
                                        else
                                        {
                                            message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(Inputs.InputParams.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy") + " End date : " + Convert.ToDateTime(Inputs.InputParams.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy");
                                            return View("ManReq", inputs);
                                        }
                                    }
                                }
                            }
                            else if (Inputs.InputParams.BtnSubmit == "BtnEmail")
                            {
                                if (Inputs.InputParams.FromDate != null && Inputs.InputParams.ToDate != null && Inputs.InputParams.AccountNo != null)
                                {
                                    string fromdate = Convert.ToDateTime(Inputs.InputParams.FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd MMMM yyyy");
                                    string todate = Convert.ToDateTime(Inputs.InputParams.ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd MMMM yyyy");
                                    PRC_EMAIL_CONFIGRATION CheckEmail = context.PRC_EMAIL_CONFIGRATION.Where(m => m.EC_STATUS == true && m.EC_IS_AUTH == true).FirstOrDefault();
                                    if (CheckEmail != null)
                                    {
                                        ReportDocument rd = new ReportDocument();
                                        PRC_CLIENT_PORTFOLIO GetClientClient = context.PRC_CLIENT_PORTFOLIO.Where(m => m.PCP_IBAN.Contains(Inputs.InputParams.AccountNo)).FirstOrDefault();
                                        if (GetClientClient != null)
                                        {
                                            if (Inputs.InputParams.ePRC == true)
                                            {
                                                var Entity = context.PRC_SEND_EMAIL_WITH_PRC(Inputs.InputParams.FromDate, Inputs.InputParams.ToDate, Inputs.InputParams.AccountNo, Inputs.InputParams.PRCNumber, Inputs.InputParams.TransReference).ToList();
                                                if (Entity.Count() > 0)
                                                {
                                                    Entity = Entity.Where(m => m.GPD_MAKER_ID != Session["USER_ID"].ToString() || m.GPD_ISAUTH == true).ToList();
                                                    if (Entity.Count <= 200)
                                                    {
                                                        using (MailMessage mail = new MailMessage())
                                                        {
                                                            string ToEmail = GetClientClient.PCP_EMAIL;
                                                            string FromEmail = CheckEmail.EC_CREDENTIAL_ID;
                                                            string HostName = CheckEmail.EC_SEREVER_HOST;
                                                            int EmailPort = Convert.ToInt32(CheckEmail.EC_EMAIL_PORT);
                                                            NetworkCredential NetworkCredentials = new System.Net.NetworkCredential();
                                                            NetworkCredentials.UserName = FromEmail;
                                                            //////#if dev
                                                            //NetworkCredentials.Password = "Test@@123";
                                                            mail.From = new MailAddress(FromEmail);
                                                            mail.To.Add(new MailAddress(ToEmail));
                                                            if (GetClientClient.PCP_NOTIFICATION_EMAIL != null)
                                                            {
                                                                string[] CC_Email = GetClientClient.PCP_NOTIFICATION_EMAIL.Split(',');
                                                                foreach (string MultiEmail in CC_Email)
                                                                {
                                                                    mail.CC.Add(MultiEmail);
                                                                }
                                                            }
                                                            mail.Subject = "(Secure) PRC Report " + fromdate + " To " + todate;
                                                            if (Inputs.InputParams.ePRC == true)
                                                                mail.Body = "<div class='row' style='background-color:#E9ECEF;'><div class='col-md-2'></div><div class='col-md-8'><div class='container' style='background-color:#FFFFFF'><div class='container-fluid'><p style='font-family:Bahnschrift;font-weight:lighter;font-size:15px'>Dear Client,</p><p style='font-family:Bahnschrift;font-weight:lighter;font-size:15px'>Please find attached herewith the PRC (Proceed Realization Certificate) for transactions covering the period from " + fromdate + " To " + todate + " with respect to Account number " + Inputs.InputParams.AccountNo + ".<br />  </p><p style='font-family:Bahnschrift;font-size:15px;font-weight:lighter;'>For any queries, please reach out to below contacts:<br /> </p>	<p style='font-family:Bahnschrift;font-size:15px;font-weight:lighter;'>Email: citiservice.pakistan@citi.com <br /> </p><p style='font-family:Arial;font-size:13px'>Contact No: 021-111-777-777 </p>	<p style='font-family:Bahnschrift;font-size:15px;font-weight:lighter;'><b>Note</b>: This is a system generated email, Please don't reply. <br /> </p><p style='font-family:Bahnschrift;font-size:15px;font-weight:lighter;' ><b> Disclaimer </b>: This email is confidential, may be legally privileged, and is for the intended recipient only.Access, disclosure, copying, distribution, or reliance on any of it by anyone else is prohibited and may be a criminal offence.Please delete if obtained in error and email confirmation to the sender.<br /> </p></div></div></div><div class='col-md-2'></div></div>";
                                                            if (Inputs.InputParams.ePRC == true || Inputs.InputParams.sPRC == true)
                                                            {
                                                                string LogRequestNo = DAL.GenerateRequestNo();
                                                                foreach (var PRC in Entity)
                                                                {
                                                                    #region Log PRC Request
                                                                    PRC_REQUEST_LOG Request = new PRC_REQUEST_LOG();
                                                                    Request.PRL_ID = Convert.ToInt32(context.PRC_REQUEST_LOG.Max(m => (decimal?)m.PRL_ID)) + 1;
                                                                    RequestLogIDs.Add(Request.PRL_ID);
                                                                    Request.PRL_CLIENT_EMAIL = GetClientClient.PCP_EMAIL;
                                                                    Request.PRL_FROM_DATE = Inputs.InputParams.FromDate;
                                                                    Request.PRL_TO_DATE = Inputs.InputParams.ToDate;
                                                                    Request.PRL_IS_SUCCESS = false;
                                                                    Request.PRL_REQUEST_DATE = DateTime.Now;
                                                                    Request.PRL_IBAN_NO = Inputs.InputParams.AccountNo;
                                                                    Request.PRL_REQUEST_TYPE = "ePRC";
                                                                    Request.PRL_UNIQUE_NO = PRC.GPD_PRC_NUMBER;
                                                                    Request.PRL_REQUEST_NO = LogRequestNo;
                                                                    Request.PRL_ISSUANCE_DATE = PRC.GPD_UPLOAD_DATE;
                                                                    context.PRC_REQUEST_LOG.Add(Request);
                                                                    context.SaveChanges();
                                                                    #endregion

                                                                    string FileName = "EPRC_" + PRC.GPD_BANKREFNO.Replace(" ", "") + "_" + DateTime.Now.ToString("yyyyMMdd");
                                                                    var PRCReportView = context.PRC_GOV_PRC_DETAIL_VIEW.Where(m => m.GPD_ID == PRC.GPD_ID).FirstOrDefault();
                                                                    if (Inputs.InputParams.ePRC == true)
                                                                        rd.Load(Path.Combine(HostingEnvironment.ApplicationPhysicalPath, "CRReport\\e-PRC.rpt"));
                                                                    rd.DataSourceConnections.Clear();
                                                                    rd.SetDataSource(new[] { PRCReportView });
                                                                    if (Inputs.PaymentType != "0")
                                                                    {
                                                                        if (Inputs.InputParams.ePRC)
                                                                            rd.SetParameterValue("PaymentCode", Inputs.PaymentType);
                                                                    }
                                                                    else
                                                                        rd.SetParameterValue("PaymentCode", "J/O-3");
                                                                    mail.Attachments.Add(new Attachment(rd.ExportToStream(ExportFormatType.PortableDocFormat), FileName + ".pdf"));
                                                                }
                                                            }
                                                            else
                                                            {
                                                                message += "Please Select at least one option from ePRC or SPRC.";
                                                                return View("ManReq", inputs);
                                                            }
                                                            mail.IsBodyHtml = true;
                                                            using (SmtpClient smtp = new SmtpClient(HostName, EmailPort))
                                                            {
                                                                smtp.Credentials = NetworkCredentials;
                                                                //if dev
                                                                //smtp.EnableSsl = true;
                                                                smtp.Send(mail);
                                                            }
                                                            message += "Email has been sent to the Client : " + GetClientClient.PCP_NAME + Environment.NewLine;
                                                            foreach (int LogRequestIds in RequestLogIDs)
                                                            {
                                                                PRC_REQUEST_LOG Update = context.PRC_REQUEST_LOG.Where(m => m.PRL_ID == LogRequestIds).FirstOrDefault();
                                                                if (Update != null)
                                                                {
                                                                    Update.PRL_IS_SUCCESS = true;
                                                                    Update.PRL_RESPONSE = message;
                                                                    context.Entry(Update).State = System.Data.Entity.EntityState.Modified;
                                                                    context.SaveChanges();
                                                                }
                                                                else
                                                                    message += "Problem while updating response in logs on : " + LogRequestIds + " .";
                                                            }
                                                            return View("ManReq", inputs);
                                                        }
                                                    }
                                                    else
                                                        message = "Requested data exceeds the maximum limit. Please reduce your request date range.";
                                                }
                                                else
                                                    message = "No PRC Found on the selectd Start Date : " + fromdate + " End Date : " + todate;
                                            }
                                            else if (Inputs.InputParams.sPRC)
                                            {
                                                var Entity = context.PRC_SEND_EMAIL_WITH_PRC(Inputs.InputParams.FromDate, Inputs.InputParams.ToDate, Inputs.InputParams.AccountNo, Inputs.InputParams.PRCNumber, Inputs.InputParams.TransReference).FirstOrDefault();
                                                if (Entity != null)
                                                {
                                                    #region Log PRC Request
                                                    PRC_REQUEST_LOG Request = new PRC_REQUEST_LOG();
                                                    Request.PRL_ID = Convert.ToInt32(context.PRC_REQUEST_LOG.Max(m => (decimal?)m.PRL_ID)) + 1;
                                                    RequestLogID = Request.PRL_ID;
                                                    Request.PRL_CLIENT_EMAIL = GetClientClient.PCP_EMAIL;
                                                    Request.PRL_FROM_DATE = Inputs.InputParams.FromDate;
                                                    Request.PRL_TO_DATE = Inputs.InputParams.ToDate;
                                                    Request.PRL_IS_SUCCESS = false;
                                                    Request.PRL_REQUEST_DATE = DateTime.Now;
                                                    Request.PRL_IBAN_NO = Inputs.InputParams.AccountNo;
                                                    Request.PRL_REQUEST_TYPE = "sPRC";
                                                    Request.PRL_UNIQUE_NO = DAL.GenerateSPRCNumber();
                                                    Request.PRL_REQUEST_NO = DAL.GenerateRequestNo();
                                                    Request.PRL_ISSUANCE_DATE = Request.PRL_REQUEST_DATE;
                                                    context.PRC_REQUEST_LOG.Add(Request);
                                                    context.SaveChanges();
                                                    #endregion
                                                    using (MailMessage mail = new MailMessage())
                                                    {
                                                        string ToEmail = GetClientClient.PCP_EMAIL;
                                                        string FromEmail = CheckEmail.EC_CREDENTIAL_ID;
                                                        string HostName = CheckEmail.EC_SEREVER_HOST;
                                                        int EmailPort = Convert.ToInt32(CheckEmail.EC_EMAIL_PORT);
                                                        NetworkCredential NetworkCredentials = new System.Net.NetworkCredential();
                                                        NetworkCredentials.UserName = FromEmail;
                                                        //////#if dev
                                                        //NetworkCredentials.Password = "Test@@123";
                                                        mail.From = new MailAddress(FromEmail);
                                                        mail.To.Add(new MailAddress(ToEmail));
                                                        if (GetClientClient.PCP_NOTIFICATION_EMAIL != null)
                                                        {
                                                            string[] CC_Email = GetClientClient.PCP_NOTIFICATION_EMAIL.Split(',');
                                                            foreach (string MultiEmail in CC_Email)
                                                            {
                                                                mail.CC.Add(MultiEmail);
                                                            }
                                                        }
                                                        mail.Subject = "(Secure) PRC Certificate Request - " + fromdate + " To " + todate;
                                                        if (Inputs.InputParams.sPRC == true)
                                                            mail.Body = "<div class='row' style='background-color:#E9ECEF;'><div class='col-md-2'></div><div class='col-md-8'><div class='container' style='background-color:#FFFFFF'><div class='container-fluid'><p style='font-family:Bahnschrift;font-weight:lighter;font-size:15px'>Dear Client,</p><p style='font-family:Bahnschrift;font-weight:lighter;font-size:15px'>Please find attached here with the S-PRC (Statement of Proceeds Realization Certificates) for transactions covering the period from " + fromdate + " To " + todate + " with respect to Account number " + Inputs.InputParams.AccountNo + ".<br />  </p><p style='font-family:Bahnschrift;font-size:15px;font-weight:lighter;'>For any queries, please reach out to below contacts:<br /> </p>	<p style='font-family:Bahnschrift;font-size:15px;font-weight:lighter;'>Email: citiservice.pakistan@citi.com <br /> </p><p style='font-family:Arial;font-size:13px'>Contact No: 021-111-777-777 </p>	<p style='font-family:Bahnschrift;font-size:15px;font-weight:lighter;'><b>Note</b>: This is a system generated email, Please don't reply. <br /> </p><p style='font-family:Bahnschrift;font-size:15px;font-weight:lighter;' ><b> Disclaimer </b>: This email is confidential, may be legally privileged, and is for the intended recipient only.Access, disclosure, copying, distribution, or reliance on any of it by anyone else is prohibited and may be a criminal offence.Please delete if obtained in error and email confirmation to the sender.<br /> </p></div></div></div><div class='col-md-2'></div></div>";
                                                        if (Inputs.InputParams.ePRC == true || Inputs.InputParams.sPRC == true)
                                                        {

                                                            string FileName = "SPRC_Summary_" + DateTime.Now.ToString("yyyyMMdd");
                                                            var PRCReportView = context.PRC_GOV_PRC_DETAIL_VIEW.Where(m => m.GPD_ID == Entity.GPD_ID).FirstOrDefault();
                                                            var PRCSubReportSource = context.PRC_GOV_PRC_DETAIL_SUB_REPORT_PROC(Inputs.InputParams.FromDate, Inputs.InputParams.ToDate, Inputs.InputParams.AccountNo).ToList();
                                                            if (PRCSubReportSource.Count() > 0)
                                                                PRCSubReportSource = PRCSubReportSource.Where(m => m.GPD_MAKER_ID != Session["USER_ID"].ToString() || m.GPD_ISAUTH == true).ToList();
                                                            if (Inputs.InputParams.sPRC == true)
                                                                rd.Load(Path.Combine(HostingEnvironment.ApplicationPhysicalPath, "CRReport\\S-PRC.rpt"));
                                                            rd.DataSourceConnections.Clear();
                                                            rd.SetDataSource(new[] { PRCReportView });
                                                            rd.Subreports[0].DataSourceConnections.Clear();
                                                            rd.Subreports[0].SetDataSource(PRCSubReportSource);
                                                            rd.SetParameterValue("S-PRCNumber", DAL.GenerateSPRCNumber());
                                                            rd.SetParameterValue("RequestIssuanceDate", DateTime.Now);
                                                            rd.SetParameterValue("FromDateParm", Inputs.InputParams.FromDate);
                                                            rd.SetParameterValue("ToDateParm", Inputs.InputParams.ToDate);
                                                            if (Inputs.PaymentType != "0")
                                                            {
                                                                if (Inputs.InputParams.sPRC)
                                                                    rd.SetParameterValue("PaymentCode", Inputs.PaymentType);
                                                            }
                                                            else
                                                                rd.SetParameterValue("PaymentCode", "J/O-3");
                                                            mail.Attachments.Add(new Attachment(rd.ExportToStream(ExportFormatType.PortableDocFormat), FileName + ".pdf"));
                                                        }
                                                        else
                                                        {
                                                            message += "Please Select at least one option from ePRC or SPRC.";
                                                            return View("ManReq", inputs);
                                                        }
                                                        mail.IsBodyHtml = true;
                                                        using (SmtpClient smtp = new SmtpClient(HostName, EmailPort))
                                                        {
                                                            smtp.Credentials = NetworkCredentials;
                                                            //if dev
                                                            //smtp.EnableSsl = true;
                                                            smtp.Send(mail);
                                                        }
                                                        message += "Email has been sent to the Client : " + GetClientClient.PCP_NAME + Environment.NewLine;
                                                        PRC_REQUEST_LOG Update = context.PRC_REQUEST_LOG.Where(m => m.PRL_ID == Request.PRL_ID).FirstOrDefault();
                                                        if (Update != null)
                                                        {
                                                            Update.PRL_IS_SUCCESS = true;
                                                            Update.PRL_RESPONSE = message;
                                                            context.Entry(Update).State = System.Data.Entity.EntityState.Modified;
                                                            context.SaveChanges();
                                                        }
                                                        else
                                                            message += "Problem while updating response in logs";
                                                        return View("ManReq", inputs);
                                                    }
                                                }
                                                else
                                                    message = "No PRC Found on the selectd Start Date : " + fromdate + " End Date : " + todate;
                                            }
                                            else
                                            {
                                                message = "Please select atleast one option from ePRC or SPRC";
                                                rd.Close();
                                                rd.Dispose();
                                                return View();
                                            }
                                            rd.Close();
                                            rd.Dispose();
                                        }
                                        else
                                            message = "No Client found on the given Account Number : " + Inputs.InputParams.AccountNo;
                                    }
                                    else
                                        message = "No Email Configration found." + Environment.NewLine;
                                    return View("ManReq", inputs);
                                }
                                else
                                {
                                    if (Inputs.InputParams.FromDate == null)
                                        message = "Select From Date to continue";
                                    else if (Inputs.InputParams.ToDate == null)
                                        message = "Select To Date to continue";
                                    else if (Inputs.InputParams.AccountNo == null)
                                        message = "Enter Account Number to continue";
                                    else if (Inputs.InputParams.ePRC == false && Inputs.InputParams.sPRC == false)
                                        message = "Select e-PRC or S-PRC to continue";
                                    else
                                        message = "From Date and To Date are required";
                                    return View("ManReq", inputs);
                                }
                            }
                        }
                        else
                        {
                            if (Inputs.InputParams.FromDate == null)
                                message = "Select From Date to continue";
                            else if (Inputs.InputParams.ToDate == null)
                                message = "Select To Date to continue";
                            else if (Inputs.InputParams.ePRC == false && Inputs.InputParams.sPRC == false)
                                message = "Select e-PRC or S-PRC to continue";
                            else
                                message = "From Date and To Date are required";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = "Exception Message : " + ex.Message;
                        if (ex.InnerException != null)
                        {
                            message += " , Inner Exception : " + ex.InnerException.Message;
                            if (ex.InnerException.InnerException != null)
                                message += " , Inner Exception Details : " + ex.InnerException.InnerException;
                        }
                        if (Inputs.InputParams.sPRC)
                        {
                            if (RequestLogID != 0)
                            {
                                PRC_REQUEST_LOG Update = context.PRC_REQUEST_LOG.Where(m => m.PRL_ID == RequestLogID).FirstOrDefault();
                                if (Update != null)
                                {
                                    Update.PRL_RESPONSE = message;
                                    context.Entry(Update).State = System.Data.Entity.EntityState.Modified;
                                    context.SaveChanges();
                                }
                                else
                                    message += "Problem while updating response in logs";
                            }
                        }
                        else if (Inputs.InputParams.ePRC)
                        {
                            if (RequestLogIDs.Count > 0)
                            {
                                foreach (int LogRequestId in RequestLogIDs)
                                {
                                    PRC_REQUEST_LOG Update = context.PRC_REQUEST_LOG.Where(m => m.PRL_ID == LogRequestId).FirstOrDefault();
                                    if (Update != null)
                                    {
                                        Update.PRL_IS_SUCCESS = false;
                                        Update.PRL_RESPONSE = message;
                                        context.Entry(Update).State = System.Data.Entity.EntityState.Modified;
                                        context.SaveChanges();
                                    }
                                    else
                                        message += "Problem while updating response in logs on : " + LogRequestId + " .";
                                }
                            }
                        }
                    }
                    finally
                    {
                        List<SelectListItem> PaymentType = new SelectList(context.PRC_PAYMENT_TYPE.ToList(), "PPT_NAME", "PPT_NAME", 0).ToList();
                        PaymentType.Insert(0, (new SelectListItem { Text = "--Select Payment Type--", Value = "0" }));
                        ViewBag.PaymentType = PaymentType;
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
            return View("ManReq");
        }
        public ActionResult ViewPRC(int PRC_ID = 0, string FileType = "")
        {
            string message = "";
            try
            {
                if (Session["USER_ID"] != null)
                {
                    if (DAL.CheckFunctionValidity("PRC", "ManReq", Session["USER_ID"].ToString()))
                    {
                        var PRCReportView = context.PRC_GOV_PRC_DETAIL_VIEW.Where(m => m.GPD_ID == PRC_ID).FirstOrDefault();
                        ReportDocument rd = new ReportDocument();
                        string FileName = "";
                        if (FileType == "ePRC")
                        {
                            rd.Load(Path.Combine(HostingEnvironment.ApplicationPhysicalPath, "CRReport\\e-PRC.rpt"));
                            rd.DataSourceConnections.Clear();
                            rd.SetDataSource(new[] { PRCReportView });
                            rd.SetParameterValue("PaymentCode", "J/O-3");
                            Response.Buffer = false;
                            Response.ClearContent();
                            Response.ClearHeaders();
                            FileName = "EPRC_" + PRCReportView.GPD_BANKREFNO.Replace(" ", "") + "_" + DateTime.Now.ToString("yyyyMMdd");
                        }
                        else if (FileType == "sPRC")
                        {
                            var PRCSubReportSource = context.PRC_GOV_PRC_DETAIL_SUB_REPORT_PROC(PRCReportView.GPD_TRNSDATE, PRCReportView.GPD_TRNSDATE, PRCReportView.GPD_ACCOUNT_NO).ToList();
                            if (PRCSubReportSource.Count() > 0)
                            {
                                PRCSubReportSource = PRCSubReportSource.Where(m => m.GPD_BANKREFNO == PRCReportView.GPD_BANKREFNO).ToList();
                            }
                            rd.Load(Path.Combine(HostingEnvironment.ApplicationPhysicalPath, "CRReport\\S-PRC.rpt"));
                            rd.DataSourceConnections.Clear();
                            rd.SetDataSource(new[] { PRCReportView });
                            rd.Subreports[0].DataSourceConnections.Clear();
                            rd.Subreports[0].SetDataSource(PRCSubReportSource);
                            rd.SetParameterValue("PaymentCode", "J/O-3");
                            rd.SetParameterValue("S-PRCNumber", DAL.GenerateSPRCNumber());
                            rd.SetParameterValue("RequestIssuanceDate", DateTime.Now);
                            rd.SetParameterValue("FromDateParm", DateTime.Now);
                            rd.SetParameterValue("ToDateParm", DateTime.Now);
                            Response.Buffer = false;
                            Response.ClearContent();
                            Response.ClearHeaders();
                            FileName = "SPRC_Summary_" + DateTime.Now.ToString("yyyyMMdd");
                        }
                        Stream stream = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                        stream.Seek(0, SeekOrigin.Begin);
                        return File(stream, "application/pdf", FileName + ".pdf");
                    }
                    else
                    {
                        return RedirectToAction("UnAuthorizedUrl", "Error");
                    }
                }
                else
                {
                    return RedirectToAction("Index", "Login");
                }
            }
            catch (Exception ex)
            {
                message += "Exception Occured :" + ex.Message + Environment.NewLine;
                if (ex.InnerException != null)
                {
                    message += "Inner Exception : " + ex.InnerException.Message + Environment.NewLine;
                    if (ex.InnerException.InnerException != null)
                    {
                        message += "Inner Inner Exception : " + ex.InnerException.InnerException.Message + Environment.NewLine;
                    }
                }
                return CreateLogFile(message);
            }
        }
        #endregion
    }
}