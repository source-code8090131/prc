﻿using PRC.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PRC.Controllers
{
    public class PRC_GroupsController : Controller
    {
        PRCEntities context = new PRCEntities();
        // GET: PRC_Groups
        public ActionResult Index(int R = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("PRC_Groups", "Index", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (R != 0)
                        {
                            AssignFunctions_Custom entity = new AssignFunctions_Custom();
                            entity.Entity = context.PRC_ROLES.Where(m => m.PR_ID == R).FirstOrDefault();
                            if (entity.Entity != null)
                            {
                                List<int?> PreviousFunctionIds = context.PRC_ASSIGN_FUNCTIONS.Where(m => m.PAF_ROLE_ID == R && m.PAF_STATUS == true).Select(m => m.PAF_FUNCTION_ID).ToList();
                                List<SelectListItem> items = new SelectList(context.PRC_FUNCTIONS.OrderBy(m => m.PF_PARENT_ID).ToList(), "PF_ID", "PF_NAME", 0).ToList();
                                foreach (SelectListItem item in items)
                                {
                                    int? CurrentFId = Convert.ToInt32(item.Value);
                                    if (PreviousFunctionIds.Contains(CurrentFId))
                                        item.Selected = true;
                                }
                                entity.FunctionsList = items;
                                return View(entity);
                            }
                            else
                            {
                                message = "Exception Occur while fetching your record on User Id = " + R;
                                return View();
                            }
                        }
                        else
                        {
                            AssignFunctions_Custom entity = new AssignFunctions_Custom();
                            entity.Entity = new PRC_ROLES();
                            entity.Entity.PR_ID = 0;
                            List<SelectListItem> items = new SelectList(context.PRC_FUNCTIONS.OrderBy(m => m.PF_PARENT_ID).ToList(), "PF_ID", "PF_NAME", 0).ToList();
                            entity.FunctionsList = items;
                            return View(entity);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("PRC_Groups", "Index", Session["USER_ID"].ToString(), ex);
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
            return View();
        }
        [HttpPost]
        public ActionResult Index(AssignFunctions_Custom db_table)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("PRC_Groups", "Index", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (db_table.Entity.PR_NAME == "" || db_table.Entity.PR_NAME == null)
                        {
                            message = "Role Name is required.";
                        }
                        else
                        {
                            if (db_table.SelectedFunctions != null && db_table.SelectedFunctions.Count() > 0)
                            {

                                PRC_ROLES MasterEntityToUpdate = new PRC_ROLES();
                                MasterEntityToUpdate = context.PRC_ROLES.Where(m => m.PR_NAME.ToLower() == db_table.Entity.PR_NAME.ToLower() && m.PR_ID != db_table.Entity.PR_ID).FirstOrDefault();
                                if (MasterEntityToUpdate == null)
                                {
                                    MasterEntityToUpdate = new PRC_ROLES();
                                    if (db_table.Entity.PR_ID != 0)
                                    {
                                        MasterEntityToUpdate = context.PRC_ROLES.Where(m => m.PR_ID == db_table.Entity.PR_ID).FirstOrDefault();
                                        MasterEntityToUpdate.PR_DATA_EDIT_DATETIME = DateTime.Now;
                                        context.Entry(MasterEntityToUpdate).State = EntityState.Modified;
                                    }
                                    else
                                    {
                                        MasterEntityToUpdate.PR_ID = Convert.ToInt32(context.PRC_ROLES.Max(m => (decimal?)m.PR_ID)) + 1;
                                        MasterEntityToUpdate.PR_DATA_ENTRY_DATETIME = DateTime.Now;
                                        context.PRC_ROLES.Add(MasterEntityToUpdate);
                                    }
                                    MasterEntityToUpdate.PR_NAME = db_table.Entity.PR_NAME;
                                    MasterEntityToUpdate.PR_DESCRIPTION = db_table.Entity.PR_DESCRIPTION;
                                    MasterEntityToUpdate.PR_MAKER_ID = Session["USER_ID"].ToString();
                                    if (MasterEntityToUpdate.PR_MAKER_ID == "Admin123")
                                    {
                                        MasterEntityToUpdate.PR_CHECKER_ID = MasterEntityToUpdate.PR_MAKER_ID;
                                        MasterEntityToUpdate.PR_ISAUTH = true;
                                    }
                                    else
                                    {
                                        MasterEntityToUpdate.PR_CHECKER_ID = null;
                                        MasterEntityToUpdate.PR_ISAUTH = false;
                                    }
                                    MasterEntityToUpdate.PR_STATUS = db_table.Entity.PR_STATUS;
                                    int RowCount = context.SaveChanges();
                                    if (RowCount > 0)
                                    {
                                        #region Get Old Function
                                        List<PRC_ASSIGN_FUNCTIONS> OldFunctions = context.PRC_ASSIGN_FUNCTIONS.Where(m => m.PAF_ROLE_ID == MasterEntityToUpdate.PR_ID && m.PAF_STATUS == true).ToList();

                                        string TempNewFunction = "";
                                        string TempRemoveFunction = "";
                                        foreach (int functionid in db_table.SelectedFunctions)
                                        {
                                            PRC_ASSIGN_FUNCTIONS Function = context.PRC_ASSIGN_FUNCTIONS.Where(m => m.PAF_FUNCTION_ID == functionid && m.PAF_ROLE_ID == MasterEntityToUpdate.PR_ID && m.PAF_STATUS == true).FirstOrDefault();
                                            if (Function == null)
                                            {
                                                PRC_FUNCTIONS FunctionTbl = context.PRC_FUNCTIONS.Where(m => m.PF_ID == functionid).FirstOrDefault();
                                                TempNewFunction += FunctionTbl.PF_NAME + ",";
                                            }
                                        }

                                        #endregion

                                        List<PRC_ASSIGN_FUNCTIONS> DeActivatedFunctions = context.PRC_ASSIGN_FUNCTIONS.Where(m => m.PAF_ROLE_ID == db_table.Entity.PR_ID && !db_table.SelectedFunctions.Contains(m.PAF_FUNCTION_ID)).ToList();
                                        if (DeActivatedFunctions.Count > 0)
                                        {
                                            foreach (PRC_ASSIGN_FUNCTIONS EachFunctionForUpdate in DeActivatedFunctions)
                                            {
                                                PRC_FUNCTIONS FunctionTbl = context.PRC_FUNCTIONS.Where(m => m.PF_ID == EachFunctionForUpdate.PAF_FUNCTION_ID).FirstOrDefault();
                                                TempRemoveFunction += FunctionTbl.PF_NAME + ",";
                                                EachFunctionForUpdate.PAF_STATUS = false;
                                                context.Entry(EachFunctionForUpdate).State = EntityState.Modified;
                                            }
                                        }
                                        int AssignFunId = 0;
                                        foreach (int? SelectedFunctionId in db_table.SelectedFunctions)
                                        {
                                            if (SelectedFunctionId != null)
                                            {
                                                PRC_ASSIGN_FUNCTIONS EachFunctionToUpdate = context.PRC_ASSIGN_FUNCTIONS.Where(m => m.PAF_ROLE_ID == db_table.Entity.PR_ID && m.PAF_FUNCTION_ID == SelectedFunctionId).FirstOrDefault();
                                                if (EachFunctionToUpdate == null)
                                                {
                                                    EachFunctionToUpdate = new PRC_ASSIGN_FUNCTIONS();
                                                    if (AssignFunId == 0)
                                                    {
                                                        EachFunctionToUpdate.PAF_ID = Convert.ToInt32(context.PRC_ASSIGN_FUNCTIONS.Max(m => (decimal?)m.PAF_ID)) + 1;
                                                        AssignFunId = EachFunctionToUpdate.PAF_ID;
                                                    }
                                                    else
                                                    {
                                                        AssignFunId = (AssignFunId + 1);
                                                        EachFunctionToUpdate.PAF_ID = AssignFunId;
                                                    }
                                                    EachFunctionToUpdate.PAF_FUNCTION_ID = SelectedFunctionId;
                                                    EachFunctionToUpdate.PAF_ROLE_ID = MasterEntityToUpdate.PR_ID;
                                                    EachFunctionToUpdate.PAF_MAKER_ID = Session["USER_ID"].ToString();
                                                    EachFunctionToUpdate.PAF_STATUS = true;
                                                    EachFunctionToUpdate.PAF_DATA_ENTRY_DATETIME = DateTime.Now;
                                                    context.PRC_ASSIGN_FUNCTIONS.Add(EachFunctionToUpdate);
                                                }
                                                else
                                                {
                                                    EachFunctionToUpdate.PAF_MAKER_ID = Session["USER_ID"].ToString();
                                                    EachFunctionToUpdate.PAF_STATUS = true;
                                                    EachFunctionToUpdate.PAF_DATA_ENTRY_DATETIME = DateTime.Now;
                                                    context.Entry(EachFunctionToUpdate).State = EntityState.Modified;
                                                }
                                            }
                                        }
                                        RowCount = context.SaveChanges();
                                        if (RowCount > 0)
                                        {
                                            #region Group Role Log
                                            RowCount = 0;
                                            PRC_ROLE_WITH_FUNCTIONS_LIST_VIEW AssignFunction = context.PRC_ROLE_WITH_FUNCTIONS_LIST_VIEW.Where(m => m.PR_ID == MasterEntityToUpdate.PR_ID).FirstOrDefault();
                                            PRC_GROUP_LOG GroupLog = new PRC_GROUP_LOG();
                                            GroupLog.PGLOG_ID = Convert.ToInt32(context.PRC_GROUP_LOG.Max(m => (decimal?)m.PGLOG_ID)) + 1;
                                            GroupLog.PGLOG_ROLE_NAME = MasterEntityToUpdate.PR_NAME;
                                            GroupLog.PGLOG_DESCRIPTION = MasterEntityToUpdate.PR_DESCRIPTION;
                                            GroupLog.PGLOG_ASSIGNED_ROLES = AssignFunction.ASSIGNED_FUNCTION;
                                            GroupLog.PGLOG_ADDED_BY = Session["USER_ID"].ToString();
                                            GroupLog.PGLOG_ADDED_DATETIME = DateTime.Now;
                                            if (GroupLog.PGLOG_ADDED_BY == "Admin123")
                                            {
                                                GroupLog.PGLOG_ACTIVATED_BY = GroupLog.PGLOG_ADDED_BY;
                                                GroupLog.PGLOG_ACTIVATED_DATETIME = DateTime.Now;
                                            }
                                            else
                                            {
                                                GroupLog.PGLOG_ACTIVATED_BY = null;
                                                GroupLog.PGLOG_ACTIVATED_DATETIME = null;
                                            }
                                            if (TempNewFunction != "" && TempRemoveFunction != "")
                                                GroupLog.PGLOG_CURRENT_STATUS = "New function " + TempNewFunction + " assigned & function name " + TempRemoveFunction + " is removed from the system : " + MasterEntityToUpdate.PR_NAME;
                                            else if (TempNewFunction != "" && TempRemoveFunction == "")
                                                GroupLog.PGLOG_CURRENT_STATUS = "New functions assigned to the Role : " + MasterEntityToUpdate.PR_NAME + ", New Assigned functions " + TempNewFunction;
                                            else if (TempNewFunction == "" && TempRemoveFunction != "")
                                                GroupLog.PGLOG_CURRENT_STATUS = "Following Functions Removed From Role " + MasterEntityToUpdate.PR_NAME + ", Removed Functions " + TempRemoveFunction;
                                            else if (GroupLog.PGLOG_ACTIVATED_BY == "Admin123")
                                                GroupLog.PGLOG_CURRENT_STATUS = "New functions assigned to the Role : " + MasterEntityToUpdate.PR_NAME + ", New Assigned functions " + TempNewFunction + ". Role " + MasterEntityToUpdate.PR_NAME + " is activated";
                                            GroupLog.PGLOG_ENTRY_DATETIME = DateTime.Now;
                                            context.PRC_GROUP_LOG.Add(GroupLog);
                                            RowCount = context.SaveChanges();
                                            if (RowCount > 0)
                                            {
                                                message = "Data Inserted Successfully " + RowCount + " Affected";
                                            }
                                            #endregion
                                        }
                                    }
                                    else
                                        message = "Problem While Inserting data " + Environment.NewLine + "Please Contact to Administrator";
                                }
                                else
                                {
                                    message = "A Role with the given name " + db_table.Entity.PR_NAME + " Already Exist";
                                }
                            }
                            else
                            {
                                message = "Please Select Functions.";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("PRC_Groups", "Index", Session["USER_ID"].ToString(), ex);
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                    AssignFunctions_Custom entity = new AssignFunctions_Custom();
                    entity.Entity = new PRC_ROLES();
                    entity.Entity.PR_ID = db_table.Entity.PR_ID;
                    List<int?> PreviousFunctionIds = context.PRC_ASSIGN_FUNCTIONS.Where(m => m.PAF_ROLE_ID == db_table.Entity.PR_ID).Select(m => m.PAF_FUNCTION_ID).ToList();
                    List<SelectListItem> items = new SelectList(context.PRC_FUNCTIONS.OrderBy(m => m.PF_PARENT_ID).ToList(), "PF_ID", "PF_NAME", 0).ToList();
                    foreach (SelectListItem item in items)
                    {
                        int? CurrentFId = Convert.ToInt32(item.Value);
                        if (PreviousFunctionIds.Contains(CurrentFId))
                            item.Selected = true;
                    }
                    entity.FunctionsList = items;
                    return View(entity);
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }
        [ChildActionOnly]
        public PartialViewResult Grid_View()
        {
            if (Session["USER_ID"] != null)
            {
                string SessionUser = Session["USER_ID"].ToString();
                List<PRC_ROLE_WITH_FUNCTIONS_LIST_VIEW> ViewData = new List<PRC_ROLE_WITH_FUNCTIONS_LIST_VIEW>();
                if (SessionUser == "Admin123")
                    ViewData = context.PRC_ROLE_WITH_FUNCTIONS_LIST_VIEW.Where(m => m.PR_MAKER_ID != SessionUser || m.PR_ISAUTH == true).OrderByDescending(m => m.PR_ID).ToList();
                else
                    ViewData = context.PRC_ROLE_WITH_FUNCTIONS_LIST_VIEW.Where(m => m.PR_NAME != "ADMIN" && (m.PR_MAKER_ID != SessionUser || m.PR_ISAUTH == true)).OrderByDescending(m => m.PR_ID).ToList();
                return PartialView(ViewData);
            }
            else
            {
                return PartialView(null);
            }
        }
        public JsonResult AuthorizeGroupRole(int R_ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("PRC_Groups", "Index", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PRC_ROLES UpdateEntity = context.PRC_ROLES.Where(m => m.PR_ID == R_ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            if (UpdateEntity.PR_MAKER_ID != Session["USER_ID"].ToString())
                            {
                                UpdateEntity.PR_ISAUTH = true;
                                UpdateEntity.PR_CHECKER_ID = Session["USER_ID"].ToString();
                                UpdateEntity.PR_DATA_EDIT_DATETIME = DateTime.Now;
                                context.Entry(UpdateEntity).State = EntityState.Modified;
                                int RowCount = context.SaveChanges();
                                if (RowCount > 0)
                                {
                                    RowCount = 0;
                                    #region Group Role Log
                                    PRC_ROLES MasterEntityToUpdate = context.PRC_ROLES.Where(m => m.PR_ID == R_ID).FirstOrDefault();
                                    PRC_ROLE_WITH_FUNCTIONS_LIST_VIEW AssignFunction = context.PRC_ROLE_WITH_FUNCTIONS_LIST_VIEW.Where(m => m.PR_ID == MasterEntityToUpdate.PR_ID).FirstOrDefault();
                                    PRC_GROUP_LOG GroupLog = new PRC_GROUP_LOG();
                                    GroupLog.PGLOG_ID = Convert.ToInt32(context.PRC_GROUP_LOG.Max(m => (decimal?)m.PGLOG_ID)) + 1;
                                    GroupLog.PGLOG_ROLE_NAME = MasterEntityToUpdate.PR_NAME;
                                    GroupLog.PGLOG_DESCRIPTION = MasterEntityToUpdate.PR_DESCRIPTION;
                                    GroupLog.PGLOG_ASSIGNED_ROLES = AssignFunction.ASSIGNED_FUNCTION;
                                    GroupLog.PGLOG_ADDED_BY = UpdateEntity.PR_MAKER_ID;
                                    GroupLog.PGLOG_ADDED_DATETIME = UpdateEntity.PR_DATA_ENTRY_DATETIME;
                                    GroupLog.PGLOG_ACTIVATED_BY = Session["USER_ID"].ToString();
                                    GroupLog.PGLOG_ACTIVATED_DATETIME = DateTime.Now;
                                    GroupLog.PGLOG_ENTRY_DATETIME = DateTime.Now;
                                    GroupLog.PGLOG_CURRENT_STATUS = " Activated the Role : " + MasterEntityToUpdate.PR_NAME;
                                    context.PRC_GROUP_LOG.Add(GroupLog);
                                    RowCount = context.SaveChanges();
                                    if (RowCount > 0)
                                    {
                                        message = "Group Role : " + UpdateEntity.PR_NAME + " is successfully Authorized";
                                    }
                                    #endregion
                                }
                            }
                            else
                            {
                                message = "Sorry, Maker cannot authorize the same record.";
                            }
                        }
                        else
                        {
                            message = "Probleum while fetching your record on ID# " + R_ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("PRC_Groups", "AuthorizeGroup", Session["USER_ID"].ToString(), ex);
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        public JsonResult RejectAuthorizeGroupRole(int R_ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("PRC_Groups", "Index", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PRC_ROLES UpdateEntity = context.PRC_ROLES.Where(m => m.PR_ID == R_ID).FirstOrDefault();
                        #region Save Temp Function Name Before Deleting
                        string GetAllFunction = "";
                        PRC_ROLE_WITH_FUNCTIONS_LIST_VIEW AllFunction = context.PRC_ROLE_WITH_FUNCTIONS_LIST_VIEW.Where(m => m.PR_ID == R_ID).FirstOrDefault();
                        if (AllFunction != null)
                        {
                            GetAllFunction += AllFunction.ASSIGNED_FUNCTION;
                        }
                        #endregion
                        if (UpdateEntity != null)
                        {
                            if (UpdateEntity.PR_MAKER_ID != Session["USER_ID"].ToString())
                            {
                                context.PRC_ROLES.Remove(UpdateEntity);
                                int RowCount = context.SaveChanges();
                                if (RowCount > 0)
                                {
                                    List<PRC_ASSIGN_FUNCTIONS> GetAllAssignFun = context.PRC_ASSIGN_FUNCTIONS.Where(m => m.PAF_ROLE_ID == UpdateEntity.PR_ID).ToList();
                                    if (GetAllAssignFun.Count > 0)
                                    {
                                        RowCount = 0;
                                        foreach (PRC_ASSIGN_FUNCTIONS FuntionToRemove in GetAllAssignFun)
                                        {
                                            context.PRC_ASSIGN_FUNCTIONS.Remove(FuntionToRemove);
                                            RowCount += context.SaveChanges();
                                        }
                                    }
                                    if (RowCount > 0)
                                    {
                                        RowCount = 0;
                                        PRC_GROUP_LOG GroupLog = new PRC_GROUP_LOG();
                                        GroupLog.PGLOG_ID = Convert.ToInt32(context.PRC_GROUP_LOG.Max(m => (decimal?)m.PGLOG_ID)) + 1;
                                        GroupLog.PGLOG_ROLE_NAME = UpdateEntity.PR_NAME;
                                        GroupLog.PGLOG_DESCRIPTION = UpdateEntity.PR_DESCRIPTION;
                                        GroupLog.PGLOG_ASSIGNED_ROLES = GetAllFunction;
                                        GroupLog.PGLOG_ADDED_BY = UpdateEntity.PR_MAKER_ID;
                                        GroupLog.PGLOG_ADDED_DATETIME = UpdateEntity.PR_DATA_ENTRY_DATETIME;
                                        GroupLog.PGLOG_CURRENT_STATUS = "Following Role Rejected/Deleted : " + UpdateEntity.PR_NAME;
                                        GroupLog.PGLOG_ACTIVATED_BY = Session["USER_ID"].ToString();
                                        GroupLog.PGLOG_ACTIVATED_DATETIME = DateTime.Now;
                                        GroupLog.PGLOG_ENTRY_DATETIME = DateTime.Now;
                                        context.PRC_GROUP_LOG.Add(GroupLog);
                                        RowCount = context.SaveChanges();
                                        if (RowCount > 0)
                                            message = "Group Role : " + UpdateEntity.PR_NAME + " is Rejected";
                                    }
                                }
                            }
                            else
                                message = "Sorry, Maker cannot reject the same record.";
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + R_ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("PRC_Groups", "RejectAuthorizeGroup", Session["USER_ID"].ToString(), ex);
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }

        #region Group Report
        public ActionResult GroupActivity()
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("PRC_Groups", "GroupActivity", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {

                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
            return View();
        }
        [HttpPost]
        public ActionResult GroupActivity(DateTime? FromDate, DateTime? EndDate)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("PRC_Groups", "GroupActivity", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (FromDate != null && EndDate != null)
                        {
                            var Data = context.PRC_GROUP_ACTIVITY_REPORT(FromDate, EndDate).OrderByDescending(m => m.PGLOG_ENTRY_DATETIME).ToList();
                            if (Data.Count() > 0)
                            {
                                Data = Data.Where(m => m.PGLOG_ACTIVATED_BY != "Admin123" && m.PGLOG_ADDED_BY != "Admin123").ToList();
                                return View(Data);
                            }
                            else
                            {
                                message = "No Data found in the system : On selected Start date : " + FromDate + " End date : " + EndDate;
                            }
                        }
                        else
                        {
                            if (FromDate == null && EndDate == null)
                                message = "Select start date and end date to continue";
                            else if (FromDate == null)
                                message = "Select start date to continue";
                            else if (EndDate == null)
                                message = "Select end date to continue";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                        DAL.LogException("PRC_Groups", "GroupActivity", Session["USER_ID"].ToString(), ex);
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                    return View();
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }
        #endregion
    }
}