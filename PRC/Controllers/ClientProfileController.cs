﻿using PRC.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PRC.Controllers
{
    public class ClientProfileController : Controller
    {
        PRCEntities context = new PRCEntities();
        // GET: ClientProfile
        #region Client Portfolio
        public ActionResult Index(int CP = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ClientProfile", "Index", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (CP != 0)
                        {
                            PRC_CLIENT_PORTFOLIO edit = context.PRC_CLIENT_PORTFOLIO.Where(m => m.PCP_ID == CP).FirstOrDefault();
                            if (edit != null)
                            {
                                return View(edit);
                            }
                            else
                            {
                                message = "Problem while getting your id " + CP;
                                return View();
                            }
                        }
                        else
                        {
                            ViewBag.message = message;
                            return View();
                        }
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("ClientProfile", "Index", Session["USER_ID"].ToString(), ex);
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                    return View();
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }
        [HttpPost]
        public ActionResult Index(PRC_CLIENT_PORTFOLIO db_table)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ClientProfile", "Index", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (db_table.PCP_ID != 0)
                        {
                            PRC_CLIENT_PORTFOLIO CheckDuplicateClient = context.PRC_CLIENT_PORTFOLIO.Where(m => m.PCP_ID != db_table.PCP_ID && m.PCP_PARTY_ID == db_table.PCP_PARTY_ID && m.PCP_EMAIL == db_table.PCP_EMAIL && m.PCP_IBAN == db_table.PCP_IBAN).FirstOrDefault();
                            if (CheckDuplicateClient == null)
                            {
                                PRC_CLIENT_PORTFOLIO UpdateEntity = context.PRC_CLIENT_PORTFOLIO.Where(m => m.PCP_ID == db_table.PCP_ID).FirstOrDefault();
                                if (UpdateEntity != null)
                                {
                                    UpdateEntity.PCP_NAME = db_table.PCP_NAME;
                                    UpdateEntity.PCP_EMAIL = db_table.PCP_EMAIL;
                                    UpdateEntity.PCP_CONTACT = db_table.PCP_CONTACT;
                                    UpdateEntity.PCP_IBAN = db_table.PCP_IBAN;
                                    UpdateEntity.PCP_NTN = db_table.PCP_NTN;
                                    UpdateEntity.PCP_STATUS = db_table.PCP_STATUS;
                                    UpdateEntity.PCP_SENT_AUTO_EMAILING = db_table.PCP_SENT_AUTO_EMAILING;
                                    UpdateEntity.PCP_ADDRESS = db_table.PCP_ADDRESS;
                                    UpdateEntity.PCP_NOTIFICATION_EMAIL = db_table.PCP_NOTIFICATION_EMAIL;
                                    UpdateEntity.PCP_PARTY_ID = db_table.PCP_PARTY_ID;
                                    UpdateEntity.PCP_EDIT_DATETIME = DateTime.Now;
                                    UpdateEntity.PCP_MAKER_ID = Session["USER_ID"].ToString();
                                    if (UpdateEntity.PCP_MAKER_ID == "Admin123")
                                    {
                                        UpdateEntity.PCP_CHECKER_ID = UpdateEntity.PCP_MAKER_ID;
                                        UpdateEntity.PCP_ISAUTH = true;
                                    }
                                    else
                                    {
                                        UpdateEntity.PCP_CHECKER_ID = null;
                                        UpdateEntity.PCP_ISAUTH = false;
                                    }
                                    context.Entry(UpdateEntity).State = EntityState.Modified;
                                }
                                else
                                {
                                    message = "Problem occur while getting previous record";
                                }
                            }
                            else
                            {
                                message = "Client already exist on provided Party ID # " + CheckDuplicateClient.PCP_PARTY_ID + " Email Address : " + CheckDuplicateClient.PCP_EMAIL + " IBAN No : " + CheckDuplicateClient.PCP_IBAN;
                            }
                        }
                        else
                        {
                            PRC_CLIENT_PORTFOLIO check_dep = context.PRC_CLIENT_PORTFOLIO.Where(m => m.PCP_PARTY_ID == db_table.PCP_PARTY_ID && m.PCP_IBAN == db_table.PCP_IBAN && m.PCP_EMAIL == db_table.PCP_EMAIL).FirstOrDefault();
                            if (check_dep == null)
                            {
                                PRC_CLIENT_PORTFOLIO Insert_Entity = new PRC_CLIENT_PORTFOLIO();
                                Insert_Entity.PCP_ID = Convert.ToInt32(context.PRC_CLIENT_PORTFOLIO.Max(m => (decimal?)m.PCP_ID)) + 1;
                                Insert_Entity.PCP_NAME = db_table.PCP_NAME;
                                Insert_Entity.PCP_EMAIL = db_table.PCP_EMAIL;
                                Insert_Entity.PCP_CONTACT = db_table.PCP_CONTACT;
                                Insert_Entity.PCP_IBAN = db_table.PCP_IBAN;
                                Insert_Entity.PCP_NTN = db_table.PCP_NTN;
                                Insert_Entity.PCP_ADDRESS = db_table.PCP_ADDRESS;
                                Insert_Entity.PCP_NOTIFICATION_EMAIL = db_table.PCP_NOTIFICATION_EMAIL;
                                Insert_Entity.PCP_PARTY_ID = db_table.PCP_PARTY_ID;
                                Insert_Entity.PCP_STATUS = db_table.PCP_STATUS;
                                Insert_Entity.PCP_SENT_AUTO_EMAILING = db_table.PCP_SENT_AUTO_EMAILING;
                                Insert_Entity.PCP_ENTRY_DATETIME = DateTime.Now;
                                Insert_Entity.PCP_MAKER_ID = Session["USER_ID"].ToString();
                                if (Insert_Entity.PCP_MAKER_ID == "Admin123")
                                {
                                    Insert_Entity.PCP_CHECKER_ID = Insert_Entity.PCP_MAKER_ID;
                                    Insert_Entity.PCP_ISAUTH = true;
                                }
                                else
                                {
                                    Insert_Entity.PCP_CHECKER_ID = null;
                                    Insert_Entity.PCP_ISAUTH = false;
                                }
                                context.PRC_CLIENT_PORTFOLIO.Add(Insert_Entity);
                            }
                            else
                            {
                                if (check_dep.PCP_PARTY_ID == db_table.PCP_PARTY_ID && check_dep.PCP_IBAN == db_table.PCP_IBAN && check_dep.PCP_EMAIL == db_table.PCP_EMAIL)
                                    message = "Client already exist on provided Party ID # " + db_table.PCP_PARTY_ID + " Email Address : " + db_table.PCP_EMAIL + " IBAN No : " + db_table.PCP_IBAN;
                                else if (check_dep.PCP_PARTY_ID == db_table.PCP_PARTY_ID)
                                    message = "Party Id : " + db_table.PCP_PARTY_ID + " Alredy Exist";
                                else if (check_dep.PCP_IBAN == db_table.PCP_IBAN)
                                    message = "IBAN No # : " + db_table.PCP_IBAN + " Already Exists";
                            }

                        }

                        if (message == "")
                        {
                            int overall_msg = context.SaveChanges();
                            if (overall_msg > 0)
                            {
                                message = "Data Saved Sucessfully " + overall_msg + "  Row affected";
                                ModelState.Clear();
                            }
                            else
                            {
                                message = "Problem While Inserting data " + Environment.NewLine + "Please Contact to Administrator";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("ClientProfile", "Index", Session["USER_ID"].ToString(), ex);
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                    return View(db_table);
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }
        [ChildActionOnly]
        public ActionResult ClientView()
        {
            if (Session["USER_ID"] == null)
            {
                return RedirectToAction("Index", "Error");
            }
            else
            {
                if (DAL.CheckFunctionValidity("ClientProfile", "Index", Session["USER_ID"].ToString()))
                {
                    string SessionUser = Session["USER_ID"].ToString();
                    List<PRC_CLIENT_PORTFOLIO> ViewData = context.PRC_CLIENT_PORTFOLIO.Where(m => m.PCP_ISAUTH == true || m.PCP_MAKER_ID != SessionUser).OrderByDescending(m => m.PCP_ID).ToList();
                    return PartialView(ViewData);
                }
                else
                    return PartialView(null);
            }
        }
        [HttpPost]
        public JsonResult AuthorizeClient(int CP_ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("ClientProfile", "Index", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PRC_CLIENT_PORTFOLIO UpdateEntity = context.PRC_CLIENT_PORTFOLIO.Where(m => m.PCP_ID == CP_ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            if (UpdateEntity.PCP_MAKER_ID != Session["USER_ID"].ToString())
                            {
                                UpdateEntity.PCP_ISAUTH = true;
                                UpdateEntity.PCP_CHECKER_ID = Session["USER_ID"].ToString();
                                UpdateEntity.PCP_EDIT_DATETIME = DateTime.Now;
                                context.Entry(UpdateEntity).State = EntityState.Modified;
                                int RowCount = context.SaveChanges();
                                if (RowCount > 0)
                                {
                                    message = "Client : " + UpdateEntity.PCP_NAME + " is successfully Authorized";
                                }
                            }
                            else
                                message = "Sorry, Maker cannot authorize the same record.";
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + CP_ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("ClientProfile", "AuthorizeClient", Session["USER_ID"].ToString(), ex);
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid request");
                }
            }
            else
                return Json("Invalid request");
        }
        [HttpPost]
        public JsonResult DeleteFromAuthorizeClient(int CP_ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("ClientProfile", "Index", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PRC_CLIENT_PORTFOLIO UpdateEntity = context.PRC_CLIENT_PORTFOLIO.Where(m => m.PCP_ID == CP_ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            if (UpdateEntity.PCP_MAKER_ID != Session["USER_ID"].ToString())
                            {
                                context.PRC_CLIENT_PORTFOLIO.Remove(UpdateEntity);
                                int RowCount = context.SaveChanges();
                                if (RowCount > 0)
                                {
                                    message = "Client : " + UpdateEntity.PCP_NAME + " is Rejected";
                                }
                            }
                            else
                                message = "Sorry, Maker cannot reject the same record.";
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + CP_ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("ClientProfile", "DeleteFromAuthorizeClient", Session["USER_ID"].ToString(), ex);
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid request");
                }
            }
            else
                return Json("Invalid request");
        }
        #endregion

        #region Client Request Log
        [ChildActionOnly]
        public PartialViewResult CustomerPortalLogs()
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("ClientProfile", "Index", Session["USER_ID"].ToString()))
                {
                    List<PRC_CLIENT_REQUESTS_LOG> DataList = context.PRC_CLIENT_REQUESTS_LOG.OrderByDescending(m => m.PCRL_ID).Take(1000).ToList();
                    return PartialView(DataList);
                }
                else
                    return PartialView(null);
            }
            else
            {
                return PartialView(null);
            }
        }
        public PartialViewResult RequestLogDetail(int CRL_ID = 0)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("ClientProfile", "Index", Session["USER_ID"].ToString()))
                {
                    List<PRC_CLIENT_REQUESTS_LOG> DataList = context.PRC_CLIENT_REQUESTS_LOG.Where(m => m.PCRL_ID == CRL_ID).ToList();
                    return PartialView(DataList);
                }
                else
                {
                    return PartialView();
                }
            }
            else
            {
                return PartialView();
            }
        }
        #endregion
    }
}