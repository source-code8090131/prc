﻿using PRC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace PRC.Controllers
{
    public class LoginController : Controller
    {
        PRCEntities context = new PRCEntities();
        public string GetIP()
        {
            string Str = "";
            Str = System.Net.Dns.GetHostName();
            IPHostEntry ipEntry = System.Net.Dns.GetHostEntry(Str);
            IPAddress[] addr = ipEntry.AddressList;
            return addr[addr.Length - 1].ToString();
        }
        [OutputCacheAttribute(VaryByParam = "*", Duration = 0, NoStore = true)]
        // GET: Login
        public ActionResult Index()
        {
            Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
            return View();
        }
        [HttpPost]
        public ActionResult Index(PRC_LOGIN_VIEW_CUSTOM db_table)
        {
            string message = "";
            try
            {
                //FX-00001 Invalid email
                //FX-00002 Invalid password
                //FX-00003 Unauthorized user
                //FX-00004 Authentication Failed from the Domain
                //FX-00005 Login Was Successfull but No Rights Assign to the User
                //FX-00006 Invalid Domain Seleted For Authentication.
                //FX-00007 Exception & Inner Exception.
                Session["IP"] = GetIP();
                PRC_LOGIN login = context.PRC_LOGIN.Where(m => m.PLOG_USER_ID == db_table.PLOG_USER_ID && m.PLOG_ISAUTH == true && m.PLOG_USER_STATUS == "Active").FirstOrDefault();
                if (login != null)
                {
                    PRC_LOGIN_VIEW loginview = context.PRC_LOGIN_VIEW.Where(m => m.PLOG_USER_ID == db_table.PLOG_USER_ID).FirstOrDefault();

                    Session["IP"] = GetIP();
                    if (loginview != null)
                    {
                        PRC_USER_WITH_ROLES_LIST_VIEW CheckRoles = context.PRC_USER_WITH_ROLES_LIST_VIEW.Where(m => m.PLOG_USER_ID == login.PLOG_USER_ID).FirstOrDefault();
                        if (CheckRoles != null)
                        {
                            if (CheckRoles.ASSIGNED_ROLES != null)
                            {
                                if (!loginview.PLOG_USER_ID.ToLower().Contains("admin123") && !loginview.PLOG_USER_ID.ToLower().Contains("adminc"))
                                {
                                    string adPath = "";
                                    string adurl = "";
                                    if (db_table.DOMAIN_NAME == "APAC")
                                    {
                                        adPath = "LDAP://apac.nsroot.net"; //Fully-qualified Domain Name
                                        adurl = "apac.nsroot.net";
                                    }
                                    else if (db_table.DOMAIN_NAME == "EUR")
                                    {
                                        //adPath = "LDAP://eur.nsroot.net";
                                        adPath = "LDAP://EURDCLN057.eur.nsroot.net"; //Fully-qualified Domain Name
                                        adurl = "eur.nsroot.net";
                                    }
                                    else if (db_table.DOMAIN_NAME == "NAM")
                                    {
                                        adPath = "LDAP://nam.nsroot.net"; //Fully-qualified Domain Name
                                        adurl = "nam.nsroot.net";
                                    }
                                    else
                                    {
                                        message = "Unsuccessful login. Please contact the support desk.";
                                        DAL.LogLoginMessage(db_table.PLOG_USER_ID, db_table.DOMAIN_NAME, "Select the domain to continue.");
                                        return View();
                                    }
                                    LdapAuthentication adAuth = new LdapAuthentication(adPath);
                                    bool CheckAuth = adAuth.IsAuthenticated(adurl, db_table.PLOG_USER_ID, db_table.PLOG_PASSWORD);
                                    if (CheckAuth == true)
                                    {
                                        bool isCookiePersistent = User.Identity.IsAuthenticated;
                                        FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket
                                            (1, db_table.PLOG_USER_ID, DateTime.Now, DateTime.Now.AddMinutes(60), isCookiePersistent, null);
                                        string encryptedTicket = FormsAuthentication.Encrypt(authTicket);
                                        HttpCookie authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
                                        if (true == isCookiePersistent)
                                            authCookie.Expires = authTicket.Expiration;
                                        Response.Cookies.Add(authCookie);
                                        Session["USER_ID"] = loginview.PLOG_USER_ID;
                                        Session["USER_NAME"] = loginview.PLOG_USER_NAME;
                                        Session["USER_TYPE"] = loginview.PR_NAME;
                                        return RedirectToAction("Index", "Home");
                                    }
                                    else
                                    {
                                        message = "Unsuccessful login. Please contact the support desk.";
                                        DAL.LogLoginMessage(db_table.PLOG_USER_ID, db_table.DOMAIN_NAME, "Authentication Failed from the Domain.");
                                    }
                                }
                                else
                                {
                                    PRC_SYSYTEM_SETUP SystemSetup = context.PRC_SYSYTEM_SETUP.Where(m => m.PSS_ID == 1).FirstOrDefault();
                                    if (SystemSetup != null)
                                    {
                                        if (login.PLOG_USER_ID == db_table.PLOG_USER_ID && SystemSetup.PSS_HASH == DAL.Encrypt(db_table.PLOG_PASSWORD) && login.PLOG_ISAUTH == true)
                                        {
                                            bool isCookiePersistent = User.Identity.IsAuthenticated;
                                            FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket
                                                   (1, db_table.PLOG_USER_ID, DateTime.Now, DateTime.Now.AddMinutes(60), isCookiePersistent, null);
                                            string encryptedTicket = FormsAuthentication.Encrypt(authTicket);
                                            HttpCookie authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
                                            if (true == isCookiePersistent)
                                                authCookie.Expires = authTicket.Expiration;
                                            Response.Cookies.Add(authCookie);
                                            Session["USER_ID"] = loginview.PLOG_USER_ID;
                                            Session["USER_NAME"] = loginview.PLOG_USER_NAME;
                                            Session["USER_TYPE"] = loginview.PR_NAME;
                                            return RedirectToAction("Index", "Home");
                                        }
                                        else
                                        {
                                            if (SystemSetup.PSS_HASH != DAL.Encrypt(db_table.PLOG_PASSWORD))
                                            {
                                                message = "Unsuccessful login. Please contact the support desk.";
                                                DAL.LogLoginMessage(db_table.PLOG_USER_ID, db_table.DOMAIN_NAME, "Invalid Password Provided for the User " + db_table.PLOG_USER_ID);
                                            }
                                            else if (login.PLOG_USER_ID != db_table.PLOG_USER_ID)
                                            {
                                                message = "Unsuccessful login. Please contact the support desk.";
                                                DAL.LogLoginMessage(db_table.PLOG_USER_ID, db_table.DOMAIN_NAME, "Invalid User ID provided '" + db_table.PLOG_USER_ID + "'");
                                            }
                                            else if (login.PLOG_ISAUTH != true)
                                            {
                                                message = "Unsuccessful login. Please contact the support desk.";
                                                DAL.LogLoginMessage(db_table.PLOG_USER_ID, db_table.DOMAIN_NAME, "Unauthorize User : " + db_table.PLOG_USER_ID + " is unauthorize");
                                            }
                                            return View();
                                        }
                                    }
                                    else
                                    {
                                        message = "Unsuccessful login. Please contact the support desk.";
                                        DAL.LogLoginMessage(db_table.PLOG_USER_ID, db_table.DOMAIN_NAME, "Invalid Password Provided for the User " + db_table.PLOG_USER_ID);
                                        return View();
                                    }
                                }
                            }
                            else
                            {
                                message = "Unsuccessful login. Please contact the support desk.";
                                DAL.LogLoginMessage(db_table.PLOG_USER_ID, db_table.DOMAIN_NAME, "Login Was Successfull but No Rights Assign to the User.");
                                return View();
                            }
                        }
                        else
                        {
                            message = "Unsuccessful login. Please contact the support desk.";
                            DAL.LogLoginMessage(db_table.PLOG_USER_ID, db_table.DOMAIN_NAME, "Login Was Successfull but No Rights Assign to the User.");
                            return View();
                        }
                    }
                    else
                    {
                        message = "Unsuccessful login. Please contact the support desk.";
                        DAL.LogLoginMessage(db_table.PLOG_USER_ID, db_table.DOMAIN_NAME, "Login Was Successfull but No Rights Assign to the User.");
                        return View();
                    }
                }
                else
                {
                    DAL.LogLoginMessage(db_table.PLOG_USER_ID, db_table.DOMAIN_NAME, "Invalid User Id.");
                    message = "Unsuccessful login. Please contact the support desk.";
                    return View();
                }
            }
            catch (Exception ex)
            {
                message = DAL.LogException("Login", "Index", db_table.PLOG_USER_ID, ex);
                //message += "Exception Occured " + ex.Message + Environment.NewLine;
                message += "Unsuccessful login. Please contact the support desk.";
                if (ex.InnerException != null)
                {
                    //message += "Inner Exception : " + ex.InnerException.Message.ToString() + Environment.NewLine;
                    message += "Unsuccessful login. Please contact the support desk.";
                    if (ex.InnerException.InnerException != null)
                    {
                        //message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                        message += "Unsuccessful login. Please contact the support desk.";
                    }
                }
            }
            finally
            {
                ViewBag.message = message;
                ModelState.Clear();
            }
            return View();
        }
        public ActionResult Logout()
        {
            if (Session["USER_ID"] != null)
            {
                Session.Clear();
                Session.Abandon();
                Session.RemoveAll();
            }
            FormsAuthentication.SignOut();
            Session.Clear();
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ExpiresAbsolute = DateTime.UtcNow.AddDays(-1d);
            Response.Expires = -1500;
            Response.CacheControl = "no-Cache";
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetExpires(DateTime.UtcNow.AddHours(-1));
            Response.Cache.SetNoStore();
            Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
            return RedirectToAction("Index", "Login");
        }
    }
}