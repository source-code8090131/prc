﻿using PRC.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;

namespace PRC.Controllers
{
    public class SetupController : Controller
    {
        PRCEntities context = new PRCEntities();
        // GET: Setup

        public FileStreamResult CreateLogFile(string Message)
        {
            var byteArray = Encoding.ASCII.GetBytes(Message);
            var stream = new MemoryStream(byteArray);
            string FileName = DateTime.Now.ToString();
            return File(stream, "text/plain", FileName + ".txt");
        }

        #region Verifying Agency
        public ActionResult VerifyingAgency(int A = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Setup", "VerifyingAgency", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (A != 0)
                        {
                            PRC_VERIFYING_AGENCY edit = context.PRC_VERIFYING_AGENCY.Where(m => m.PVA_ID == A).FirstOrDefault();
                            if (edit != null)
                            {
                                return View(edit);
                            }
                            else
                            {
                                message = "Exception Occur while fetching your record on User Id = " + A;
                                return View();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("Setup", "VerifyingAgency", Session["USER_ID"].ToString(), ex);
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            return View();
        }
        [HttpPost]
        public ActionResult VerifyingAgency(PRC_VERIFYING_AGENCY db_table)
        {
            string message = "";
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Setup", "VerifyingAgency", Session["USER_ID"].ToString()))
                {
                    try
                    {
                        PRC_VERIFYING_AGENCY UpdateEntity = new PRC_VERIFYING_AGENCY();

                        PRC_VERIFYING_AGENCY CheckIfExist = new PRC_VERIFYING_AGENCY();


                        if (db_table.PVA_ID == 0)
                        {
                            CheckIfExist = context.PRC_VERIFYING_AGENCY.Where(m => m.PVA_NAME == db_table.PVA_NAME).FirstOrDefault();
                            if (CheckIfExist == null)
                            {
                                CheckIfExist = new PRC_VERIFYING_AGENCY();
                                UpdateEntity.PVA_ID = Convert.ToInt32(context.PRC_VERIFYING_AGENCY.Max(m => (decimal?)m.PVA_ID)) + 1;
                                UpdateEntity.PVA_ENTRY_DATETIME = DateTime.Now;
                                CheckIfExist = null;
                            }
                            else
                            {
                                message = "Verifying Agency : " + db_table.PVA_NAME + " is already exist.";
                                return View();
                            }
                        }
                        else
                        {
                            UpdateEntity = context.PRC_VERIFYING_AGENCY.Where(m => m.PVA_ID == db_table.PVA_ID).FirstOrDefault();
                            UpdateEntity.PVA_EDIT_DATETIME = DateTime.Now;
                            CheckIfExist = UpdateEntity;
                        }
                        UpdateEntity.PVA_NAME = db_table.PVA_NAME;
                        UpdateEntity.PVA_MAKER_ID = Session["USER_ID"].ToString();
                        if (UpdateEntity.PVA_MAKER_ID == "Admin123")
                        {
                            UpdateEntity.PVA_CHECKER_ID = UpdateEntity.PVA_MAKER_ID;
                            UpdateEntity.PVA_ISAUTH = true;
                        }
                        else
                        {
                            UpdateEntity.PVA_CHECKER_ID = null;
                            UpdateEntity.PVA_ISAUTH = false;
                        }
                        UpdateEntity.PVA_STATUS = db_table.PVA_STATUS;
                        if (CheckIfExist == null)
                            context.PRC_VERIFYING_AGENCY.Add(UpdateEntity);
                        else
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                        int RowsAffected = context.SaveChanges();
                        if (RowsAffected > 0)
                        {
                            message = "Data Inserted Successfully " + RowsAffected + " Affected";
                            ModelState.Clear();
                        }
                        else
                        {
                            message = "Problem While Inserting data " + Environment.NewLine + "Please Contact to Administrator";
                        }

                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("Setup", "VerifyingAgency", Session["USER_ID"].ToString(), ex);
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                    return View();
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }
        [ChildActionOnly]
        public PartialViewResult VerifyingAgencyView()
        {
            if (Session["USER_ID"] != null)
            {
                string SessionUser = Session["USER_ID"].ToString();
                List<PRC_VERIFYING_AGENCY> ViewData = context.PRC_VERIFYING_AGENCY.Where(m => m.PVA_ISAUTH == true || m.PVA_MAKER_ID != SessionUser).OrderByDescending(m => m.PVA_ID).ToList();
                return PartialView(ViewData);
            }
            else
            {
                return PartialView(null);
            }
        }
        [HttpPost]
        public JsonResult AuthorizeVerifyingAgency(int PVA_ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("Setup", "VerifyingAgency", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PRC_VERIFYING_AGENCY UpdateEntity = context.PRC_VERIFYING_AGENCY.Where(m => m.PVA_ID == PVA_ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            if (UpdateEntity.PVA_MAKER_ID != Session["USER_ID"].ToString())
                            {
                                UpdateEntity.PVA_ISAUTH = true;
                                UpdateEntity.PVA_CHECKER_ID = Session["USER_ID"].ToString();
                                UpdateEntity.PVA_EDIT_DATETIME = DateTime.Now;
                                context.Entry(UpdateEntity).State = EntityState.Modified;
                                int RowCount = context.SaveChanges();
                                if (RowCount > 0)
                                {
                                    message = "Agency : " + UpdateEntity.PVA_NAME + " is successfully Authorized";
                                }
                            }
                            else
                                message = "Sorry, Maker cannot authorize the same record.";
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + PVA_ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("Setup", "AuthorizeVerifyingAgency", Session["USER_ID"].ToString(), ex);
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        [HttpPost]
        public JsonResult RejectVerifyingAgency(int PVA_ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("Setup", "VerifyingAgency", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PRC_VERIFYING_AGENCY UpdateEntity = context.PRC_VERIFYING_AGENCY.Where(m => m.PVA_ID == PVA_ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            if (UpdateEntity.PVA_MAKER_ID != Session["USER_ID"].ToString())
                            {
                                context.PRC_VERIFYING_AGENCY.Remove(UpdateEntity);
                                int RowCount = context.SaveChanges();
                                if (RowCount > 0)
                                    message = "Agency : " + UpdateEntity.PVA_NAME + " is rejected successfully";
                            }
                            else
                                message = "Sorry, Maker cannot reject the same record.";
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + PVA_ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("Setup", "RejectVerifyingAgency", Session["USER_ID"].ToString(), ex);
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        #endregion

        #region Verifying Officer
        public ActionResult VerifyingOfficer(int A = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Setup", "VerifyingOfficer", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (A != 0)
                        {
                            PRC_AGENT_PORTOLIO edit = context.PRC_AGENT_PORTOLIO.Where(m => m.PAP_ID == A).FirstOrDefault();
                            if (edit != null)
                            {
                                return View(edit);
                            }
                            else
                            {
                                message = "Exception Occur while fetching your record on User Id = " + A;
                                return View();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("Setup", "VerifyingOfficer", Session["USER_ID"].ToString(), ex);
                    }
                    finally
                    {
                        List<SelectListItem> Agency = new SelectList(context.PRC_VERIFYING_AGENCY.Where(m => m.PVA_STATUS == true && m.PVA_ISAUTH == true).ToList(), "PVA_NAME", "PVA_NAME", 0).ToList();
                        Agency.Insert(0, (new SelectListItem { Text = "--Select Agency--", Value = "0" }));
                        ViewBag.PAP_AGENCY_WING_NAME = Agency;
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            return View();
        }
        [HttpPost]
        public ActionResult VerifyingOfficer(PRC_AGENT_PORTOLIO db_table)
        {
            string message = "";
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Setup", "VerifyingOfficer", Session["USER_ID"].ToString()))
                {
                    try
                    {
                        PRC_AGENT_PORTOLIO UpdateEntity = new PRC_AGENT_PORTOLIO();

                        PRC_AGENT_PORTOLIO CheckIfExist = new PRC_AGENT_PORTOLIO();


                        if (db_table.PAP_ID == 0)
                        {
                            CheckIfExist = context.PRC_AGENT_PORTOLIO.Where(m => m.PAP_NAME_OF_OFFICER == db_table.PAP_NAME_OF_OFFICER).FirstOrDefault();
                            if (CheckIfExist == null)
                            {
                                CheckIfExist = new PRC_AGENT_PORTOLIO();
                                UpdateEntity.PAP_ID = Convert.ToInt32(context.PRC_AGENT_PORTOLIO.Max(m => (decimal?)m.PAP_ID)) + 1;
                                UpdateEntity.PAP_ENTRY_DATETIME = DateTime.Now;
                                CheckIfExist = null;
                            }
                            else
                            {
                                message = "Verifying Officer : " + db_table.PAP_NAME_OF_OFFICER + " is already exist.";
                                return View();
                            }
                        }
                        else
                        {
                            UpdateEntity = context.PRC_AGENT_PORTOLIO.Where(m => m.PAP_ID == db_table.PAP_ID).FirstOrDefault();
                            UpdateEntity.PAP_EDIT_DATETIME = DateTime.Now;
                            CheckIfExist = UpdateEntity;
                        }
                        UpdateEntity.PAP_NAME_OF_OFFICER = db_table.PAP_NAME_OF_OFFICER;
                        UpdateEntity.PAP_AUTHORITIES = db_table.PAP_AUTHORITIES;
                        UpdateEntity.PAP_AGENCY_WING_NAME = db_table.PAP_AGENCY_WING_NAME;
                        UpdateEntity.PAP_DESIGNATION = db_table.PAP_DESIGNATION;
                        UpdateEntity.PAP_CNIC_NO = db_table.PAP_CNIC_NO;
                        UpdateEntity.PAP_CONTACT_LANDLINE_NO = db_table.PAP_CONTACT_LANDLINE_NO;
                        UpdateEntity.PAP_CONTACT_MOBILE_NO = db_table.PAP_CONTACT_MOBILE_NO;
                        UpdateEntity.PAP_EMAIL_ADDRESS = db_table.PAP_EMAIL_ADDRESS;
                        UpdateEntity.PAP_OFFICE_ADDRESS = db_table.PAP_OFFICE_ADDRESS;
                        UpdateEntity.PAP_CITY = db_table.PAP_CITY;
                        UpdateEntity.PAP_STATUS = db_table.PAP_STATUS;
                        UpdateEntity.PAP_MAKER_ID = Session["USER_ID"].ToString();
                        if (UpdateEntity.PAP_MAKER_ID == "Admin123")
                        {
                            UpdateEntity.PAP_CHECKER_ID = UpdateEntity.PAP_MAKER_ID;
                            UpdateEntity.PAP_ISAUTH = true;
                        }
                        else
                        {
                            UpdateEntity.PAP_CHECKER_ID = null;
                            UpdateEntity.PAP_ISAUTH = false;
                        }
                        if (CheckIfExist == null)
                            context.PRC_AGENT_PORTOLIO.Add(UpdateEntity);
                        else
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                        int RowsAffected = context.SaveChanges();
                        if (RowsAffected > 0)
                        {
                            message = "Data Inserted Successfully " + RowsAffected + " Affected";
                            ModelState.Clear();
                        }
                        else
                        {
                            message = "Problem While Inserting data " + Environment.NewLine + "Please Contact to Administrator";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("Setup", "VerifyingOfficer", Session["USER_ID"].ToString(), ex);
                    }
                    finally
                    {
                        List<SelectListItem> Agency = new SelectList(context.PRC_VERIFYING_AGENCY.Where(m => m.PVA_STATUS == true && m.PVA_ISAUTH == true).ToList(), "PVA_NAME", "PVA_NAME", 0).ToList();
                        Agency.Insert(0, (new SelectListItem { Text = "--Select Agency--", Value = "0" }));
                        ViewBag.PAP_AGENCY_WING_NAME = Agency;
                        ViewBag.message = message;
                    }
                    return View();
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }
        [ChildActionOnly]
        public PartialViewResult VerifyingOfficerView()
        {
            if (Session["USER_ID"] != null)
            {
                string SessionUser = Session["USER_ID"].ToString();
                List<PRC_AGENT_PORTOLIO> ViewData = context.PRC_AGENT_PORTOLIO.Where(m => m.PAP_ISAUTH == true || m.PAP_MAKER_ID != SessionUser).OrderByDescending(m => m.PAP_ID).ToList();
                return PartialView(ViewData);
            }
            else
            {
                return PartialView(null);
            }
        }
        public FileStreamResult BulkVerifyingOfficer(HttpPostedFileBase PostedFile)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Setup", "VerifyingOfficer", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        string filePath = string.Empty;
                        if (PostedFile != null)
                        {
                            string path = Server.MapPath("~/Uploads/");
                            if (!Directory.Exists(path))
                            {
                                Directory.CreateDirectory(path);
                            }
                            filePath = path + Path.GetFileName(PostedFile.FileName);
                            string extension = Path.GetExtension(PostedFile.FileName);
                            if (extension == ".xlsx" || extension == ".xls")
                            {
                                PostedFile.SaveAs(filePath);
                                var Scanner = new AntiVirus.Scanner();
                                var result = Scanner.ScanAndClean(filePath);
                                if (result != AntiVirus.ScanResult.VirusNotFound)
                                {
                                    message += "malicious file detected Unable to Upload File To the Server.";
                                    if (System.IO.File.Exists(filePath))
                                        System.IO.File.Delete(filePath);
                                    return CreateLogFile(message);
                                }
                            }
                            else
                            {
                                message += "Invalid format : '" + extension + "' provided for the file required format is '.xlsx' or 'xls'.";
                                return CreateLogFile(message);
                            }
                            DataTable dt = new DataTable();
                            try
                            {
                                dt = BulkOptions.ReadAsDataTable(filePath);
                            }
                            catch (Exception ex)
                            {
                                message += "Exception Occured " + ex.Message + Environment.NewLine;
                                if (ex.InnerException != null)
                                {
                                    message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                                }
                                return CreateLogFile(message);
                            }
                            if (dt.Rows.Count > 0)
                            {
                                int RowCount = 0;
                                dt = dt.Rows.Cast<DataRow>().Where(row => !row.ItemArray.All(field => field is DBNull ||
                                                                     string.IsNullOrWhiteSpace(field as string)))
                                    .CopyToDataTable();
                                foreach (DataRow row in dt.Rows)
                                {
                                    PRC_AGENT_PORTOLIO ListOfAgent = new PRC_AGENT_PORTOLIO();
                                    if (row["Name of Authorized  Officer"].ToString().Length > 0)
                                    {
                                        string EmailAddress = row["Email  Address (Official  with agency domain)"].ToString();
                                        string CNICNO = row["CNICNo."].ToString();
                                        string Name = row["Name of Authorized  Officer"].ToString();
                                        bool CheckIfExist = false;
                                        ListOfAgent = context.PRC_AGENT_PORTOLIO.Where(m => m.PAP_CNIC_NO == CNICNO && m.PAP_EMAIL_ADDRESS == EmailAddress).FirstOrDefault();

                                        if (ListOfAgent == null)
                                        {
                                            ListOfAgent = new PRC_AGENT_PORTOLIO();
                                            ListOfAgent.PAP_ID = Convert.ToInt32(context.PRC_AGENT_PORTOLIO.Max(m => (decimal?)m.PAP_ID)) + 1;
                                            ListOfAgent.PAP_ENTRY_DATETIME = DateTime.Now;
                                            CheckIfExist = false;
                                        }
                                        else
                                        {
                                            ListOfAgent.PAP_EDIT_DATETIME = DateTime.Now;
                                            CheckIfExist = true;
                                        }
                                        if (row["Authorities"] != null && row["Authorities"].ToString() != "")
                                        {
                                            string Authorities = row["Authorities"].ToString();
                                            ListOfAgent.PAP_AUTHORITIES = Authorities;
                                            if (row["Name of Authorized  Officer"] != null && row["Name of Authorized  Officer"].ToString() != "" || row["Name of Authorized  Officer"] == null || row["Name of Authorized  Officer"].ToString() == "")
                                            {
                                                string OfficerName = row["Name of Authorized  Officer"].ToString();
                                                ListOfAgent.PAP_NAME_OF_OFFICER = OfficerName;
                                                if (row["Agency/Wing  Name"] != null && row["Agency/Wing  Name"].ToString() != "")
                                                {
                                                    string WingName = row["Agency/Wing  Name"].ToString();
                                                    PRC_VERIFYING_AGENCY Auth = context.PRC_VERIFYING_AGENCY.Where(m => m.PVA_NAME == WingName).FirstOrDefault();
                                                    if (Auth != null)
                                                    {
                                                        ListOfAgent.PAP_AGENCY_WING_NAME = WingName;
                                                        if (row["Designation"] != null && row["Designation"].ToString() != "" || row["Designation"] == null || row["Designation"].ToString() == "")
                                                        {
                                                            string Designation = row["Designation"].ToString();
                                                            ListOfAgent.PAP_DESIGNATION = Designation;
                                                            if (row["CNICNo."] != null && row["CNICNo."].ToString() != "" || row["CNICNo."] == null || row["CNICNo."].ToString() == "")
                                                            {
                                                                ListOfAgent.PAP_CNIC_NO = CNICNO;
                                                                if (row["Contact-Landline (With City Code)"] != null && row["Contact-Landline (With City Code)"].ToString() != "" || row["Contact-Landline (With City Code)"] == null || row["Contact-Landline (With City Code)"].ToString() == "")
                                                                {
                                                                    string LandlineNo = row["Contact-Landline (With City Code)"].ToString();
                                                                    ListOfAgent.PAP_CONTACT_LANDLINE_NO = LandlineNo;
                                                                    if (row["Contact-Mobile"] != null && row["Contact-Mobile"].ToString() != "" || row["Contact-Mobile"] == null || row["Contact-Mobile"].ToString() == "")
                                                                    {
                                                                        string ContactMobile = row["Contact-Mobile"].ToString();
                                                                        ListOfAgent.PAP_CONTACT_MOBILE_NO = ContactMobile;
                                                                        if (row["Email  Address (Official  with agency domain)"] != null && row["Email  Address (Official  with agency domain)"].ToString() != "" || row["Email  Address (Official  with agency domain)"] == null || row["Email  Address (Official  with agency domain)"].ToString() == "")
                                                                        {
                                                                            ListOfAgent.PAP_EMAIL_ADDRESS = EmailAddress;
                                                                            if (row["Office Address"] != null && row["Office Address"].ToString() != "" || row["Office Address"] == null || row["Office Address"].ToString() == "")
                                                                            {
                                                                                string OfficeAddress = row["Office Address"].ToString();
                                                                                ListOfAgent.PAP_OFFICE_ADDRESS = OfficeAddress;
                                                                                if (row["City"] != null && row["City"].ToString() != "" || row["City"].ToString() == null || row["City"].ToString() == "")
                                                                                {
                                                                                    string City = row["City"].ToString();
                                                                                    ListOfAgent.PAP_CITY = City;
                                                                                    ListOfAgent.PAP_STATUS = true;
                                                                                    ListOfAgent.PAP_MAKER_ID = Session["USER_ID"].ToString();
                                                                                    if (ListOfAgent.PAP_MAKER_ID == "Admin123")
                                                                                    {
                                                                                        ListOfAgent.PAP_CHECKER_ID = ListOfAgent.PAP_MAKER_ID;
                                                                                        ListOfAgent.PAP_ISAUTH = true;
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        ListOfAgent.PAP_CHECKER_ID = null;
                                                                                        ListOfAgent.PAP_ISAUTH = false;
                                                                                    }
                                                                                    if (CheckIfExist == false)
                                                                                    {
                                                                                        context.PRC_AGENT_PORTOLIO.Add(ListOfAgent);
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        context.Entry(ListOfAgent).State = EntityState.Modified;
                                                                                    }
                                                                                }
                                                                                else
                                                                                {
                                                                                    message += "Invalid City " + row["City"].ToString() + "  At Authorized Officer : " + row["Name of Authorized  Officer"].ToString() + Environment.NewLine;
                                                                                }
                                                                            }
                                                                            else
                                                                            {
                                                                                message += "Invalid Office Address " + row["Office Address"].ToString() + "  At Authorized Officer : " + row["Name of Authorized  Officer"].ToString() + Environment.NewLine;
                                                                            }
                                                                        }
                                                                        else
                                                                        {
                                                                            message += "Invalid Email Address " + row["Email  Address (Official  with agency domain)"].ToString() + "  At Authorized Officer : " + row["Name of Authorized  Officer"].ToString() + Environment.NewLine;
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        message += "Invalid Contact-Mobile " + row["Contact-Mobile"].ToString() + "  At Authorized Officer : " + row["Name of Authorized  Officer"].ToString() + Environment.NewLine;
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    message += "Invalid Contact-Landline (With City Code) " + row["Contact-Landline (With City Code)"].ToString() + "  At Authorized Officer : " + row["Name of Authorized  Officer"].ToString() + Environment.NewLine;
                                                                }
                                                            }
                                                            else
                                                            {
                                                                message += "Invalid CNIC No " + row["CNICNo."].ToString() + "  At Authorized Officer : " + row["Name of Authorized  Officer"].ToString() + Environment.NewLine;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            message += "Invalid Designation " + row["Designation"].ToString() + "  At Authorized Officer : " + row["Name of Authorized  Officer"].ToString() + Environment.NewLine;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        message += "Invalid Agency " + row["Agency/Wing  Name"].ToString() + ".System does not have any authorities like " + row["Agency/Wing  Name"].ToString() + Environment.NewLine;
                                                    }
                                                }
                                                else
                                                {
                                                    message += "Invalid Agency/Wing Name " + row["Agency/Wing  Name"].ToString() + "  At Authorized Officer : " + row["Name of Authorized  Officer"].ToString() + Environment.NewLine;
                                                }
                                            }
                                            else
                                            {
                                                message += "Invalid Officer " + row["Name of Authorized  Officer"].ToString() + "  At Authority : " + row["Authorities"].ToString() + Environment.NewLine;
                                            }
                                        }
                                        else
                                        {
                                            message += "Invalid Authority " + row["Authorities"].ToString() + ".System does not have any authorities like " + row["Authorities"].ToString() + Environment.NewLine;
                                        }
                                    }
                                    RowCount += context.SaveChanges();
                                }
                                if (RowCount > 0)
                                {
                                    message += "Data Uploaded Successfully " + RowCount + " Rows Affected" + Environment.NewLine;
                                }
                            }
                            else
                            {
                                message = "No Data Found In The File." + Environment.NewLine;
                            }
                            if (System.IO.File.Exists(filePath))
                                System.IO.File.Delete(filePath);
                        }
                    }
                    catch (Exception ex)
                    {
                        message += "Exception Occured " + ex.Message + Environment.NewLine;
                        if (ex.InnerException != null)
                        {
                            message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                        }
                    }
                    return CreateLogFile(message);
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }
        [HttpPost]
        public JsonResult AuthorizeVerifyingOfficer(int PAP_ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("Setup", "VerifyingOfficer", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PRC_AGENT_PORTOLIO UpdateEntity = context.PRC_AGENT_PORTOLIO.Where(m => m.PAP_ID == PAP_ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            if (UpdateEntity.PAP_MAKER_ID != Session["USER_ID"].ToString())
                            {
                                UpdateEntity.PAP_ISAUTH = true;
                                UpdateEntity.PAP_CHECKER_ID = Session["USER_ID"].ToString();
                                UpdateEntity.PAP_EDIT_DATETIME = DateTime.Now;
                                context.Entry(UpdateEntity).State = EntityState.Modified;
                                int RowCount = context.SaveChanges();
                                if (RowCount > 0)
                                    message = "Agent : " + UpdateEntity.PAP_NAME_OF_OFFICER + " is successfully Authorized";
                            }
                            else
                                message = "Sorry, Maker cannot authorize the same record.";
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + PAP_ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("Setup", "AuthorizeVerifyingOfficer", Session["USER_ID"].ToString(), ex);
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        [HttpPost]
        public JsonResult RejectVerifyingOfficer(int PAP_ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("Setup", "VerifyingOfficer", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PRC_AGENT_PORTOLIO UpdateEntity = context.PRC_AGENT_PORTOLIO.Where(m => m.PAP_ID == PAP_ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            if (UpdateEntity.PAP_MAKER_ID != Session["USER_ID"].ToString())
                            {
                                context.PRC_AGENT_PORTOLIO.Remove(UpdateEntity);
                                int RowCount = context.SaveChanges();
                                if (RowCount > 0)
                                {
                                    message = "Agent : " + UpdateEntity.PAP_NAME_OF_OFFICER + " is rejected successfully";
                                }
                            }
                            else
                                message = "Sorry, Maker cannot reject the same record.";
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + PAP_ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("Setup", "RejectVerifyingOfficer", Session["USER_ID"].ToString(), ex);
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        #endregion

        #region Department
        public ActionResult Department(int D = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Setup", "Department", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (D != 0)
                        {
                            PRC_DEPARTMENT edit = context.PRC_DEPARTMENT.Where(m => m.PD_ID == D).FirstOrDefault();
                            if (edit != null)
                            {
                                return View(edit);
                            }
                            else
                            {
                                message = "Exception Occur while fetching your record on User Id = " + D;
                                return View();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("Setup", "Department", Session["USER_ID"].ToString(), ex);
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            return View();
        }
        [HttpPost]
        public ActionResult Department(PRC_DEPARTMENT db_table)
        {
            string message = "";
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Setup", "Department", Session["USER_ID"].ToString()))
                {
                    try
                    {
                        PRC_DEPARTMENT UpdateEntity = new PRC_DEPARTMENT();

                        PRC_DEPARTMENT CheckIfExist = new PRC_DEPARTMENT();


                        if (db_table.PD_ID == 0)
                        {
                            CheckIfExist = context.PRC_DEPARTMENT.Where(m => m.PD_NAME == db_table.PD_NAME).FirstOrDefault();
                            if (CheckIfExist == null)
                            {
                                CheckIfExist = new PRC_DEPARTMENT();
                                UpdateEntity.PD_ID = Convert.ToInt32(context.PRC_DEPARTMENT.Max(m => (decimal?)m.PD_ID)) + 1;
                                UpdateEntity.PD_ENTRY_DATETIME = DateTime.Now;
                                CheckIfExist = null;
                            }
                            else
                            {
                                message = "Department : " + db_table.PD_NAME + " is already exist.";
                                return View();
                            }
                        }
                        else
                        {
                            UpdateEntity = context.PRC_DEPARTMENT.Where(m => m.PD_ID == db_table.PD_ID).FirstOrDefault();
                            UpdateEntity.PD_EDIT_DATETIME = DateTime.Now;
                            CheckIfExist = UpdateEntity;
                        }
                        UpdateEntity.PD_NAME = db_table.PD_NAME;
                        UpdateEntity.PD_MAKER_ID = Session["USER_ID"].ToString();
                        if (UpdateEntity.PD_MAKER_ID == "Admin123")
                        {
                            UpdateEntity.PD_CHECKER_ID = UpdateEntity.PD_MAKER_ID;
                            UpdateEntity.PD_ISAUTH = true;
                        }
                        else
                        {
                            UpdateEntity.PD_CHECKER_ID = null;
                            UpdateEntity.PD_ISAUTH = false;
                        }
                        UpdateEntity.PD_STATUS = db_table.PD_STATUS;
                        if (CheckIfExist == null)
                            context.PRC_DEPARTMENT.Add(UpdateEntity);
                        else
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                        int RowsAffected = context.SaveChanges();
                        if (RowsAffected > 0)
                        {
                            message = "Data Inserted Successfully " + RowsAffected + " Affected";
                            ModelState.Clear();
                        }
                        else
                        {
                            message = "Problem While Inserting data " + Environment.NewLine + "Please Contact to Administrator";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("Setup", "Department", Session["USER_ID"].ToString(), ex);
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                    return View();
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }
        [ChildActionOnly]
        public PartialViewResult DepartmentView()
        {
            if (Session["USER_ID"] != null)
            {
                string SessionUser = Session["USER_ID"].ToString();
                List<PRC_DEPARTMENT> ViewData = context.PRC_DEPARTMENT.Where(m => m.PD_ISAUTH == true || m.PD_MAKER_ID != SessionUser).OrderByDescending(m => m.PD_ID).ToList();
                return PartialView(ViewData);
            }
            else
            {
                return PartialView(null);
            }
        }
        [HttpPost]
        public JsonResult AuthorizeDepartment(int PD_ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("Setup", "Department", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PRC_DEPARTMENT UpdateEntity = context.PRC_DEPARTMENT.Where(m => m.PD_ID == PD_ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            if (UpdateEntity.PD_MAKER_ID != Session["USER_ID"].ToString())
                            {
                                UpdateEntity.PD_ISAUTH = true;
                                UpdateEntity.PD_CHECKER_ID = Session["USER_ID"].ToString();
                                UpdateEntity.PD_EDIT_DATETIME = DateTime.Now;
                                context.Entry(UpdateEntity).State = EntityState.Modified;
                                int RowCount = context.SaveChanges();
                                if (RowCount > 0)
                                {
                                    message = "Department : " + UpdateEntity.PD_NAME + " is successfully Authorized";
                                }
                            }
                            else
                                message = "Sorry, Maker cannot authorize the same record.";
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + PD_ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("Setup", "AuthorizeDepartment", Session["USER_ID"].ToString(), ex);
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        [HttpPost]
        public JsonResult RejectDepartment(int PD_ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("Setup", "Department", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PRC_DEPARTMENT UpdateEntity = context.PRC_DEPARTMENT.Where(m => m.PD_ID == PD_ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            if (UpdateEntity.PD_MAKER_ID != Session["USER_ID"].ToString())
                            {
                                context.PRC_DEPARTMENT.Remove(UpdateEntity);
                                int RowCount = context.SaveChanges();
                                if (RowCount > 0)
                                {
                                    message = "Department : " + UpdateEntity.PD_NAME + " is rejected successfully";
                                }
                            }
                            else
                                message = "Sorry, Maker cannot reject the same record.";
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + PD_ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("Setup", "RejectDepartment", Session["USER_ID"].ToString(), ex);
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        #endregion

        #region Purpose Of Verification
        public ActionResult VerificationPurpose(int P = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Setup", "VerificationPurpose", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (P != 0)
                        {
                            PRC_PURPOSE_OF_VERIFICATION edit = context.PRC_PURPOSE_OF_VERIFICATION.Where(m => m.PPV_ID == P).FirstOrDefault();
                            if (edit != null)
                            {
                                return View(edit);
                            }
                            else
                            {
                                message = "Exception Occur while fetching your record on User Id = " + P;
                                return View();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("Setup", "VerificationPurpose", Session["USER_ID"].ToString(), ex);
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
                return RedirectToAction("Index", "Login");
            return View();
        }
        [HttpPost]
        public ActionResult VerificationPurpose(PRC_PURPOSE_OF_VERIFICATION db_table)
        {
            string message = "";
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Setup", "VerificationPurpose", Session["USER_ID"].ToString()))
                {
                    try
                    {
                        PRC_PURPOSE_OF_VERIFICATION UpdateEntity = new PRC_PURPOSE_OF_VERIFICATION();

                        PRC_PURPOSE_OF_VERIFICATION CheckIfExist = new PRC_PURPOSE_OF_VERIFICATION();


                        if (db_table.PPV_ID == 0)
                        {
                            CheckIfExist = context.PRC_PURPOSE_OF_VERIFICATION.Where(m => m.PPV_NAME == db_table.PPV_NAME).FirstOrDefault();
                            if (CheckIfExist == null)
                            {
                                CheckIfExist = new PRC_PURPOSE_OF_VERIFICATION();
                                UpdateEntity.PPV_ID = Convert.ToInt32(context.PRC_PURPOSE_OF_VERIFICATION.Max(m => (decimal?)m.PPV_ID)) + 1;
                                UpdateEntity.PPV_ENTRY_DATETIME = DateTime.Now;
                                CheckIfExist = null;
                            }
                            else
                            {
                                message = "Pusrpose : " + db_table.PPV_NAME + " is already exist.";
                                return View();
                            }
                        }
                        else
                        {
                            UpdateEntity = context.PRC_PURPOSE_OF_VERIFICATION.Where(m => m.PPV_ID == db_table.PPV_ID).FirstOrDefault();
                            UpdateEntity.PPV_EDIT_DATETIME = DateTime.Now;
                            CheckIfExist = UpdateEntity;
                        }
                        UpdateEntity.PPV_NAME = db_table.PPV_NAME;
                        UpdateEntity.PPV_MAKER_ID = Session["USER_ID"].ToString();
                        if (UpdateEntity.PPV_MAKER_ID == "Admin123")
                        {
                            UpdateEntity.PPV_CHECKER_ID = UpdateEntity.PPV_MAKER_ID;
                            UpdateEntity.PPV_ISAUTH = true;
                        }
                        else
                        {
                            UpdateEntity.PPV_CHECKER_ID = null;
                            UpdateEntity.PPV_ISAUTH = false;
                        }
                        UpdateEntity.PPV_STATUS = db_table.PPV_STATUS;
                        if (CheckIfExist == null)
                            context.PRC_PURPOSE_OF_VERIFICATION.Add(UpdateEntity);
                        else
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                        int RowsAffected = context.SaveChanges();
                        if (RowsAffected > 0)
                        {
                            message = "Data Inserted Successfully " + RowsAffected + " Affected";
                            ModelState.Clear();
                        }
                        else
                        {
                            message = "Problem While Inserting data " + Environment.NewLine + "Please Contact to Administrator";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("Setup", "VerificationPurpose", Session["USER_ID"].ToString(), ex);
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                    return View();
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }
        [ChildActionOnly]
        public PartialViewResult VerificationPurposeView()
        {
            if (Session["USER_ID"] != null)
            {
                string SessionUser = Session["USER_ID"].ToString();
                List<PRC_PURPOSE_OF_VERIFICATION> ViewData = context.PRC_PURPOSE_OF_VERIFICATION.Where(m => m.PPV_ISAUTH == true || m.PPV_MAKER_ID != SessionUser).OrderByDescending(m => m.PPV_ID).ToList();
                return PartialView(ViewData);
            }
            else
            {
                return PartialView(null);
            }
        }
        [HttpPost]
        public JsonResult AuthorizeVerificationPurpose(int PPV_ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("Setup", "VerificationPurpose", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PRC_PURPOSE_OF_VERIFICATION UpdateEntity = context.PRC_PURPOSE_OF_VERIFICATION.Where(m => m.PPV_ID == PPV_ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            if (UpdateEntity.PPV_MAKER_ID != Session["USER_ID"].ToString())
                            {
                                UpdateEntity.PPV_ISAUTH = true;
                                UpdateEntity.PPV_CHECKER_ID = Session["USER_ID"].ToString();
                                UpdateEntity.PPV_EDIT_DATETIME = DateTime.Now;
                                context.Entry(UpdateEntity).State = EntityState.Modified;
                                int RowCount = context.SaveChanges();
                                if (RowCount > 0)
                                {
                                    message = UpdateEntity.PPV_NAME + " is successfully Authorized";
                                }
                            }
                            else
                                message = "Sorry, Maker cannot authorize the same record.";
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + PPV_ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("Setup", "AuthorizeVerificationPurpose", Session["USER_ID"].ToString(), ex);
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        [HttpPost]
        public JsonResult RejectVerificationPurpose(int PPV_ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("Setup", "VerificationPurpose", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PRC_PURPOSE_OF_VERIFICATION UpdateEntity = context.PRC_PURPOSE_OF_VERIFICATION.Where(m => m.PPV_ID == PPV_ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            if (UpdateEntity.PPV_MAKER_ID != Session["USER_ID"].ToString())
                            {
                                context.PRC_PURPOSE_OF_VERIFICATION.Remove(UpdateEntity);
                                int RowCount = context.SaveChanges();
                                if (RowCount > 0)
                                {
                                    message = UpdateEntity.PPV_NAME + " is rejected successfully";
                                }
                            }
                            else
                                message = "Sorry, Maker cannot reject the same record.";
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + PPV_ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("Setup", "RejectVerificationPurpose", Session["USER_ID"].ToString(), ex);
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        #endregion

        #region Manage Email Setup
        public ActionResult EmailSetup(int E = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Setup", "EmailSetup", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (E != 0)
                        {
                            PRC_EMAIL_CONFIGRATION edit = context.PRC_EMAIL_CONFIGRATION.Where(m => m.EC_ID == E).FirstOrDefault();
                            if (edit != null)
                            {
                                return View(edit);
                            }
                            else
                            {
                                message = "Exception Occur while fetching your record on User Id = " + E;
                                return View();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("Setup", "EmailSetup", Session["USER_ID"].ToString(), ex);
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
            return View();
        }
        [HttpPost]
        public ActionResult EmailSetup(PRC_EMAIL_CONFIGRATION db_table)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Setup", "EmailSetup", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PRC_EMAIL_CONFIGRATION UpdateEntity = new PRC_EMAIL_CONFIGRATION();

                        PRC_EMAIL_CONFIGRATION CheckIfExist = new PRC_EMAIL_CONFIGRATION();
                        if (db_table.EC_ID == 0)
                        {
                            UpdateEntity.EC_ID = Convert.ToInt32(context.PRC_EMAIL_CONFIGRATION.Max(m => (decimal?)m.EC_ID)) + 1;
                            UpdateEntity.EC_ENTRY_DATETIME = DateTime.Now;
                            CheckIfExist = null;
                        }
                        else
                        {
                            UpdateEntity = context.PRC_EMAIL_CONFIGRATION.Where(m => m.EC_ID == db_table.EC_ID).FirstOrDefault();
                            UpdateEntity.EC_EDIT_DATETIME = DateTime.Now;
                            CheckIfExist = UpdateEntity;
                        }
                        UpdateEntity.EC_SEREVER_HOST = db_table.EC_SEREVER_HOST;
                        UpdateEntity.EC_CREDENTIAL_ID = db_table.EC_CREDENTIAL_ID;
                        UpdateEntity.EC_EMAIL_PORT = db_table.EC_EMAIL_PORT;
                        //pdateEntity.EC_CC_EMAIL = db_table.EC_CC_EMAIL;
                        UpdateEntity.EC_STATUS = false;
                        UpdateEntity.EC_MAKER_ID = Session["USER_ID"].ToString();
                        if (UpdateEntity.EC_MAKER_ID == "Admin123")
                        {
                            UpdateEntity.EC_CHECKER_ID = UpdateEntity.EC_MAKER_ID;
                            UpdateEntity.EC_IS_AUTH = true;
                        }
                        else
                        {
                            UpdateEntity.EC_CHECKER_ID = null;
                            UpdateEntity.EC_IS_AUTH = false;
                        }
                        if (CheckIfExist == null)
                            context.PRC_EMAIL_CONFIGRATION.Add(UpdateEntity);
                        else
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                        int RowsAffected = context.SaveChanges();
                        if (RowsAffected > 0)
                        {
                            message = "Data Inserted Successfully " + RowsAffected + " Affected";
                            ModelState.Clear();
                        }
                        else
                        {
                            message = "Problem While Inserting data " + Environment.NewLine + "Please Contact to Administrator";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("Setup", "EmailSetup", Session["USER_ID"].ToString(), ex);
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
            return View();
        }
        [ChildActionOnly]
        public PartialViewResult ManageEmailSetupList()
        {
            if (Session["USER_ID"] != null)
            {
                string SessionUser = Session["USER_ID"].ToString();
                List<PRC_EMAIL_CONFIGRATION> Data = context.PRC_EMAIL_CONFIGRATION.Where(m => m.EC_MAKER_ID != SessionUser || m.EC_IS_AUTH == true).ToList();
                return PartialView(Data);
            }
            else
            {
                return PartialView(null);
            }
        }
        [HttpPost]
        public JsonResult AuthorizeEmailSetup(int EC_ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("Setup", "EmailSetup", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PRC_EMAIL_CONFIGRATION UpdateEntity = context.PRC_EMAIL_CONFIGRATION.Where(m => m.EC_ID == EC_ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            if (UpdateEntity.EC_MAKER_ID != Session["USER_ID"].ToString())
                            {
                                UpdateEntity.EC_IS_AUTH = true;
                                UpdateEntity.EC_CHECKER_ID = Session["USER_ID"].ToString();
                                UpdateEntity.EC_EDIT_DATETIME = DateTime.Now;
                                context.Entry(UpdateEntity).State = EntityState.Modified;
                                int RowCount = context.SaveChanges();
                                if (RowCount > 0)
                                {
                                    message = "Setup Email : " + UpdateEntity.EC_CREDENTIAL_ID + " is successfully Authorized";
                                }
                            }
                            else
                                message = "Sorry, Maker cannot authorize the same record.";
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + EC_ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("Setup", "AuthorizeEmailSetup", Session["USER_ID"].ToString(), ex);
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        [HttpPost]
        public JsonResult RejectEmailSetup(int EC_ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("Setup", "EmailSetup", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PRC_EMAIL_CONFIGRATION UpdateEntity = context.PRC_EMAIL_CONFIGRATION.Where(m => m.EC_ID == EC_ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            if (UpdateEntity.EC_MAKER_ID != Session["USER_ID"].ToString())
                            {
                                context.PRC_EMAIL_CONFIGRATION.Remove(UpdateEntity);
                                int RowCount = context.SaveChanges();
                                if (RowCount > 0)
                                {
                                    message = "Setup Email : " + UpdateEntity.EC_CREDENTIAL_ID + " is rejected successfully";
                                }
                            }
                            else
                                message = "Sorry, Maker cannot reject the same record.";
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + EC_ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("Setup", "RejectEmailSetup", Session["USER_ID"].ToString(), ex);
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        [HttpPost]
        public JsonResult UpdateEmailSetupStatus(int EC_ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("Setup", "EmailSetup", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        int RowCount = 0;
                        PRC_EMAIL_CONFIGRATION EntityToUpdate = new PRC_EMAIL_CONFIGRATION();
                        List<PRC_EMAIL_CONFIGRATION> DeactiveStatus = context.PRC_EMAIL_CONFIGRATION.Where(m => m.EC_ID != EC_ID).ToList();
                        if (DeactiveStatus.Count > 0)
                        {
                            foreach (PRC_EMAIL_CONFIGRATION item in DeactiveStatus)
                            {
                                PRC_EMAIL_CONFIGRATION GetListOfEmails = context.PRC_EMAIL_CONFIGRATION.Where(m => m.EC_ID == item.EC_ID).FirstOrDefault();
                                if (GetListOfEmails != null)
                                {
                                    EntityToUpdate = GetListOfEmails;
                                    EntityToUpdate.EC_STATUS = false;
                                    context.Entry(EntityToUpdate).State = EntityState.Modified;
                                    RowCount += context.SaveChanges();
                                }
                            }
                            if (RowCount > 0)
                            {
                                RowCount = 0;
                                PRC_EMAIL_CONFIGRATION CheckEntity = context.PRC_EMAIL_CONFIGRATION.Where(m => m.EC_ID == EC_ID).FirstOrDefault();
                                if (CheckEntity != null)
                                {
                                    EntityToUpdate = new PRC_EMAIL_CONFIGRATION();
                                    EntityToUpdate = CheckEntity;
                                    EntityToUpdate.EC_STATUS = true;
                                    context.Entry(EntityToUpdate).State = EntityState.Modified;
                                    RowCount = context.SaveChanges();
                                    if (RowCount > 0)
                                        message = "Current email server is changed to : " + CheckEntity.EC_CREDENTIAL_ID;
                                }

                            }
                        }
                        else
                        {
                            List<PRC_EMAIL_CONFIGRATION> CheckEmail = context.PRC_EMAIL_CONFIGRATION.ToList();
                            if (CheckEmail.Count() == 1)
                            {
                                PRC_EMAIL_CONFIGRATION CheckEntity = context.PRC_EMAIL_CONFIGRATION.Where(m => m.EC_ID == EC_ID).FirstOrDefault();
                                if (CheckEntity != null)
                                {
                                    EntityToUpdate = new PRC_EMAIL_CONFIGRATION();
                                    EntityToUpdate = CheckEntity;
                                    EntityToUpdate.EC_STATUS = true;
                                    context.Entry(EntityToUpdate).State = EntityState.Modified;
                                    RowCount = context.SaveChanges();
                                    if (RowCount > 0)
                                        message = "Current email server is changed to : " + CheckEntity.EC_CREDENTIAL_ID;
                                }
                            }
                            else
                                message = "Problem while fetching your record on ID# " + EC_ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("Setup", "UpdateEmailSetupStatus", Session["USER_ID"].ToString(), ex);
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        #endregion

        #region Setup

        #region System Setting
        public ActionResult SystemSetting(int S = 0,int AE = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Setup", "SystemSetting", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (S != 0)
                        {
                            TestingInputs Entity = new TestingInputs();
                            Entity.SettingParms = context.PRC_SYSYTEM_SETUP.Where(m => m.PSS_ID == S).FirstOrDefault();
                            if (Entity.SettingParms != null)
                            {
                                Entity.SettingParms.PSS_GCAR_CERT_HASH = DAL.Decrypt(Entity.SettingParms.PSS_GCAR_CERT_HASH);
                                return View(Entity);
                            }
                            else
                            {
                                message = "Exception Occur while fetching your record on User Id = " + S;
                                return View();
                            }
                        }
                        if (AE != 0)
                        {
                            TestingInputs Entity = new TestingInputs();
                            Entity.AutoEmailing = context.PRC_AUTO_EMAILING.Where(m => m.PAE_ID == AE).FirstOrDefault();
                            if (Entity.AutoEmailing != null)
                            {
                                return View(Entity);
                            }
                            else
                            {
                                message = "Exception Occur while fetching your record on User Id = " + S;
                                return View();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("Setup", "SystemSetting", Session["USER_ID"].ToString(), ex);
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
            return View();
        }
        [HttpPost]
        public ActionResult SystemSetting(TestingInputs db_table)
        {
            string message = "";
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Setup", "SystemSetting", Session["USER_ID"].ToString()))
                {
                    try
                    {
                        if (db_table.SettingParms.PSS_ID != 0)
                        {
                            PRC_SYSYTEM_SETUP UpdateEntity = context.PRC_SYSYTEM_SETUP.Where(m => m.PSS_ID == db_table.SettingParms.PSS_ID).FirstOrDefault();
                            if (UpdateEntity != null)
                            {
                                UpdateEntity.PSS_EDIT_DATETIME = DateTime.Now;
                                UpdateEntity.PSS_GCAR_API_URL = db_table.SettingParms.PSS_GCAR_API_URL;
                                UpdateEntity.PSS_GCAR_CERT_HASH = DAL.Encrypt(db_table.SettingParms.PSS_GCAR_CERT_HASH);
                                UpdateEntity.PSS_LOGOUT_CITI_DIRECT = db_table.SettingParms.PSS_LOGOUT_CITI_DIRECT;
                                UpdateEntity.PSS_CHANGE_PASSWORD_CITI_DIRECT = db_table.SettingParms.PSS_CHANGE_PASSWORD_CITI_DIRECT;
                                UpdateEntity.PSS_LOGOUT_CITI_VELOCITY = db_table.SettingParms.PSS_LOGOUT_CITI_VELOCITY;
                                UpdateEntity.PSS_CHANGE_PASSWORD_CITI_VELOCITY = db_table.SettingParms.PSS_CHANGE_PASSWORD_CITI_VELOCITY;
                                UpdateEntity.PSS_MAKER_ID = Session["USER_ID"].ToString();
                                if (UpdateEntity.PSS_MAKER_ID == "Admin123")
                                {
                                    UpdateEntity.PSS_CHECKER_ID = UpdateEntity.PSS_MAKER_ID;
                                    UpdateEntity.PSS_ISAUTH = true;
                                }
                                else
                                {
                                    UpdateEntity.PSS_CHECKER_ID = null;
                                    UpdateEntity.PSS_ISAUTH = false;
                                }
                                context.Entry(UpdateEntity).State = EntityState.Modified;
                                int RowsAffected = context.SaveChanges();
                                if (RowsAffected > 0)
                                {
                                    message = "Data Updated Successfully " + RowsAffected + " Affected";
                                    ModelState.Clear();
                                }
                                else
                                {
                                    message = "Problem While updating data " + Environment.NewLine + "Please Contact to Administrator";
                                }
                            }
                            else
                            {
                                message = "Problem While fetching data " + Environment.NewLine + "Please Contact to Administrator";
                            }
                        }
                        else
                        {
                            message = "Select system setting before continue";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("Setup", "SystemSetting", Session["USER_ID"].ToString(), ex);
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                    return View();
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }
        [ChildActionOnly]
        public PartialViewResult SystemSettingView()
        {
            if (Session["USER_ID"] != null)
            {
                string SessionUser = Session["USER_ID"].ToString();
                List<PRC_SYSYTEM_SETUP> ViewData = context.PRC_SYSYTEM_SETUP.Where(m => m.PSS_ISAUTH == true || m.PSS_MAKER_ID != SessionUser).OrderByDescending(m => m.PSS_ID).ToList();
                return PartialView(ViewData);
            }
            else
            {
                return PartialView(null);
            }
        }
        [HttpPost]
        public JsonResult AuthorizeSystemSetting(int PSS_ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("Setup", "SystemSetting", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PRC_SYSYTEM_SETUP UpdateEntity = context.PRC_SYSYTEM_SETUP.Where(m => m.PSS_ID == PSS_ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            if (UpdateEntity.PSS_MAKER_ID != Session["USER_ID"].ToString())
                            {
                                UpdateEntity.PSS_ISAUTH = true;
                                UpdateEntity.PSS_CHECKER_ID = Session["USER_ID"].ToString();
                                UpdateEntity.PSS_EDIT_DATETIME = DateTime.Now;
                                context.Entry(UpdateEntity).State = EntityState.Modified;
                                int RowCount = context.SaveChanges();
                                if (RowCount > 0)
                                {
                                    message = "Successfully Authorized";
                                }
                            }
                            else
                                message = "Sorry, Maker cannot authorize the same record.";
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + PSS_ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("Setup", "AuthorizeSystemSetting", Session["USER_ID"].ToString(), ex);
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        [HttpPost]
        public JsonResult RejectSystemSetting(int PSS_ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("Setup", "SystemSetting", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PRC_SYSYTEM_SETUP UpdateEntity = context.PRC_SYSYTEM_SETUP.Where(m => m.PSS_ID == PSS_ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            if (UpdateEntity.PSS_MAKER_ID != Session["USER_ID"].ToString())
                            {
                                context.PRC_SYSYTEM_SETUP.Remove(UpdateEntity);
                                int RowCount = context.SaveChanges();
                                if (RowCount > 0)
                                {
                                    message = "Rejected successfully";
                                }
                            }
                            else
                                message = "Sorry, Maker cannot reject the same record.";
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + PSS_ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("Setup", "RejectSystemSetting", Session["USER_ID"].ToString(), ex);
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        #endregion

        #region Main Request
        [HttpPost]
        public ActionResult ManReq(TestingInputs Entity)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("PRC", "ManReq", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PRC_SYSYTEM_SETUP SystemConfig = context.PRC_SYSYTEM_SETUP.Where(m => m.PSS_ISAUTH == true && m.PSS_GCAR_API_URL != null && m.PSS_GCAR_CERT_HASH != null).FirstOrDefault();
                        if (SystemConfig != null)
                        {
                            if (Entity.GCARParams.CLIENT_ID != null && Entity.GCARParams.IBAN_ACCTNO != null && Entity.GCARParams.UNIQUE_REF != null)
                            {
                                string CertificateDirectory = Path.Combine(HostingEnvironment.ApplicationPhysicalPath, "ClientCertificates\\GCAR");

                                if (Directory.Exists(CertificateDirectory))
                                {
                                    string firstFileName = Directory.GetFiles(CertificateDirectory).FirstOrDefault();
                                    if (firstFileName != null)
                                    {
                                        if (firstFileName.Contains(".pfx"))
                                        {
                                            string CertPath = Path.Combine(CertificateDirectory, firstFileName);
                                            if (System.IO.File.Exists(CertPath))
                                            {
                                                message = DAL.SendGidaRequestWithCertPath(Entity.GCARParams.CLIENT_ID, Entity.GCARParams.IBAN_ACCTNO, Entity.GCARParams.UNIQUE_REF, SystemConfig.PSS_GCAR_API_URL, CertPath, DAL.Decrypt(SystemConfig.PSS_GCAR_CERT_HASH));
                                                if (message == "VALIDATION_STAT = ACK")
                                                {
                                                    message = "GCAR Request was successfull with response ACK";
                                                }

                                            }
                                            else
                                            {
                                                message = "unable to find file @ " + CertPath;
                                            }
                                        }
                                        else
                                        {
                                            message = "cant find a pfx file under client certificates folder";
                                        }
                                    }
                                    else
                                    {
                                        message = "no file exist in the client certificates";
                                    }

                                }
                                else
                                {
                                    message = "Client certificates directory does not exist on the server";
                                }
                            }
                            else
                            {
                                if (Entity.GCARParams.UNIQUE_REF == null)
                                    message = "Enter Unique Reference";
                                else if (Entity.GCARParams.CLIENT_ID == null)
                                    message = "Enter Client Id";
                                else if (Entity.GCARParams.IBAN_ACCTNO == null)
                                    message = "Enter Iban/Account Number";
                                else
                                    message = "Enter Data to continue";

                            }
                            return View("SystemSetting");
                        }
                        else
                        {
                            message = "No Configration for API Url & Cert Hash found in the system";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
            return View("SystemSetting");
        }
        #endregion

        #region Auto Emailing
        [HttpPost]
        public ActionResult AutoEmailing(TestingInputs Entity)
        {
            string message = "";
            int RowCount = 0;
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Setup", "SystemSetting", Session["USER_ID"].ToString()))
                {
                    try
                    {
                        PRC_AUTO_EMAILING Update = new PRC_AUTO_EMAILING();
                        if (Entity.AutoEmailing.PAE_ID == 0)
                        {
                            Update.PAE_ID = Convert.ToInt32(context.PRC_AUTO_EMAILING.Max(m => (decimal?)m.PAE_ID)) + 1;
                            Update.PAE_EXECUTION_DATE_N_MONTH = Entity.AutoEmailing.PAE_EXECUTION_DATE_N_MONTH;
                            Update.PAE_STATUS = false;
                            Update.PAE_DATA_RANGE_FROM_DATE = Entity.AutoEmailing.PAE_DATA_RANGE_FROM_DATE;
                            Update.PAE_DATA_RANGE_TO = Entity.AutoEmailing.PAE_DATA_RANGE_TO;
                            Update.PAE_ADDED_DATE = DateTime.Now;
                            Update.PAE_MAKER_ID = Session["USER_ID"].ToString();
                            Update.PAE_CHECKER_ID = null;
                            Update.PAE_EDIT_DATE = null;
                            context.PRC_AUTO_EMAILING.Add(Update);
                            RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                message = "Data inserted successfully " + RowCount + " affected";
                                ModelState.Clear();
                            }
                        }
                        else
                        {
                            Update = context.PRC_AUTO_EMAILING.Where(m => m.PAE_ID == Entity.AutoEmailing.PAE_ID).FirstOrDefault();
                            if (Update != null)
                            {
                                Update.PAE_EXECUTION_DATE_N_MONTH = Entity.AutoEmailing.PAE_EXECUTION_DATE_N_MONTH;
                                Update.PAE_STATUS = false;
                                Update.PAE_DATA_RANGE_FROM_DATE = Entity.AutoEmailing.PAE_DATA_RANGE_FROM_DATE;
                                Update.PAE_DATA_RANGE_TO = Entity.AutoEmailing.PAE_DATA_RANGE_TO;
                                Update.PAE_CHECKER_ID = Session["USER_ID"].ToString();
                                Update.PAE_EDIT_DATE = DateTime.Now;
                                context.Entry(Update).State = EntityState.Modified;
                                RowCount = context.SaveChanges();
                                if (RowCount > 0)
                                {
                                    message = "Data updated successfully " + RowCount + " affected";
                                    ModelState.Clear();
                                }
                            }
                            else
                            {
                                message = "Error while fetching record on selected ID " + Entity.AutoEmailing.PAE_ID;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("Setup", "AutoEmailing", Session["USER_ID"].ToString(), ex);
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                    return View("SystemSetting");
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }
        [ChildActionOnly]
        public PartialViewResult AutoEmailingView()
        {
            if (Session["USER_ID"] != null)
            {
                string SessionUser = Session["USER_ID"].ToString();
                List<PRC_AUTO_EMAILING> ViewData = context.PRC_AUTO_EMAILING.OrderByDescending(m => m.PAE_ID).ToList();
                return PartialView(ViewData);
            }
            else
            {
                return PartialView(null);
            }
        }
        #endregion

        #endregion
    }
}