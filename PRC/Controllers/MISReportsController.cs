﻿using PRC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PRC.Controllers
{
    public class MISReportsController : Controller
    {
        // GET: MISReports
        PRCEntities context = new PRCEntities();
        #region Request Log
        public ActionResult RequestLog()
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("MISReports", "RequestLog", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {

                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
            return View();
        }
        [HttpPost]
        public ActionResult RequestLog(DateTime? FromDate, DateTime? ToDate, string ClientEmail)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("MISReports", "RequestLog", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (FromDate != null && ToDate != null)
                        {
                            var Data = context.PRC_CLIENT_ePRC_sPRC_REQUEST_LOG(FromDate, ToDate, ClientEmail).OrderByDescending(m => m.PRL_REQUEST_DATE).ToList();
                            if (Data.Count() > 0)
                            {
                                return View(Data);
                            }
                            else
                            {
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy") + " End date : " + Convert.ToDateTime(ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy");
                            }
                        }
                        else
                        {
                            if (FromDate == null && ToDate == null)
                                message = "Select start date and end date to continue";
                            else if (FromDate == null)
                                message = "Select start date to continue";
                            else if (ToDate == null)
                                message = "Select end date to continue";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                        DAL.LogException("MISReports", "RequestLog", Session["USER_ID"].ToString(), ex);
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
            return View();
        }
        #endregion

        #region Client Request
        public ActionResult ClientRequest()
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("MISReports", "ClientRequest", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        return View();
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
            return View();
        }
        [HttpPost]
        public ActionResult ClientRequest(DateTime? FromDate, DateTime? ToDate, string ClientName, string AccountNo, string PRCNumber, string BankRefNo)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("MISReports", "ClientRequest", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (FromDate != null && ToDate != null)
                        {
                            var Data = context.PRC_CLIENT_REQUEST_PROC(FromDate, ToDate,ClientName, AccountNo,PRCNumber, BankRefNo).OrderByDescending(m => m.PRL_REQUEST_DATE).ToList();
                            if (Data.Count() > 0)
                            {
                                return View(Data);
                            }
                            else
                            {
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy") + " End date : " + Convert.ToDateTime(ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy");
                            }
                        }
                        else
                        {
                            if (FromDate == null && ToDate == null)
                                message = "Select start date and end date to continue";
                            else if (FromDate == null)
                                message = "Select start date to continue";
                            else if (ToDate == null)
                                message = "Select end date to continue";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                        DAL.LogException("MISReports", "ClientRequest", Session["USER_ID"].ToString(), ex);
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
            return View();
        }
        #endregion

        #region PRC Report
        public ActionResult PRCReport()
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("MISReports", "PRCReport", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        return View();
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
            return View();
        }
        [HttpPost]
        public ActionResult PRCReport(DateTime? FromDate, DateTime? ToDate, string ClientName, string AccountNo, string PRCNumber, string BankRefNo)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("MISReports", "PRCReport", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (FromDate != null && ToDate != null)
                        {
                            var Data = context.PRC_PRC_REPORT_PROC(FromDate, ToDate, ClientName, AccountNo, PRCNumber, BankRefNo).OrderByDescending(m => m.GPD_UPLOAD_DATE).ToList();
                            if (Data.Count() > 0)
                            {
                                return View(Data);
                            }
                            else
                            {
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy") + " End date : " + Convert.ToDateTime(ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy");
                            }
                        }
                        else
                        {
                            if (FromDate == null && ToDate == null)
                                message = "Select start date and end date to continue";
                            else if (FromDate == null)
                                message = "Select start date to continue";
                            else if (ToDate == null)
                                message = "Select end date to continue";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                        DAL.LogException("MISReports", "PRCReport", Session["USER_ID"].ToString(), ex);
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
            return View();
        }
        #endregion

        #region Verification Authority
        public ActionResult VerificationAuthority()
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("MISReports", "VerificationAuthority", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        return View();
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
            return View();
        }
        [HttpPost]
        public ActionResult VerificationAuthority(DateTime? FromDate, DateTime? ToDate, string OfficerName, string CNIC_NO, string PRCNumber)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("MISReports", "VerificationAuthority", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (FromDate != null && ToDate != null)
                        {
                            var Data = context.PRC_VERIFICATION_AUTHORITY_PROC(FromDate, ToDate, OfficerName, CNIC_NO, PRCNumber).OrderByDescending(m => m.PERL_REQUESTED_DATE).ToList();
                            if (Data.Count() > 0)
                            {
                                return View(Data);
                            }
                            else
                            {
                                message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy") + " End date : " + Convert.ToDateTime(ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy");
                            }
                        }
                        else
                        {
                            if (FromDate == null && ToDate == null)
                                message = "Select start date and end date to continue";
                            else if (FromDate == null)
                                message = "Select start date to continue";
                            else if (ToDate == null)
                                message = "Select end date to continue";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                        DAL.LogException("MISReports", "VerificationAuthority", Session["USER_ID"].ToString(), ex);
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
            return View();
        }
        #endregion
    }
}