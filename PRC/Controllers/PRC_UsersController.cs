﻿using PRC.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PRC.Controllers
{
    public class PRC_UsersController : Controller
    {
        PRCEntities context = new PRCEntities();
        // GET: PRC_Users
        public ActionResult AddUser(int L = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("PRC_Users", "AddUser", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    AssignRoles_Custom entity = new AssignRoles_Custom();
                    try
                    {
                        if (L != 0)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            entity.Entity = context.PRC_LOGIN.Where(m => m.PLOG_ID == L && m.PLOG_USER_ID != SessionUser).FirstOrDefault();
                            if (entity.Entity != null)
                            {
                                List<int?> PreviousRoles = context.PRC_ASSIGN_LOGIN_RIGHTS.Where(m => m.PALR_LOG_ID == L && m.PALR_STATUS == true).Select(m => m.PALR_ROLE_ID).ToList();
                                List<SelectListItem> items = new SelectList(context.PRC_ROLES.Where(m => m.PR_NAME != "ADMIN" && m.PR_STATUS == "true" && m.PR_ISAUTH == true).OrderBy(m => m.PR_ID).ToList(), "PR_ID", "PR_NAME", 0).ToList();
                                foreach (SelectListItem item in items)
                                {
                                    int? CurrentRId = Convert.ToInt32(item.Value);
                                    if (PreviousRoles.Contains(CurrentRId))
                                        item.Selected = true;
                                }
                                entity.RolesList = items;
                                ViewBag.FormType = "Edit";
                                return View(entity);
                            }
                            else
                            {
                                entity.Entity = new PRC_LOGIN();
                                entity.Entity.PLOG_ID = 0;
                                List<SelectListItem> items = new SelectList(context.PRC_ROLES.Where(m => m.PR_NAME != "ADMIN" && m.PR_STATUS == "true" && m.PR_ISAUTH == true).OrderBy(m => m.PR_ID).ToList(), "PR_ID", "PR_NAME", 0).ToList();
                                entity.RolesList = items;
                                message = "Exception Occur while fetching your record on User Id = " + L;
                                return View(entity);
                            }
                        }
                        else
                        {
                            entity.Entity = new PRC_LOGIN();
                            entity.Entity.PLOG_ID = 0;
                            List<SelectListItem> items = new SelectList(context.PRC_ROLES.Where(m => m.PR_NAME != "ADMIN" && m.PR_STATUS == "true" && m.PR_ISAUTH == true).OrderBy(m => m.PR_ID).ToList(), "PR_ID", "PR_NAME", 0).ToList();
                            entity.RolesList = items;
                            return View(entity);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("PRC_Users", "AddUser", Session["USER_ID"].ToString(), ex);
                        entity.Entity = new PRC_LOGIN();
                        entity.Entity.PLOG_ID = 0;
                        List<SelectListItem> items = new SelectList(context.PRC_ROLES.Where(m => m.PR_NAME != "ADMIN" && m.PR_STATUS == "true" && m.PR_ISAUTH == true).OrderBy(m => m.PR_ID).ToList(), "PR_ID", "PR_NAME", 0).ToList();
                        entity.RolesList = items;
                        return View(entity);
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }
        [HttpPost]
        public ActionResult AddUser(AssignRoles_Custom db_table)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("PRC_Users", "AddUser", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PRC_LOGIN UpdateEntity = new PRC_LOGIN();
                        PRC_LOGIN CheckIfExist = new PRC_LOGIN();
                        if (db_table.Entity.PLOG_ID == 0)
                        {
                            CheckIfExist = context.PRC_LOGIN.Where(m => m.PLOG_USER_ID.ToLower() == db_table.Entity.PLOG_USER_ID.ToLower()).FirstOrDefault();
                            if (CheckIfExist == null)
                            {
                                UpdateEntity.PLOG_ID = Convert.ToInt32(context.PRC_LOGIN.Max(m => (decimal?)m.PLOG_ID)) + 1;
                                UpdateEntity.PLOG_DATA_ENTRY_DATETIME = DateTime.Now;
                                UpdateEntity.PLOG_USER_ID = db_table.Entity.PLOG_USER_ID;
                                UpdateEntity.PLOG_USER_NAME = db_table.Entity.PLOG_USER_NAME;
                                UpdateEntity.PLOG_USER_LAST_NAME = db_table.Entity.PLOG_USER_LAST_NAME;
                                UpdateEntity.PLOG_USER_GE_ID = db_table.Entity.PLOG_USER_GE_ID;
                                UpdateEntity.PLOG_EMAIL = db_table.Entity.PLOG_EMAIL;
                                UpdateEntity.PLOG_RIST_ID = db_table.Entity.PLOG_RIST_ID;
                                UpdateEntity.PLOG_USER_STATUS = db_table.Entity.PLOG_USER_STATUS;
                                UpdateEntity.PLOG_MAKER_ID = Session["USER_ID"].ToString();
                                if (UpdateEntity.PLOG_MAKER_ID == "Admin123")
                                {
                                    UpdateEntity.PLOG_ISAUTH = true;
                                    UpdateEntity.PLOG_CHECKER_ID = UpdateEntity.PLOG_MAKER_ID;
                                }
                                else
                                {
                                    UpdateEntity.PLOG_ISAUTH = false;
                                    UpdateEntity.PLOG_CHECKER_ID = null;
                                }
                                context.PRC_LOGIN.Add(UpdateEntity);
                            }
                            else
                            {
                                message = "A User with the given " + db_table.Entity.PLOG_USER_ID + " Already Exist";
                            }
                        }
                        else
                        {
                            UpdateEntity = context.PRC_LOGIN.Where(m => m.PLOG_ID == db_table.Entity.PLOG_ID).FirstOrDefault();
                            UpdateEntity.PLOG_DATA_EDIT_DATETIME = DateTime.Now;
                            UpdateEntity = context.PRC_LOGIN.Where(m => m.PLOG_ID == db_table.Entity.PLOG_ID).FirstOrDefault();
                            UpdateEntity.PLOG_DATA_EDIT_DATETIME = DateTime.Now;
                            UpdateEntity.PLOG_USER_ID = db_table.Entity.PLOG_USER_ID;
                            UpdateEntity.PLOG_USER_NAME = db_table.Entity.PLOG_USER_NAME;
                            UpdateEntity.PLOG_USER_LAST_NAME = db_table.Entity.PLOG_USER_LAST_NAME;
                            UpdateEntity.PLOG_USER_GE_ID = db_table.Entity.PLOG_USER_GE_ID;
                            UpdateEntity.PLOG_EMAIL = db_table.Entity.PLOG_EMAIL;
                            UpdateEntity.PLOG_RIST_ID = db_table.Entity.PLOG_RIST_ID;
                            UpdateEntity.PLOG_USER_STATUS = db_table.Entity.PLOG_USER_STATUS;
                            UpdateEntity.PLOG_MAKER_ID = Session["USER_ID"].ToString();
                            if (UpdateEntity.PLOG_MAKER_ID == "Admin123")
                            {
                                UpdateEntity.PLOG_ISAUTH = true;
                                UpdateEntity.PLOG_CHECKER_ID = UpdateEntity.PLOG_MAKER_ID;
                            }
                            else
                            {
                                UpdateEntity.PLOG_ISAUTH = false;
                                UpdateEntity.PLOG_CHECKER_ID = null;
                            }

                            context.Entry(UpdateEntity).State = EntityState.Modified;
                        }
                        int RowsAffected = context.SaveChanges();
                        if (RowsAffected > 0)
                        {
                            string TempNewUser = "";
                            string TempRemoveUser = "";
                            if (db_table.SelectedRoles != null)
                            {
                                #region Get Groups for Log Table
                                foreach (int RoleId in db_table.SelectedRoles)
                                {
                                    PRC_ASSIGN_LOGIN_RIGHTS RoleTbl = context.PRC_ASSIGN_LOGIN_RIGHTS.Where(m => m.PALR_ROLE_ID == RoleId && m.PALR_USER_ID == UpdateEntity.PLOG_USER_ID && m.PALR_STATUS == true).FirstOrDefault();
                                    if (RoleTbl == null)
                                    {
                                        PRC_ROLES GetRoleName = context.PRC_ROLES.Where(m => m.PR_ID == RoleId).FirstOrDefault();
                                        TempNewUser += GetRoleName.PR_NAME + ",";
                                    }
                                }
                                #endregion

                                List<PRC_ASSIGN_LOGIN_RIGHTS> DeActivatedRoles = context.PRC_ASSIGN_LOGIN_RIGHTS.Where(m => m.PALR_LOG_ID == db_table.Entity.PLOG_ID && !db_table.SelectedRoles.Contains(m.PALR_ROLE_ID)).ToList();
                                if (DeActivatedRoles.Count > 0)
                                {
                                    foreach (PRC_ASSIGN_LOGIN_RIGHTS EachRolesForUpdate in DeActivatedRoles)
                                    {
                                        PRC_ROLES RoleTbl = context.PRC_ROLES.Where(m => m.PR_ID == EachRolesForUpdate.PALR_ROLE_ID).FirstOrDefault();
                                        TempRemoveUser += RoleTbl.PR_NAME + ",";
                                        EachRolesForUpdate.PALR_STATUS = false;
                                        context.Entry(EachRolesForUpdate).State = EntityState.Modified;
                                    }
                                }
                                int AssignRoleId = 0;
                                foreach (int? SelectedRolesId in db_table.SelectedRoles)
                                {
                                    if (SelectedRolesId != null)
                                    {
                                        PRC_ASSIGN_LOGIN_RIGHTS EachRolesToUpdate = context.PRC_ASSIGN_LOGIN_RIGHTS.Where(m => m.PALR_LOG_ID == db_table.Entity.PLOG_ID && m.PALR_ROLE_ID == SelectedRolesId).FirstOrDefault();
                                        if (EachRolesToUpdate == null)
                                        {
                                            EachRolesToUpdate = new PRC_ASSIGN_LOGIN_RIGHTS();
                                            if (AssignRoleId == 0)
                                            {
                                                EachRolesToUpdate.PALR_ID = Convert.ToInt32(context.PRC_ASSIGN_LOGIN_RIGHTS.Max(m => (decimal?)m.PALR_ID)) + 1;
                                                AssignRoleId = EachRolesToUpdate.PALR_ID;
                                            }
                                            else
                                            {
                                                AssignRoleId = (AssignRoleId + 1);
                                                EachRolesToUpdate.PALR_ID = AssignRoleId;
                                            }
                                            EachRolesToUpdate.PALR_ROLE_ID = SelectedRolesId;
                                            EachRolesToUpdate.PALR_USER_ID = db_table.Entity.PLOG_USER_ID;
                                            EachRolesToUpdate.PALR_STATUS = true;
                                            EachRolesToUpdate.PALR_MAKER_ID = Session["USER_ID"].ToString();
                                            EachRolesToUpdate.PALR_CHECKER_ID = null;
                                            EachRolesToUpdate.PALR_DATA_ENTRY_DATETIME = DateTime.Now;
                                            EachRolesToUpdate.PALR_LOG_ID = UpdateEntity.PLOG_ID;
                                            context.PRC_ASSIGN_LOGIN_RIGHTS.Add(EachRolesToUpdate);
                                        }
                                        else
                                        {
                                            EachRolesToUpdate.PALR_MAKER_ID = Session["USER_ID"].ToString();
                                            EachRolesToUpdate.PALR_STATUS = true;
                                            EachRolesToUpdate.PALR_DATA_EDIT_DATETIME = DateTime.Now;
                                            context.Entry(EachRolesToUpdate).State = EntityState.Modified;
                                        }
                                    }
                                }
                                RowsAffected = context.SaveChanges();
                                if (RowsAffected > 0)
                                {
                                    if (Session["USER_ID"].ToString() != "Admin123")
                                    {
                                        RowsAffected = 0;
                                        PRC_USER_WITH_ROLES_LIST_VIEW AssignGroup = context.PRC_USER_WITH_ROLES_LIST_VIEW.Where(m => m.PLOG_ID == UpdateEntity.PLOG_ID).FirstOrDefault();
                                        PRC_LOGIN GetLogin = context.PRC_LOGIN.Where(m => m.PLOG_ID == AssignGroup.PLOG_ID).FirstOrDefault();
                                        PRC_USER_LOG UserLog = new PRC_USER_LOG();
                                        UserLog.PULOG_ID = Convert.ToInt32(context.PRC_USER_LOG.Max(m => (decimal?)m.PULOG_ID)) + 1;
                                        UserLog.PULOG_USER_ID = UpdateEntity.PLOG_USER_ID;
                                        UserLog.PULOG_EMAIL = UpdateEntity.PLOG_EMAIL;
                                        UserLog.PULOG_ASSIGN_GROUP = AssignGroup.ASSIGNED_ROLES;
                                        UserLog.PULOG_ADDED_BY = Session["USER_ID"].ToString();
                                        UserLog.PULOG_ADDED_DATETIME = GetLogin.PLOG_DATA_ENTRY_DATETIME;
                                        if (TempNewUser != "" && TempRemoveUser != "")
                                            UserLog.PULOG_STATUS = "New group " + TempNewUser + " assigned & group name " + TempRemoveUser + " is removed for the user : " + UpdateEntity.PLOG_USER_ID;
                                        else if (TempNewUser != "" && TempRemoveUser == "")
                                            UserLog.PULOG_STATUS = "New group assigned to the user : " + UpdateEntity.PLOG_USER_ID + ", New Assigned group " + TempNewUser;
                                        else if (TempNewUser == "" && TempRemoveUser != "")
                                            UserLog.PULOG_STATUS = "Following group removed for user : " + UpdateEntity.PLOG_USER_ID + ", removed group " + TempRemoveUser;
                                        UserLog.PULOG_ENTRY_DATETIME = DateTime.Now;
                                        context.PRC_USER_LOG.Add(UserLog);
                                        RowsAffected = context.SaveChanges();
                                    }
                                    if (RowsAffected > 0)
                                    {
                                        RowsAffected = 1;
                                        message = "Data Inserted Successfully " + RowsAffected + " Affected";
                                    }
                                }
                            }
                            else
                            {
                                List<PRC_ASSIGN_LOGIN_RIGHTS> DeActivatedRoles = context.PRC_ASSIGN_LOGIN_RIGHTS.Where(m => m.PALR_LOG_ID == db_table.Entity.PLOG_ID).ToList();
                                if (DeActivatedRoles.Count > 0)
                                {
                                    foreach (PRC_ASSIGN_LOGIN_RIGHTS EachRolesForUpdate in DeActivatedRoles)
                                    {
                                        PRC_ROLES RoleTbl = context.PRC_ROLES.Where(m => m.PR_ID == EachRolesForUpdate.PALR_ROLE_ID).FirstOrDefault();
                                        TempRemoveUser += RoleTbl.PR_NAME + ",";
                                        EachRolesForUpdate.PALR_STATUS = false;
                                        context.Entry(EachRolesForUpdate).State = EntityState.Modified;
                                    }
                                    RowsAffected = context.SaveChanges();
                                }
                                else
                                    RowsAffected = 1;
                                if (RowsAffected > 0)
                                {
                                    if (Session["USER_ID"].ToString() != "Admin123")
                                    {
                                        RowsAffected = 0;
                                        PRC_USER_WITH_ROLES_LIST_VIEW AssignGroup = context.PRC_USER_WITH_ROLES_LIST_VIEW.Where(m => m.PLOG_ID == UpdateEntity.PLOG_ID).FirstOrDefault();
                                        PRC_LOGIN GetLogin = context.PRC_LOGIN.Where(m => m.PLOG_ID == AssignGroup.PLOG_ID).FirstOrDefault();
                                        PRC_USER_LOG UserLog = new PRC_USER_LOG();
                                        UserLog.PULOG_ID = Convert.ToInt32(context.PRC_USER_LOG.Max(m => (decimal?)m.PULOG_ID)) + 1;
                                        UserLog.PULOG_USER_ID = UpdateEntity.PLOG_USER_ID;
                                        UserLog.PULOG_EMAIL = UpdateEntity.PLOG_EMAIL;
                                        UserLog.PULOG_ASSIGN_GROUP = AssignGroup.ASSIGNED_ROLES;
                                        UserLog.PULOG_ADDED_BY = Session["USER_ID"].ToString();
                                        UserLog.PULOG_ADDED_DATETIME = GetLogin.PLOG_DATA_ENTRY_DATETIME;
                                        UserLog.PULOG_STATUS = "Roles is successfully removed for the user : " + UpdateEntity.PLOG_USER_ID;
                                        UserLog.PULOG_ENTRY_DATETIME = DateTime.Now;
                                        context.PRC_USER_LOG.Add(UserLog);
                                        RowsAffected = context.SaveChanges();
                                    }
                                    if (RowsAffected > 0)
                                    {
                                        RowsAffected = 1;
                                        message = "Data Inserted Successfully " + RowsAffected + " Affected";
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("PRC_Users", "AddUser", Session["USER_ID"].ToString(), ex);
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                    AssignRoles_Custom entity = new AssignRoles_Custom();
                    if (entity.RolesList != null)
                    {
                        entity.Entity = new PRC_LOGIN();
                        entity.Entity.PLOG_ID = db_table.Entity.PLOG_ID;
                        List<int?> PreviousRoles = context.PRC_ASSIGN_LOGIN_RIGHTS.Where(m => m.PALR_LOG_ID == db_table.Entity.PLOG_ID && m.PALR_STATUS == true).Select(m => m.PALR_ROLE_ID).ToList();
                        List<SelectListItem> items = new SelectList(context.PRC_ROLES.Where(m => m.PR_ID != 1 && m.PR_STATUS == "true" && m.PR_ISAUTH == true).OrderBy(m => m.PR_ID).ToList(), "PR_ID", "PR_NAME", 0).ToList();
                        foreach (SelectListItem item in items)
                        {
                            int? CurrentRId = Convert.ToInt32(item.Value);
                            if (PreviousRoles.Contains(CurrentRId))
                                item.Selected = true;
                        }
                        entity.RolesList = items;
                        return View(entity);
                    }
                    else
                    {
                        entity.Entity = new PRC_LOGIN();
                        entity.Entity.PLOG_ID = 0;
                        List<SelectListItem> items = new SelectList(context.PRC_ROLES.Where(m => m.PR_NAME != "ADMIN" && m.PR_STATUS == "true" && m.PR_ISAUTH == true).OrderBy(m => m.PR_ID).ToList(), "PR_ID", "PR_NAME", 0).ToList();
                        entity.RolesList = items;
                        return View(entity);
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }
        [ChildActionOnly]
        public ActionResult AddUserView()
        {
            if (Session["USER_ID"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                if (DAL.CheckFunctionValidity("PRC_Users", "AddUser", Session["USER_ID"].ToString()))
                {
                    string SessionUser = Session["USER_ID"].ToString();
                    List<PRC_USER_WITH_ROLES_LIST_VIEW> ViewData = context.PRC_USER_WITH_ROLES_LIST_VIEW.Where(m => m.PLOG_USER_ID != "Admin123" && m.PLOG_USER_ID != SessionUser && (m.PLOG_MAKER_ID != SessionUser || m.PLOG_ISAUTH == true)).OrderByDescending(m => m.PLOG_ID).ToList();
                    return PartialView(ViewData);
                }
                else
                    return PartialView(null);
            }
        }
        [HttpPost]
        public JsonResult AuthroizeUser(int LOG_ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("PRC_Users", "AddUser", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PRC_LOGIN UpdateEntity = context.PRC_LOGIN.Where(m => m.PLOG_ID == LOG_ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            if (UpdateEntity.PLOG_MAKER_ID != Session["USER_ID"].ToString())
                            {
                                UpdateEntity.PLOG_ISAUTH = true;
                                UpdateEntity.PLOG_CHECKER_ID = Session["USER_ID"].ToString();
                                UpdateEntity.PLOG_DATA_EDIT_DATETIME = DateTime.Now;
                                context.Entry(UpdateEntity).State = EntityState.Modified;
                                int RowCount = context.SaveChanges();
                                if (RowCount > 0)
                                {
                                    RowCount = 0;
                                    PRC_USER_WITH_ROLES_LIST_VIEW AssignGroup = context.PRC_USER_WITH_ROLES_LIST_VIEW.Where(m => m.PLOG_ID == UpdateEntity.PLOG_ID).FirstOrDefault();
                                    PRC_USER_LOG UserLog = new PRC_USER_LOG();
                                    UserLog.PULOG_ID = Convert.ToInt32(context.PRC_USER_LOG.Max(m => (decimal?)m.PULOG_ID)) + 1;
                                    UserLog.PULOG_USER_ID = UpdateEntity.PLOG_USER_ID;
                                    UserLog.PULOG_EMAIL = UpdateEntity.PLOG_EMAIL;
                                    UserLog.PULOG_ASSIGN_GROUP = AssignGroup.ASSIGNED_ROLES;
                                    UserLog.PULOG_ADDED_BY = UpdateEntity.PLOG_MAKER_ID;
                                    UserLog.PULOG_ADDED_DATETIME = UpdateEntity.PLOG_DATA_ENTRY_DATETIME;
                                    UserLog.PULOG_ACTIVATED_BY = Session["USER_ID"].ToString();
                                    UserLog.PULOG_ACTIVATED_DATETIME = DateTime.Now;
                                    UserLog.PULOG_STATUS = "Activated the user : " + UpdateEntity.PLOG_USER_ID;
                                    UserLog.PULOG_ENTRY_DATETIME = DateTime.Now;
                                    context.PRC_USER_LOG.Add(UserLog);
                                    RowCount = context.SaveChanges();
                                    if (RowCount > 0)
                                        message = "User : " + UpdateEntity.PLOG_USER_NAME + " is successfully Authorized";
                                }
                            }
                            else
                                message = "Sorry, Maker cannot authorize the same record.";
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + LOG_ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("PRC_Users", "AuthorizeUser", Session["USER_ID"].ToString(), ex);
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid request");
                }
            }
            else
                return Json("Invalid request");
        }
        [HttpPost]
        public JsonResult DeleteFromAuthroizeUser(int LOG_ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("PRC_Users", "AddUser", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        PRC_LOGIN UpdateEntity = context.PRC_LOGIN.Where(m => m.PLOG_ID == LOG_ID).FirstOrDefault();

                        #region Save Group Name Before Deleting
                        string GetGroupName = "";
                        PRC_USER_WITH_ROLES_LIST_VIEW GroupName = context.PRC_USER_WITH_ROLES_LIST_VIEW.Where(m => m.PLOG_USER_ID == UpdateEntity.PLOG_USER_ID).FirstOrDefault();
                        if (GroupName != null)
                        {
                            GetGroupName += GroupName.ASSIGNED_ROLES;
                        }
                        #endregion

                        if (UpdateEntity != null)
                        {
                            if (UpdateEntity.PLOG_MAKER_ID != Session["USER_ID"].ToString())
                            {
                                context.PRC_LOGIN.Remove(UpdateEntity);
                                int RowCount = context.SaveChanges();
                                if (RowCount > 0)
                                {
                                    List<PRC_ASSIGN_LOGIN_RIGHTS> AssignUser = context.PRC_ASSIGN_LOGIN_RIGHTS.Where(m => m.PALR_USER_ID == UpdateEntity.PLOG_USER_ID).ToList();
                                    if (AssignUser.Count > 0)
                                    {
                                        RowCount = 0;
                                        foreach (PRC_ASSIGN_LOGIN_RIGHTS item in AssignUser)
                                        {
                                            context.PRC_ASSIGN_LOGIN_RIGHTS.Remove(item);
                                            RowCount = +context.SaveChanges();
                                        }
                                    }
                                    if (RowCount > 0)
                                    {
                                        RowCount = 0;
                                        #region Log User
                                        PRC_USER_WITH_ROLES_LIST_VIEW AssignGroup = context.PRC_USER_WITH_ROLES_LIST_VIEW.Where(m => m.PLOG_ID == UpdateEntity.PLOG_ID).FirstOrDefault();
                                        PRC_USER_LOG UserLog = new PRC_USER_LOG();
                                        UserLog.PULOG_ID = Convert.ToInt32(context.PRC_USER_LOG.Max(m => (decimal?)m.PULOG_ID)) + 1;
                                        UserLog.PULOG_USER_ID = UpdateEntity.PLOG_USER_ID;
                                        UserLog.PULOG_EMAIL = UpdateEntity.PLOG_EMAIL;
                                        UserLog.PULOG_ASSIGN_GROUP = GetGroupName;
                                        UserLog.PULOG_ADDED_BY = UpdateEntity.PLOG_MAKER_ID;
                                        UserLog.PULOG_ADDED_DATETIME = UpdateEntity.PLOG_DATA_ENTRY_DATETIME;
                                        UserLog.PULOG_ACTIVATED_BY = Session["USER_ID"].ToString();
                                        UserLog.PULOG_ACTIVATED_DATETIME = DateTime.Now;
                                        UserLog.PULOG_STATUS = "Following user is Rejected/Deleted : " + UpdateEntity.PLOG_USER_ID;
                                        UserLog.PULOG_ENTRY_DATETIME = DateTime.Now;
                                        context.PRC_USER_LOG.Add(UserLog);
                                        RowCount = context.SaveChanges();
                                        #endregion
                                        if (RowCount > 0)
                                            message = "User : " + UpdateEntity.PLOG_USER_NAME + " is Rejected";
                                    }
                                }
                            }
                            else
                                message = "Sorry, Maker cannot reject the same record.";
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + LOG_ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("PRC_Users", "DeleteFromAuthorizeUser", Session["USER_ID"].ToString(), ex);
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid request");
                }
            }
            else
                return Json("Invalid request");
        }

        #region User Report
        public ActionResult UserActivity()
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("PRC_Users", "UserActivity", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {

                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
            return View();
        }
        [HttpPost]
        public ActionResult UserActivity(DateTime? FromDate, DateTime? EndDate)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("PRC_Users", "UserActivity", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (FromDate != null && EndDate != null)
                        {
                            var Data = context.PRC_USER_ACTIVITY_REPORT(FromDate, EndDate).ToList();
                            if (Data.Count() > 0)
                            {
                                Data = Data.Where(m => m.PULOG_ACTIVATED_BY != "Admin123" && m.PULOG_ADDED_BY != "Admin123").ToList();
                                return View(Data);
                            }
                            else
                            {
                                message = "No Data found in the system : On selected Start date : " + FromDate + " End date : " + EndDate;
                            }
                        }
                        else
                        {
                            if (FromDate == null && EndDate == null)
                                message = "Select start date and end date to continue";
                            else if (FromDate == null)
                                message = "Select start date to continue";
                            else if (EndDate == null)
                                message = "Select end date to continue";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                        DAL.LogException("PRC_Users", "UserActivity", Session["USER_ID"].ToString(), ex);
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                    return View();
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }
        #endregion
    }
}