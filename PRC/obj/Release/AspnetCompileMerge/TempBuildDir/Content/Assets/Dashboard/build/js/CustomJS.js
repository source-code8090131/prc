﻿//#region Custom Template Js 
$(window).on('load', function () {
    $('#loading').hide();
});
$(function () {
    $("#example1").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false, "order": false,
        "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    //Initialize Select2 Elements
    $('.select2').select2()
    //Initialize Select2 Elements
    $('.select2bs4').select2({
        theme: 'bootstrap4'
    })
});
$(document).ready(function () {
    $('[data-toggle="tooltip"]').tooltip();
});

//#endregion

//#region Custom Date Picker
$(function () {
    $(".CustomDatePicker").datepicker();
});
//#endregion

//#region Group Role Authorize/Reject
$("[name='AuthroizeRole']").click(function (button) {
    var R_ID = $(this).attr("value");
    $.ajax
        ({
            url: '/PRC_Groups/AuthorizeGroupRole?R_ID=' + R_ID,
            type: 'POST',
            datatype: 'application/json',
            contentType: 'application/json',
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (result) {
                alert(result);
                location.reload();
            },
            error: function () {
                //alert("Something went wrong..");
            },
        });
});
$("[name='DeleteFromAuthroizeRole']").click(function (button) {
    var R_ID = $(this).attr("value");
    var check = confirm("Are You Sure your want to delete?");
    if (check == true) {
        $.ajax
            ({
                url: '/PRC_Groups/RejectAuthorizeGroupRole?R_ID=' + R_ID,
                type: 'POST',
                datatype: 'application/json',
                contentType: 'application/json',
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (result) {
                    alert(result);
                    location.reload();
                },
                error: function () {
                    //alert("Something went wrong..");
                },
            });
    }
});
//#endregion

//#region User Authorize/Reject
$("[name='AuthroizeUser']").click(function (button) {
    var LOG_ID = $(this).attr("value");
    $.ajax
        ({
            url: '/PRC_Users/AuthroizeUser?LOG_ID=' + LOG_ID,
            type: 'POST',
            datatype: 'application/json',
            contentType: 'application/json',
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (result) {
                alert(result);
                location.reload();
            },
            error: function () {
                //alert("Something went wrong..");
            },
        });
});
$("[name='DeleteFromAuthroizeUser']").click(function (button) {
    var LOG_ID = $(this).attr("value");
    var check = confirm("Are You Sure your want to delete?");
    if (check == true) {
        $.ajax
            ({
                url: '/PRC_Users/DeleteFromAuthroizeUser?LOG_ID=' + LOG_ID,
                type: 'POST',
                datatype: 'application/json',
                contentType: 'application/json',
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (result) {
                    alert(result);
                    location.reload();
                },
                error: function () {
                    //alert("Something went wrong..");
                },
            });
    }
});
    //#endregion

//#region Client Portfolio Authorize/Reject
$("[name='AuthorizeClient']").click(function (button) {
    var CP_ID = $(this).attr("value");
    $.ajax
        ({
            url: '/ClientProfile/AuthorizeClient?CP_ID=' + CP_ID,
            type: 'POST',
            datatype: 'application/json',
            contentType: 'application/json',
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (result) {
                alert(result);
                location.reload();
            },
            error: function () {
                //alert("Something went wrong..");
            },
        });
});
$("[name='DeleteFromAuthorizeClient']").click(function (button) {
    var CP_ID = $(this).attr("value");
    var check = confirm("Are You Sure your want to delete?");
    if (check == true) {
        $.ajax
            ({
                url: '/ClientProfile/DeleteFromAuthorizeClient?CP_ID=' + CP_ID,
                type: 'POST',
                datatype: 'application/json',
                contentType: 'application/json',
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (result) {
                    alert(result);
                    location.reload();
                },
                error: function () {
                    //alert("Something went wrong..");
                },
            });
    }
});
    //#endregion

//#region Verifying Agency Authorize/Reject
$("[name='AuthorizeAgency']").click(function (button) {
    var PVA_ID = $(this).attr("value");
    $.ajax
        ({
            url: '/Setup/AuthorizeVerifyingAgency?PVA_ID=' + PVA_ID,
            type: 'POST',
            datatype: 'application/json',
            contentType: 'application/json',
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (result) {
                alert(result);
                location.reload();
            },
            error: function () {
                //alert("Something went wrong..");
            },
        });
});
$("[name='RejecAgency']").click(function (button) {
    var PVA_ID = $(this).attr("value");
    var check = confirm("Are You Sure your want to delete?");
    if (check == true) {
        $.ajax
            ({
                url: '/Setup/RejectVerifyingAgency?PVA_ID=' + PVA_ID,
                type: 'POST',
                datatype: 'application/json',
                contentType: 'application/json',
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (result) {
                    alert(result);
                    location.reload();
                },
                error: function () {
                    //alert("Something went wrong..");
                },
            });
    }
});
    //#endregion

//#region Verifying Officer Authorize/Reject
$("[name='AuthorizeOfficer']").click(function (button) {
    var PAP_ID = $(this).attr("value");
    $.ajax
        ({
            url: '/Setup/AuthorizeVerifyingOfficer?PAP_ID=' + PAP_ID,
            type: 'POST',
            datatype: 'application/json',
            contentType: 'application/json',
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (result) {
                alert(result);
                location.reload();
            },
            error: function () {
                //alert("Something went wrong..");
            },
        });
});
$("[name='RejecOfficer']").click(function (button) {
    var PAP_ID = $(this).attr("value");
    var check = confirm("Are You Sure your want to delete?");
    if (check == true) {
        $.ajax
            ({
                url: '/Setup/RejectVerifyingOfficer?PAP_ID=' + PAP_ID,
                type: 'POST',
                datatype: 'application/json',
                contentType: 'application/json',
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (result) {
                    alert(result);
                    location.reload();
                },
                error: function () {
                    //alert("Something went wrong..");
                },
            });
    }
});
    //#endregion

//#region Department Authorize/Reject
$("[name='AuthorizeDepartment']").click(function (button) {
    var PD_ID = $(this).attr("value");
    $.ajax
        ({
            url: '/Setup/AuthorizeDepartment?PD_ID=' + PD_ID,
            type: 'POST',
            datatype: 'application/json',
            contentType: 'application/json',
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (result) {
                alert(result);
                location.reload();
            },
            error: function () {
                //alert("Something went wrong..");
            },
        });
});
$("[name='RejectDepartment']").click(function (button) {
    var PD_ID = $(this).attr("value");
    var check = confirm("Are You Sure your want to delete?");
    if (check == true) {
        $.ajax
            ({
                url: '/Setup/RejectDepartment?PD_ID=' + PD_ID,
                type: 'POST',
                datatype: 'application/json',
                contentType: 'application/json',
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (result) {
                    alert(result);
                    location.reload();
                },
                error: function () {
                    //alert("Something went wrong..");
                },
            });
    }
});
    //#endregion

//#region Purpose Of Verification Authorize/Reject
$("[name='AuthorizePurpose']").click(function (button) {
    var PPV_ID = $(this).attr("value");
    $.ajax
        ({
            url: '/Setup/AuthorizeVerificationPurpose?PPV_ID=' + PPV_ID,
            type: 'POST',
            datatype: 'application/json',
            contentType: 'application/json',
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (result) {
                alert(result);
                location.reload();
            },
            error: function () {
                //alert("Something went wrong..");
            },
        });
});
$("[name='RejectPurpose']").click(function (button) {
    var PPV_ID = $(this).attr("value");
    var check = confirm("Are You Sure your want to delete?");
    if (check == true) {
        $.ajax
            ({
                url: '/Setup/RejectVerificationPurpose?PPV_ID=' + PPV_ID,
                type: 'POST',
                datatype: 'application/json',
                contentType: 'application/json',
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (result) {
                    alert(result);
                    location.reload();
                },
                error: function () {
                    //alert("Something went wrong..");
                },
            });
    }
});
    //#endregion

//#region Email Configration Authorize/Reject/Set As Current
$("[name='AuthorizeEmailSetup']").click(function (button) {
    var EC_ID = $(this).attr("value");
    $.ajax
        ({
            url: '/Setup/AuthorizeEmailSetup?EC_ID=' + EC_ID,
            type: 'POST',
            datatype: 'application/json',
            contentType: 'application/json',
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (result) {
                alert(result);
                location.reload();
            },
            error: function () {
                //alert("Something went wrong..");
            },
        });
});
$("[name='RejectEmailSetup']").click(function (button) {
    var EC_ID = $(this).attr("value");
    var check = confirm("Are You Sure your want to delete?");
    if (check == true) {
        $.ajax
            ({
                url: '/Setup/RejectEmailSetup?EC_ID=' + EC_ID,
                type: 'POST',
                datatype: 'application/json',
                contentType: 'application/json',
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (result) {
                    alert(result);
                    location.reload();
                },
                error: function () {
                    //alert("Something went wrong..");
                },
            });
    }
});
$("[name='UpdateEmailSetupStatus']").click(function (button) {
    var EC_ID = $(this).attr("value");
    $.ajax
        ({
            url: '/Setup/UpdateEmailSetupStatus?EC_ID=' + EC_ID,
            type: 'POST',
            datatype: 'application/json',
            contentType: 'application/json',
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (result) {
                alert(result);
                location.reload();
            },
            error: function () {
                //alert("Something went wrong..");
            },
        });
});
//#endregion

//#region System Setting Authorize/Reject
$("[name='AuthorizeSystemSetting']").click(function (button) {
    var PSS_ID = $(this).attr("value");
    $.ajax
        ({
            url: '/Setup/AuthorizeSystemSetting?PSS_ID=' + PSS_ID,
            type: 'POST',
            datatype: 'application/json',
            contentType: 'application/json',
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (result) {
                alert(result);
                location.reload();
            },
            error: function () {
                //alert("Something went wrong..");
            },
        });
});
$("[name='RejectSystemSetting']").click(function (button) {
    var PD_ID = $(this).attr("value");
    var check = confirm("Are You Sure your want to delete?");
    if (check == true) {
        $.ajax
            ({
                url: '/Setup/RejectSystemSetting?PSS_ID=' + PSS_ID,
                type: 'POST',
                datatype: 'application/json',
                contentType: 'application/json',
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (result) {
                    alert(result);
                    location.reload();
                },
                error: function () {
                    //alert("Something went wrong..");
                },
            });
    }
});
    //#endregion

$("[name='RequestLogDetail']").click(function (button) {
    var PCRL_ID = $(this).attr("value");
    $.ajax({
        url: "/ClientProfile/RequestLogDetail",
        type: "POST",
        data: { CRL_ID: PCRL_ID },
        beforeSend: function () {
            $('#loading').show();
        },
        complete: function () {
            $('#loading').hide();
        },
        success: function (Data) {
            $("#ViewRequestLogDetail").load("/ClientProfile/RequestLogDetail?CRL_ID=" + PCRL_ID);
        },
        error: function (error) {
            alert(error);
        }
    });
});

//#region Browser Back Button
$("[name='BrowserBack']").click(function (button) {
    history.back();
});
//#endregion

//#region Check Only one Checkbox
$("[name='InputParams.ePRC']").click(function (button) {
    $("#InputParams_sPRC").prop("checked", false);
});
$("[name='InputParams.sPRC']").click(function (button) {
    $("#InputParams_ePRC").prop("checked", false);
});
//#endregion

//#region Password Filed Show on Hold
$("#passtoggle").on('mousedown', function () {
    $("#GCARParams_PassHash").attr('type','text');
});
$("#passtoggle").on('mouseup', function () {
    $("#GCARParams_PassHash").attr('type', 'password');
});

$("#showhidepass").on('mousedown', function () {
    $("#SettingParms_PSS_GCAR_CERT_HASH").attr('type', 'text');
});
$("#showhidepass").on('mouseup', function () {
    $("#SettingParms_PSS_GCAR_CERT_HASH").attr('type', 'password');
});
//#endregion

