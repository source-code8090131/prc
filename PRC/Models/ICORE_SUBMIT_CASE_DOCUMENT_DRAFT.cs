//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PRC.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class ICORE_SUBMIT_CASE_DOCUMENT_DRAFT
    {
        public int SCDD_ID { get; set; }
        public Nullable<int> SCDD_DOCUMENT_ID { get; set; }
        public string SCDD_DOCUMENT_VALUE { get; set; }
        public string SCDD_COMMENT { get; set; }
        public Nullable<System.DateTime> SCDD_ENTRY_DATETIME { get; set; }
        public Nullable<System.DateTime> SCDD_EDIT_DATETIME { get; set; }
        public string SCDD_MAKER_ID { get; set; }
        public string SCDD_CHECKER_ID { get; set; }
        public Nullable<int> SCDD_FORM_ID { get; set; }
        public string SCDD_INSERTED_BY { get; set; }
    }
}
