﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PRC.Models
{
    public class AssignFunctions_Custom
    {
        public PRC_ROLES Entity { get; set; }
        public int?[] SelectedFunctions { get; set; }
        public IEnumerable<SelectListItem> FunctionsList { get; set; }
    }
}