﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PRC.Models
{
    public class GCARValidation
    {
        public string UNIQUE_REF { get; set; }
        public string CLNT_ID { get; set; }
        public string IBAN_ACCT_NO { get; set; }
    }
}