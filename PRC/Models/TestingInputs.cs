﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PRC.Models
{
    public class TestingInputs
    {

        public GCARTESTING GCARParams { get; set; }
        public InputFilter InputParams { get; set; }
        public PRC_SYSYTEM_SETUP SettingParms { get; set; }
        public string PaymentType { get; set; }
        public PRC_AUTO_EMAILING AutoEmailing { get; set; }

        public class GCARTESTING
        {
            public string UNIQUE_REF { get; set; }
            public string CLIENT_ID { get; set; }
            public string IBAN_ACCTNO { get; set; }
            public string URL { get; set; }
            //public string PassHash { get; set; }
        }

        public class InputFilter
        {
            public DateTime? FromDate { get; set; }
            public DateTime? ToDate { get; set; }
            public string AccountNo { get; set; }
            public string BtnSubmit { get; set; }
            public bool ePRC { get; set; }
            public bool sPRC { get; set; }
            public string SearchFilter { get; set; }
            public string TransReference { get; set; }
            public string PRCNumber { get; set; }
        }
    }
}