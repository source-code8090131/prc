﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PRC.Models
{
    public class GCARResponse
    {
        public string UNIQUE_REF { get; set; }
        public string CLNT_ID { get; set; }
        public string IBAN_ACCT_NO { get; set; }
        public string VALIDATION_STAT { get; set; }
        public string VALIDATION_STAT_COD { get; set; }

    }
}