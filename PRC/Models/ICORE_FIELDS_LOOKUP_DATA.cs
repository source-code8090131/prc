//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PRC.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class ICORE_FIELDS_LOOKUP_DATA
    {
        public int Lookup_Data_Id { get; set; }
        public int Lookup_Field_Id { get; set; }
        public string Lookup_Value { get; set; }
        public string Lookup_Text { get; set; }
    }
}
