﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace PRC
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            MvcHandler.DisableMvcResponseHeader = true;
        }
        protected void Application_BeginRequest()
        {
            string[] headers = { "Server", "X-AspNet-Version", "X-AspNetMvc-Version", "X-Powered-By" };

            if (!Response.HeadersWritten)
            {
                Response.AddOnSendingHeaders((c) =>
                {
                    if (c != null && c.Response != null && c.Response.Headers != null)
                    {
                        foreach (string header in headers)
                        {
                            if (c.Response.Headers[header] != null)
                            {
                                c.Response.Headers.Remove(header);
                            }
                        }
                    }
                });
            }
        }
        protected void Session_Start(Object sender, EventArgs e)
        {
            if (Request.IsSecureConnection == true)
                Response.Cookies["ASP.NET_SessionId"].Secure = true;
        }
    }
}
