﻿//#region Custom Template Js 
$(window).on('load', function () {
    $('#loading').hide();
});

$('#FormType').each(function () {
    var CheckFormType = $(this).attr('value');
    if (CheckFormType == "Edit") {
        $('#Entity_PLOG_USER_ID').attr('readonly', true);
    }
    else {
        $('#Entity_PLOG_USER_ID').attr('readonly', false);
    }
});

$(function () {
    $("#example1").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false, "order": false
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    $("#example2").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false, "order": false
    }).buttons().container().appendTo('#example2_wrapper .col-md-6:eq(0)');
    $("#Reporting").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false, "order": false,
        "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#Reporting_wrapper .col-md-6:eq(0)');
    //Initialize Select2 Elements
    $('.select2').select2()
    //Initialize Select2 Elements
    $('.select2bs4').select2({
        theme: 'bootstrap4'
    })
    //if ($('[name="InputParams.ePRC"]').is(':checked')) {
    //    alert("EPRC is Checked");
    //    //$("#InputParams_sPRC").prop("checked", false);
    //}
    //else if ($('[name="InputParams.sPRC"]').is(':checked')) {
    //    //$("#InputParams_ePRC").prop("checked", false);
    //    alert("SPRC is Checked");
    //}
});
$(document).ready(function () {
    $('[data-toggle="tooltip"]').tooltip();

    $('#AutoEmailing_PAE_EXECUTION_DATE_N_MONTH').keypress(function (e) {
        var $jqDate = jQuery('input[id="AutoEmailing_PAE_EXECUTION_DATE_N_MONTH"]');
        var numChars = $jqDate.val().length;
        if (numChars === 2) {
            var thisVal = $jqDate.val();
            thisVal += '/';
            $jqDate.val(thisVal);
        }
        var max_chars = 4;
        if ($(this).val().length >= max_chars) {
            $(this).val($(this).val().substr(0, max_chars));
        }
        var charCode = (e.which) ? e.which : event.keyCode
        if (String.fromCharCode(charCode).match(/[^0-9/]/g))
            return false;
    });
    $('#AutoEmailing_PAE_DATA_RANGE_FROM_DATE').keypress(function (e) {
        var $jqDate = jQuery('input[id="AutoEmailing_PAE_DATA_RANGE_FROM_DATE"]');
        var numChars = $jqDate.val().length;
        if (numChars === 2) {
            var thisVal = $jqDate.val();
            thisVal += '/';
            $jqDate.val(thisVal);
        }
        var max_chars = 4;
        if ($(this).val().length >= max_chars) {
            $(this).val($(this).val().substr(0, max_chars));
        }
        var charCode = (e.which) ? e.which : event.keyCode
        if (String.fromCharCode(charCode).match(/[^0-9/]/g))
            return false;
    });
    $('#AutoEmailing_PAE_DATA_RANGE_TO').keypress(function (e) {
        var $jqDate = jQuery('input[id="AutoEmailing_PAE_DATA_RANGE_TO"]');
        var numChars = $jqDate.val().length;
        if (numChars === 2) {
            var thisVal = $jqDate.val();
            thisVal += '/';
            $jqDate.val(thisVal);
        }
        var max_chars = 4;
        if ($(this).val().length >= max_chars) {
            $(this).val($(this).val().substr(0, max_chars));
        }
        var charCode = (e.which) ? e.which : event.keyCode
        if (String.fromCharCode(charCode).match(/[^0-9/]/g))
            return false;
    });    
});

//#endregion

//#region Custom Date Picker
$(function () {
    $(".CustomDatePicker").datepicker();
});
//#endregion

//#region Group Role Authorize/Reject
$("[name='AuthroizeRole']").click(function (button) {
    var R_ID = $(this).attr("value");
    $.ajax
        ({
            url: '/PRC_Groups/AuthorizeGroupRole?R_ID=' + R_ID,
            type: 'POST',
            datatype: 'application/json',
            contentType: 'application/json',
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (result) {
                alert(result);
                location.reload();
            },
            error: function () {
                //alert("Something went wrong..");
            },
        });
});
$("[name='DeleteFromAuthroizeRole']").click(function (button) {
    var R_ID = $(this).attr("value");
    var check = confirm("Are You Sure your want to delete?");
    if (check == true) {
        $.ajax
            ({
                url: '/PRC_Groups/RejectAuthorizeGroupRole?R_ID=' + R_ID,
                type: 'POST',
                datatype: 'application/json',
                contentType: 'application/json',
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (result) {
                    alert(result);
                    location.reload();
                },
                error: function () {
                    //alert("Something went wrong..");
                },
            });
    }
});
//#endregion

//#region User Authorize/Reject
$("[name='AuthroizeUser']").click(function (button) {
    var LOG_ID = $(this).attr("value");
    $.ajax
        ({
            url: '/PRC_Users/AuthroizeUser?LOG_ID=' + LOG_ID,
            type: 'POST',
            datatype: 'application/json',
            contentType: 'application/json',
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (result) {
                alert(result);
                location.reload();
            },
            error: function () {
                //alert("Something went wrong..");
            },
        });
});
$("[name='DeleteFromAuthroizeUser']").click(function (button) {
    var LOG_ID = $(this).attr("value");
    var check = confirm("Are You Sure your want to delete?");
    if (check == true) {
        $.ajax
            ({
                url: '/PRC_Users/DeleteFromAuthroizeUser?LOG_ID=' + LOG_ID,
                type: 'POST',
                datatype: 'application/json',
                contentType: 'application/json',
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (result) {
                    alert(result);
                    location.reload();
                },
                error: function () {
                    //alert("Something went wrong..");
                },
            });
    }
});
    //#endregion

//#region Client Portfolio Authorize/Reject
$("[name='AuthorizeClient']").click(function (button) {
    var CP_ID = $(this).attr("value");
    $.ajax
        ({
            url: '/ClientProfile/AuthorizeClient?CP_ID=' + CP_ID,
            type: 'POST',
            datatype: 'application/json',
            contentType: 'application/json',
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (result) {
                alert(result);
                location.reload();
            },
            error: function () {
                //alert("Something went wrong..");
            },
        });
});
$("[name='DeleteFromAuthorizeClient']").click(function (button) {
    var CP_ID = $(this).attr("value");
    var check = confirm("Are You Sure your want to delete?");
    if (check == true) {
        $.ajax
            ({
                url: '/ClientProfile/DeleteFromAuthorizeClient?CP_ID=' + CP_ID,
                type: 'POST',
                datatype: 'application/json',
                contentType: 'application/json',
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (result) {
                    alert(result);
                    location.reload();
                },
                error: function () {
                    //alert("Something went wrong..");
                },
            });
    }
});
    //#endregion

//#region Verifying Agency Authorize/Reject
$("[name='AuthorizeAgency']").click(function (button) {
    var PVA_ID = $(this).attr("value");
    $.ajax
        ({
            url: '/Setup/AuthorizeVerifyingAgency?PVA_ID=' + PVA_ID,
            type: 'POST',
            datatype: 'application/json',
            contentType: 'application/json',
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (result) {
                alert(result);
                location.reload();
            },
            error: function () {
                //alert("Something went wrong..");
            },
        });
});
$("[name='RejecAgency']").click(function (button) {
    var PVA_ID = $(this).attr("value");
    var check = confirm("Are You Sure your want to delete?");
    if (check == true) {
        $.ajax
            ({
                url: '/Setup/RejectVerifyingAgency?PVA_ID=' + PVA_ID,
                type: 'POST',
                datatype: 'application/json',
                contentType: 'application/json',
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (result) {
                    alert(result);
                    location.reload();
                },
                error: function () {
                    //alert("Something went wrong..");
                },
            });
    }
});
    //#endregion

//#region Verifying Officer Authorize/Reject
$("[name='AuthorizeOfficer']").click(function (button) {
    var PAP_ID = $(this).attr("value");
    $.ajax
        ({
            url: '/Setup/AuthorizeVerifyingOfficer?PAP_ID=' + PAP_ID,
            type: 'POST',
            datatype: 'application/json',
            contentType: 'application/json',
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (result) {
                alert(result);
                location.reload();
            },
            error: function () {
                //alert("Something went wrong..");
            },
        });
});
$("[name='RejecOfficer']").click(function (button) {
    var PAP_ID = $(this).attr("value");
    var check = confirm("Are You Sure your want to delete?");
    if (check == true) {
        $.ajax
            ({
                url: '/Setup/RejectVerifyingOfficer?PAP_ID=' + PAP_ID,
                type: 'POST',
                datatype: 'application/json',
                contentType: 'application/json',
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (result) {
                    alert(result);
                    location.reload();
                },
                error: function () {
                    //alert("Something went wrong..");
                },
            });
    }
});
    //#endregion

//#region Department Authorize/Reject
$("[name='AuthorizeDepartment']").click(function (button) {
    var PD_ID = $(this).attr("value");
    $.ajax
        ({
            url: '/Setup/AuthorizeDepartment?PD_ID=' + PD_ID,
            type: 'POST',
            datatype: 'application/json',
            contentType: 'application/json',
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (result) {
                alert(result);
                location.reload();
            },
            error: function () {
                //alert("Something went wrong..");
            },
        });
});
$("[name='RejectDepartment']").click(function (button) {
    var PD_ID = $(this).attr("value");
    var check = confirm("Are You Sure your want to delete?");
    if (check == true) {
        $.ajax
            ({
                url: '/Setup/RejectDepartment?PD_ID=' + PD_ID,
                type: 'POST',
                datatype: 'application/json',
                contentType: 'application/json',
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (result) {
                    alert(result);
                    location.reload();
                },
                error: function () {
                    //alert("Something went wrong..");
                },
            });
    }
});
    //#endregion

//#region Purpose Of Verification Authorize/Reject
$("[name='AuthorizePurpose']").click(function (button) {
    var PPV_ID = $(this).attr("value");
    $.ajax
        ({
            url: '/Setup/AuthorizeVerificationPurpose?PPV_ID=' + PPV_ID,
            type: 'POST',
            datatype: 'application/json',
            contentType: 'application/json',
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (result) {
                alert(result);
                location.reload();
            },
            error: function () {
                //alert("Something went wrong..");
            },
        });
});
$("[name='RejectPurpose']").click(function (button) {
    var PPV_ID = $(this).attr("value");
    var check = confirm("Are You Sure your want to delete?");
    if (check == true) {
        $.ajax
            ({
                url: '/Setup/RejectVerificationPurpose?PPV_ID=' + PPV_ID,
                type: 'POST',
                datatype: 'application/json',
                contentType: 'application/json',
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (result) {
                    alert(result);
                    location.reload();
                },
                error: function () {
                    //alert("Something went wrong..");
                },
            });
    }
});
    //#endregion

//#region Email Configration Authorize/Reject/Set As Current
$("[name='AuthorizeEmailSetup']").click(function (button) {
    var EC_ID = $(this).attr("value");
    $.ajax
        ({
            url: '/Setup/AuthorizeEmailSetup?EC_ID=' + EC_ID,
            type: 'POST',
            datatype: 'application/json',
            contentType: 'application/json',
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (result) {
                alert(result);
                location.reload();
            },
            error: function () {
                //alert("Something went wrong..");
            },
        });
});
$("[name='RejectEmailSetup']").click(function (button) {
    var EC_ID = $(this).attr("value");
    var check = confirm("Are You Sure your want to delete?");
    if (check == true) {
        $.ajax
            ({
                url: '/Setup/RejectEmailSetup?EC_ID=' + EC_ID,
                type: 'POST',
                datatype: 'application/json',
                contentType: 'application/json',
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (result) {
                    alert(result);
                    location.reload();
                },
                error: function () {
                    //alert("Something went wrong..");
                },
            });
    }
});
$("[name='UpdateEmailSetupStatus']").click(function (button) {
    var EC_ID = $(this).attr("value");
    $.ajax
        ({
            url: '/Setup/UpdateEmailSetupStatus?EC_ID=' + EC_ID,
            type: 'POST',
            datatype: 'application/json',
            contentType: 'application/json',
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (result) {
                alert(result);
                location.reload();
            },
            error: function () {
                //alert("Something went wrong..");
            },
        });
});
//#endregion

//#region System Setting Authorize/Reject
$("[name='AuthorizeSystemSetting']").click(function (button) {
    var PSS_ID = $(this).attr("value");
    $.ajax
        ({
            url: '/Setup/AuthorizeSystemSetting?PSS_ID=' + PSS_ID,
            type: 'POST',
            datatype: 'application/json',
            contentType: 'application/json',
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (result) {
                alert(result);
                location.reload();
            },
            error: function () {
                //alert("Something went wrong..");
            },
        });
});
$("[name='RejectSystemSetting']").click(function (button) {
    var PD_ID = $(this).attr("value");
    var check = confirm("Are You Sure your want to delete?");
    if (check == true) {
        $.ajax
            ({
                url: '/Setup/RejectSystemSetting?PSS_ID=' + PSS_ID,
                type: 'POST',
                datatype: 'application/json',
                contentType: 'application/json',
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (result) {
                    alert(result);
                    location.reload();
                },
                error: function () {
                    //alert("Something went wrong..");
                },
            });
    }
});
    //#endregion

//#region Manual Uploaded PRC Authorize/Reject
$("[name='AuthroizeMUC']").click(function (button) {
    var GPD_ID = $(this).attr("value");
    $.ajax
        ({
            url: '/PRC/AuthroizeMUC?GPD_ID=' + GPD_ID,
            type: 'POST',
            datatype: 'application/json',
            contentType: 'application/json',
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (result) {
                alert(result);
                location.reload();
            },
            error: function () {
                //alert("Something went wrong..");
            },
        });
});
$("[name='RejectMUC']").click(function (button) {
    var GPD_ID = $(this).attr("value");
    var check = confirm("Are You Sure your want to delete?");
    if (check == true) {
        $.ajax
            ({
                url: '/PRC/RejectMUC?GPD_ID=' + GPD_ID,
                type: 'POST',
                datatype: 'application/json',
                contentType: 'application/json',
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (result) {
                    alert(result);
                    location.reload();
                },
                error: function () {
                    //alert("Something went wrong..");
                },
            });
    }
});
    //#endregion

$("[name='RequestLogDetail']").click(function (button) {
    var PCRL_ID = $(this).attr("value");
    $.ajax({
        url: "/ClientProfile/RequestLogDetail",
        type: "POST",
        data: { CRL_ID: PCRL_ID },
        beforeSend: function () {
            $('#loading').show();
        },
        complete: function () {
            $('#loading').hide();
        },
        success: function (Data) {
            $("#ViewRequestLogDetail").load("/ClientProfile/RequestLogDetail?CRL_ID=" + PCRL_ID);
        },
        error: function (error) {
            alert(error);
        }
    });
});

//#region Browser Back Button
$("[name='BrowserBack']").click(function (button) {
    history.back();
});
//#endregion

//#region Check Only one Checkbox
$("[name='InputParams.ePRC']").click(function (button) {
    $("#InputParams_sPRC").prop("checked", false);
});
$("[name='InputParams.sPRC']").click(function (button) {
    $("#InputParams_ePRC").prop("checked", false);
});
//#endregion

//#region Password Filed Show on Hold
$("#passtoggle").on('mousedown', function () {
    $("#GCARParams_PassHash").attr('type','text');
});
$("#passtoggle").on('mouseup', function () {
    $("#GCARParams_PassHash").attr('type', 'password');
});

$("#showhidepass").on('mousedown', function () {
    $("#SettingParms_PSS_GCAR_CERT_HASH").attr('type', 'text');
});
$("#showhidepass").on('mouseup', function () {
    $("#SettingParms_PSS_GCAR_CERT_HASH").attr('type', 'password');
});
//#endregion

